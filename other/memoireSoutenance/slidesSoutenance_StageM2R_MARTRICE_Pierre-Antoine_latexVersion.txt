\documentclass[8pt]{beamer}
\usetheme{Warsaw}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{multicol}
% \usepackage{paralist}


\definecolor{mygreen}{rgb}{0,0.5,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

% Paramètres juste pour la citation du code python (les architectures)
\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=python,                 % the language of the code
  morekeywords={*, KDLayer, Dense, ...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=10pt,                  % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  framexleftmargin=5mm,
  frame=single,
  rulesepcolor=\color{mygray},
  rulecolor=\color{orange},
  backgroundcolor=\color{orange!5}
}

\title{Décompositions tensorielles dans des réseaux neuronaux}
\author{\textbf{Martrice Pierre-Antoine}}
\institute{Thierry Artieres \\ Stéphane Ayache \\ Hachem Kadri \\ - \\Laboratoire d’Informatique Fondamentale de Marseille (LIF) \\ éQuipe d'AppRentissage de MArseille (QARMA)}
\setbeamerfont{institute}{size=\small}
\date{19/06/2017}

\begin{document}
%### PAGE DE GARDE ### 1 ########################
\begin{frame}
\titlepage
\end{frame}
%### INTRODUCTION ### 2 ########################
\begin{frame}
\frametitle{Introduction}
\begin{description}
\item[Problématique] Utilisation de décompositions tensorielles de faible rang au sein de réseaux de neurones.
\bigskip

\item[Intérêt] - Utilisation sur des périphériques à ressources limités.

- Apprentissage à partir de peu de données.
\bigskip

	\item[Réalisations] Étude et expérimentations des décompositions Candecomp/Parafac et Tensor Train, ainsi que du produit de Kronecker. L'étude de ce dernier nous a amené à faire un parallèle entre son fonctionnement et l'apprentissage de filtres par un réseau de neurone convolutif.
\end{description}

\end{frame}
%### PLAN ### 3 ########################
\begin{frame}
\frametitle{Plan}

\begin{itemize}
\item Exploration des décompositions Candecomp/Parafac et Tensor Train pour la compression de couches denses de réseaux de neurones.
\bigskip

\item Utilisation du produit de Kronecker pour compresser des couches denses.
\bigskip

\item Utilisation du produit de Kronecker comme alternative aux couches de convolution.
\end{itemize}{}

\end{frame}
\section{Décomposition CP et TT}
\subsection{Décomposition CP}
%### DECOMPOSITION CP ### 4 ########################
\begin{frame}
\frametitle{Décomposition CP}

La décomposition CP (pour Candecomp/Parafac) est une généralisation à de plus grandes dimensions de la décomposition en bas rang d'une matrice.
\bigskip

\begin{columns}[c]
    \begin{column}{4cm}
      \includegraphics[width=5cm]{CPdecomposition.png} \\
  \end{column}
  \begin{column}{4.5cm}
  La décomposition-CP de rang 1 approxime x en :
      \[x \approx a \circ  b \circ c\]
      ou $x(i, j, k) \approx a(i) \cdot   b(j) \cdot  c(k)$
  \end{column}
\end{columns}

\begin{block}{Pour un tenseur d'ordre p et une décomposition de rang R}
\begin{columns}[c]
    \begin{column}{4cm}
      \[T = \sum_{r=1}^{R} (a_{r}^{1} \circ \cdots  \circ a_{r}^{p})\]
  	\end{column}
    \begin{column}{4cm}
      $T \in \mathbb{R}^{d_{1} \times \cdots  \times d_{p}}$ \\
      $a_{r}^{i} \in \mathbb{R}^{d_{i}}$ \\
      $R \in \mathbb{N} \rightarrow $ rang-CP\\
  \end{column}
\end{columns}
\end{block}

\begin{columns}[c]
  \begin{column}{3cm}
  	Gain sur le nombre d'éléments:
  \end{column}
  \begin{column}{8cm}
    \begin{itemize}
    \setbeamertemplate{itemize item}[triangle]
    \item $(d_{1} \times \cdots \times d_{p})$ éléments (tenseur non compressé)
    \item $(d_{1} + \cdots + d_{p}) \times R$ éléments (tenseur compressé)
  \end{itemize}
  \end{column}
\end{columns}

\end{frame}
\subsection{Décomposition TT}
%### DECOMPOSITION TT ### 5 ########################
\begin{frame}
\frametitle{Décomposition TT}

\begin{block}{Pour un tenseur d'ordre p et une décomposition de rang $(R_{1}, \cdots , R_{p-1})$}
{\footnotesize
	\[ \tau(i_{1}, \cdots, i_{p}) = G_{1}(i_{1}) \times  G_{2}(i_{2}) \times \cdots \times G_{p}(i_{p}) \]
}
\end{block}

\begin{columns}[c]
  \begin{column}{7cm}
  	\includegraphics[width=7cm]{TTdecomposition.png}
  \end{column}
  \begin{column}{3cm}
    $\tau \in \mathbb{R}^{d_{1} \times \cdots \times d_{p}}$ \\
    $G_{1} \in \mathbb{R}^{d_{1} \times R_{1}}$ \\
    $G_{i} \in \mathbb{R}^{R_{i-1}  \times d_{i} \times R_{i}}$ \\
    $G_{p} \in \mathbb{R}^{R_{p-1} \times d_{p}}$
  \end{column}
\end{columns}

Le plus petit tuple $(r_{1}, \cdots, r_{p-1})$ tel que la décomposition existe est appelé rang-TT.
\bigskip

\begin{columns}[c]
  \begin{column}{3cm}
  	Gain sur le nombre d'éléments:
  \end{column}
  \begin{column}{8cm}
    \begin{itemize}
    \setbeamertemplate{itemize item}[triangle]
    \item $(d_{1} \times \cdots \times d_{p})$ éléments (tenseur non compressé)
    \item $\sum_{k=1}^{p} r_{k-1}d_{k}r_{k}$ éléments (tenseur compressé)
  \end{itemize}
  \end{column}
\end{columns}

\end{frame}
%### DECOMPOSITION DES PARAMETRES D'UNE COUCHE DENSE ### 6 ########################
\begin{frame}
\frametitle{Décomposition des paramètres d'une couche dense}

\begin{columns}[c]
  \begin{column}{4cm}
  	\includegraphics[width=4.5cm]{mlp_1hidenLayer.png}
  \end{column}
  \begin{column}{7cm}
    \begin{itemize}
      \setbeamertemplate{itemize item}[triangle]
      \item vecteur d'entrée de taille m (ici m = 9)
      \item couche dense de n neurones (ici n = 9)
      \item matrice des paramètres $W \in \mathbb{R}^{n \times m}$
      \item sortie $Y = W \times X$
    \end{itemize}
  \end{column}
\end{columns}
\begin{center}
	\includegraphics[width=7cm]{compress.png}
\end{center}


\end{frame}

\section{Expériences et Résultats}
\subsection{Données MNIST}

%### DONNEE MNIST ### 7 ########################
\begin{frame}[fragile]  
\frametitle{Données MNIST}
\framesubtitle{Étude de l'impact de la forme des tenseur d'entrée, de sortie et du rang}

\begin{columns}[c]
  \begin{column}{4cm}
  	\includegraphics[width=4cm]{CP_MNIST_accuracyByDecreasingOrder.png}
    \\ $CP-Layer$
    \bigskip
    
  	\includegraphics[width=4cm]{TT_MNIST_accSortedFromTopDown.png}
    \\ $TT-Layer$
  \end{column}
  \begin{column}{4cm}
      {\small 
      $\ \ $Architecture:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item CP-Layer ou TT-Layer
        \item DenseLayer(800)
        \item DenseLayer(10, 'softmax')
      \end{itemize}
      \bigskip
      
      \bigskip
      
      Grid Search:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item l'ensemble des formes possibles du tenseur d'entrée
        \item l'ensemble des formes possibles du tenseur de sortie
        \item l'ensemble des valeurs de rang $\in [4, 10]$
      \end{itemize}
    }
  \end{column}
  \begin{column}{4cm}
    {\small 
      $\ \ $Comparaison des résultats:
      \begin{itemize}
        \item Acc. dense layer : 97.66\% pour 718778 params
        \item Best acc. CP-Layer : \textbf{96.94\%} pour 183810 params dont \textbf{78792} pour la CP-Layer
        \item Best acc. TT-Layer : \textbf{97.9\%} pour 111332 params dont \textbf{6314} pour la TT-Layer
      \end{itemize}
    }
    \begin{center}
    	{\scriptsize acc = accuracy}
    \end{center}
  \end{column}
\end{columns}


\end{frame}

\subsection{Données CCDS}

%### DONNEE CCDS ### 8 ########################
\begin{frame}[fragile]  
\frametitle{Données CCDS}
\framesubtitle{Étude de l'impact de la forme des tenseur d'entrée, de sortie et du rang}

\begin{columns}[c]
  \begin{column}{4cm}
  	\includegraphics[width=4cm]{CP_CCDS_accSortedFromDownTop.png}
    \\ $CP-Layer$
    \bigskip
    
  	\includegraphics[width=4cm]{TT_CCDS_accSortedFromDownTop_InitUniform.png}
    \\ $TT-Layer$
  \end{column}
  \begin{column}{4cm}
      {\small 
      $\ \ $Architecture:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item CP-Layer ou TT-Layer
        \item DenseLayer(1024)
        \item DenseLayer(2125, 'identity')
      \end{itemize}
      \bigskip
      
      \bigskip
      
      Grid Search: \\
      {\tiny ($\sim 2400$ jeux tiré aléat.)}
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item l'ensemble des formes possibles du tenseur d'entrée
        \item l'ensemble des formes possibles du tenseur de sortie
        \item l'ensemble des valeurs de rang $\in [4, 10]$
      \end{itemize}
    }
  \end{column}
  \begin{column}{4cm}
    {\small 
      $\ \ $Comparaison des résultats:
      \begin{itemize}
        \item État de l'art : 0.7913
        \item Best err. CP-Layer : \textbf{0.9719} pour 183810 params dont \textbf{800} pour la CP-Layer
        \item Best err. TT-Layer : \textbf{0.7882} pour 142416 params dont \textbf{5830} pour la TT-Layer
      \end{itemize}
    }
    \begin{center}
    	{\scriptsize Tache de régression \\ erreur calculée avec formule RMSE}
    \end{center}
  \end{column}
\end{columns}

\end{frame}
\section{Kronecker}
\subsection{Définition}

%### PRODUIT DE KRONECKER ### 9 ########################
\begin{frame}
\frametitle{Produit de Kronecker}
\begin{block}{Produit de Kronecker classique}
  \[A \otimes B = \begin{pmatrix}
    a_{11}B & a_{12}B & \cdots  & a_{1m}B \\ 
    a_{21}B & \ddots  &  & \vdots \\ 
    \vdots  &  & \ddots  & \vdots \\ 
    a_{n1}B & \cdots  & \cdots  & a_{nm}B
  \end{pmatrix}\]
\end{block}

\only<1>{
\begin{block}{Approximation de rang 1 de la matrice M}
\begin{columns}[c]
    \begin{column}{5cm}
      \[M \approx A \otimes B\]
  	\end{column}
    \begin{column}{5cm}
      $A \in \mathbb{R}^{n \times m}$ \newline
      $B \in \mathbb{R}^{p \times q}$ \newline
      $M \in \mathbb{R}^{np \times mq}$
  \end{column}
\end{columns}
\end{block}
}

\only<2>{
\begin{block}{Approximation de rang K de la matrice M}
\begin{columns}[c]
    \begin{column}{5cm}
      \[M \approx  \sum_{k=1}^{K} A_{k} \otimes B_{k}\]
  	\end{column}
    \begin{column}{5cm}
      $A_{k} \in \mathbb{R}^{n \times m}$ \newline
      $B_{k} \in \mathbb{R}^{p \times q}$ \newline
      $M \in \mathbb{R}^{np \times mq}$
  \end{column}
\end{columns}
\end{block}
\bigskip

\begin{columns}[c]
  \begin{column}{3cm}
  	Gain sur le nombre d'éléments:
  \end{column}
  \begin{column}{8cm}
    \begin{itemize}
    \setbeamertemplate{itemize item}[triangle]
    \item $np \times mq$ éléments (tenseur non compressé)
    \item $(n \times m) + (p \times q)$ éléments (tenseur compressé)
  \end{itemize}
  \end{column}
\end{columns}
}

\end{frame}

%### UTILISATION ### 10 ########################
\begin{frame}
\frametitle{Utilisation}


La décomposition de Kronecker est liée à la structure de la matrice à décomposer.
\medskip

Exploration de deux pistes pour exploiter cette décomposition:
\begin{itemize}
  \setbeamertemplate{itemize item}[triangle]
  \item approximation d'une couche dense avec moins de paramètres
  \item définition d'une nouvelle couche cachée proche des couches convolutionnelles.
\end{itemize}

\end{frame}
\subsection{Approximation d'une couche dense avec moins de paramètres}

%### RECURTION DE LA DECOMPOSITION ### 11 ########################
\begin{frame}
\frametitle{Récursion de la décomposition}


\begin{center}
	\includegraphics[width=4cm]{decompWcompress.png}
\end{center}
\begin{block}{Décomposition réellement utilisée}
  \begin{columns}[c]
    \begin{column}{6cm}
      \[W \approx  \sum_{k=1}^{K} A_{k} \otimes B_{k}\]
      
      \[W \approx  \sum_{k=1}^{K} \overbrace{{A_{k}}' \otimes {A_{k}}''}^{A_{k}} \otimes \overbrace{{B_{k}}' \otimes {B_{k}}''}^{B_{k}}\]
    \end{column}
      \begin{column}{4cm}
      Avec : 
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item ${A_{k}}' \in \mathbb{R}^{{n}' \times {m}'}$
        \item ${A_{k}}'' \in \mathbb{R}^{{n}'' \times {m}''}$
        \item ${B_{k}}' \in \mathbb{R}^{{p}' \times {q}'}$
        \item ${B_{k}}''\in \mathbb{R}^{{p}'' \times {q}''}$
      \end{itemize}
      Ou : 
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item $n = {n}' \times {n}''$
        \item $m = {m}' \times {m}''$
        \item $p = {p}' \times {p}''$
        \item $q = {q}' \times {q}''$
      \end{itemize}
    \end{column}
  \end{columns}
\end{block}

\end{frame}

%### PERFORMANCE D'UN RESEAU DE NEURONES DE TYPE KD-LAYER ### 12 ########################
\begin{frame}[fragile]  
\frametitle{Performance d'un réseau de neurones de type KD-Layer}
\framesubtitle{Données MNIST}

\begin{columns}[c]
  \begin{column}{4cm}
  	\includegraphics[width=4cm]{KD-Layer-accSortedFromTopDown.png}
    \\ $KD-Layer$
  \end{column}
  \begin{column}{4cm}
      {\small 
      $\ \ $Architecture:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item CP-Layer ou TT-Layer
        \item DenseLayer(128)
        \item DenseLayer(10, 'softmax')
      \end{itemize}
      \bigskip
      
      \bigskip
      
      Grid Search: \\
      {\tiny ($\sim 2400$ jeux tiré aléat.)}
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item l'ensemble des forme possibles du tenseur d'entré
        \item l'ensemble des forme possibles du tenseur de sortie
        \item l'ensemble des valeurs de rang $\in [4, 10]$
      \end{itemize}
    }
  \end{column}
  \begin{column}{4cm}
    {\small 
      $\ \ $Comparaison des résultats:
      \begin{itemize}
        \item Acc. dense layer : 97.66\% pour 718778 params
        \item Best acc. KD-Layer : \textbf{98,54\%} pour 107188 paramètres dont \textbf{2170} pour la KD-Layer
        \item Best acc. CP-Layer : \textbf{96.94\%} pour 183810 params dont \textbf{78792} pour la CP-Layer
        \item Best acc. TT-Layer : \textbf{97.9\%} pour 111332 params dont \textbf{6314} pour la TT-Layer
      \end{itemize}
    }
    \begin{center}
    	{\scriptsize acc = accuracy}
    \end{center}
  \end{column}
\end{columns}
\end{frame}

%### NOUVELLE COUCHE CACHEE PROCHE DES COUCHES CONVOLUTIONNELLES ### 13 ########################
\subsection{Nouvelle couche cachée proche des couches convolutionnelles}
\begin{frame}
\frametitle{Définition d'une nouvelle couche cachée proche des couches convolutionnelles : la Kro-Layer}

\begin{enumerate}
    \begin{columns}[c]
      \begin{column}{6cm}
      	\begin{minipage}[c]{\linewidth}
          \item Approximation d'une couche dense avec moins de paramètres.
        \end{minipage}
      \end{column}
      \begin{column}{4.5cm}
        $X \rightarrow Y = \left ( \sum_{k=1}^{K} A_{k} \otimes B_{k} \right ) \times X$
      \end{column}
    \end{columns}
    \bigskip

    \begin{columns}[c]
      \begin{column}{6cm}
      	\begin{minipage}[c]{\linewidth}
          \item Définition d'une nouvelle couche cachée proche des couches convolutionnelles. L'approximation de Kronecker est vue comme une nouvelle représentation à l'aide d’une base de filtres.
        \end{minipage}
      \end{column}
      \begin{column}{4.5cm}
        $X \rightarrow $ $\left| \begin{matrix}
        				    Y = \left \{ A_{1}, \cdots, A_{K}  \right \} \\
                            $Avec $X = \sum_{k=1}^{K} A_{k} \otimes B_{k} \\
                            $Ou $B_{k}$ sont les filtres$ \\
                            $Et $A_{k}$ les valeurs $\\
                            $d'activations$\\
                          \end{matrix}\right.$
      \end{column}
    \end{columns}
\end{enumerate}
\bigskip

\begin{itemize}
\item[Kro-Layer] $X = \sum_{k=1}^{K} A_{k} \otimes B_{k}$
\item tache de classification
\item $B_{k}$ : filtres en mémoire
\item calculs et retour des $A_{k}$ à partir des données en entrée

\end{itemize}

\end{frame}

%### PERFORMANCES D'UN RESEAU DE NEURONES DE TYPE KRO-LAYER - Données MNIST - Architecture 1 ### 14 ########################
\begin{frame}[fragile]
\frametitle{Performance d'un réseau de neurones de type Kro-Layer}
\framesubtitle{Données MNIST - Architecture 1}

\begin{columns}[c]
  \begin{column}{3cm}
  	\includegraphics[width=3cm]{acc_CnnMnist1.png}
    \\ $Conv-Layer$
    \bigskip
    
  	\includegraphics[width=3cm]{acc_KroMnist1.png}
    \\ $Kro-Layer$
  \end{column}
  \begin{column}{2cm}
  	\includegraphics[width=1.5cm]{Cnn_mnist1.png}
    \\ $Conv-Layer$
    \bigskip
    
  	\includegraphics[width=1.5cm]{Kro_mnist1.png}
    \\ $Kro-Layer$
  \end{column}
  \begin{column}{4cm}
    {\small 
    
      $\ \ $Architecture 1:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item Conv-layer ou Kro-Layer
        \item Conv-layer 
        \item DenseLayer()
        \item DenseLayer(10, 'softmax')
      \end{itemize}
      \bigskip
    
      \bigskip
    
      $\ \ $Comparaison des résultats:
      \begin{itemize}
        \item Best acc. Conv-Layer : \textbf{98.51\%}
        \item Best acc. Kro-Layer : \textbf{98.36\%}
      \end{itemize}
    }
  \end{column}
\end{columns}

\end{frame}

%### PERFORMANCES D'UN RESEAU DE NEURONES DE TYPE KRO-LAYER - Données MNIST - Architecture 2 ### 14 ########################
\begin{frame}[fragile]
\frametitle{Performance d'un réseau de neurones de type Kro-Layer}
\framesubtitle{Données MNIST - Architecture 2}

\begin{columns}[c]
  \begin{column}{3cm}
  	\includegraphics[width=3cm]{acc_CnnMnist2.png}
    \\ {\footnotesize Conv-Layer -> Conv-Layer}
    \bigskip
    
  	\includegraphics[width=3cm]{acc_KroMnist2.png}
    \\ {\footnotesize Kro-Layer -> Kro-Layer}
  \end{column}
  \begin{column}{2cm}
  	\includegraphics[width=1.5cm]{Cnn_mnist2.png}
    \\ {\footnotesize Conv-L -> Conv-L}
    \bigskip
    
  	\includegraphics[width=1.5cm]{Kro_mnist2.png}
    \\ {\footnotesize Kro-L -> Kro-L}
  \end{column}
  \begin{column}{4cm}
    {\small 
    
      $\ \ $Architecture 2:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item Conv-layer ou Kro-Layer
        \item Conv-layer ou Kro-Layer
        \item DenseLayer()
        \item DenseLayer(10, 'softmax')
      \end{itemize}
      \bigskip
    
      \bigskip
    
      $\ \ $Comparaison des résultats:
      \begin{itemize}
        \item Best acc. Conv-Conv : \textbf{98.47\%}
        \item Best acc. Kro-Kro : \textbf{97.90\%}
      \end{itemize}
    }
  \end{column}
\end{columns}

\end{frame}

%### PERFORMANCES D'UN RESEAU DE NEURONES DE TYPE KRO-LAYER - Données MNIST - Architecture 1 ### 14 ########################
\begin{frame}[fragile]
\frametitle{Performance d'un réseau de neurones de type Kro-Layer}
\framesubtitle{Données CIFAR}

\begin{columns}[c]
  \begin{column}{1.7cm}
  {\footnotesize 
  Conv-L -> Conv-L \\
  \bigskip
  
  \bigskip
  
  \bigskip
  
  Conv-L -> Kro-L \\
  \bigskip
  
  \bigskip
  
  \bigskip
  
  Kro-L -> Conv-L \\
  \bigskip
  
  \bigskip
  
  \bigskip
  
  Kro-L -> Kro-L
  }
  \end{column}
  \begin{column}{3cm}
  	\includegraphics[width=2.2cm]{acc_CnnCnnCifar1.png} \\
  	\includegraphics[width=2.2cm]{acc_CnnKroCifar1.png} \\
  	\includegraphics[width=2.2cm]{acc_KroCnnCifar1.png} \\
  	\includegraphics[width=2.2cm]{acc_KroKroCifar1.png}
  \end{column}
  \begin{column}{2cm}
  	\includegraphics[width=1.5cm]{CnnCnn_cifar.png} \\
  \bigskip
  
  \bigskip
  
  	\includegraphics[width=1.5cm]{CnnKro_cifar.png} \\
  \bigskip
  
  \bigskip
  
  	\includegraphics[width=1cm]{KroCnn_cifar.png} \\
  \bigskip
  
  \bigskip
  
  	\includegraphics[width=1cm]{KroKro_cifar.png}
  \end{column}
  \begin{column}{4cm}
    {\small 
    
      $\ \ $Architecture 2:
      \begin{itemize}
        \setbeamertemplate{itemize item}[triangle]
        \item Conv-Layer ou Kro-Layer
        \item Conv-layer ou Kro-Layer
        \item Conv-Layer
        \item DenseLayer()
        \item DenseLayer(10, 'softmax')
      \end{itemize}
      \bigskip
    
      \bigskip
    
      $\ \ $Comparaison des résultats:
      \begin{itemize}
        \item Best acc. Conv-Conv : \textbf{60.62\%}
        \item Best acc. Conv-Kro : \textbf{57.64\%}
        \item Best acc. Kro-Conv : \textbf{55.61\%}
        \item Best acc. Kro-Kro : \textbf{56.37\%}
      \end{itemize}
    }
  \end{column}
\end{columns}

\end{frame}

%### CONCLUSION ### 14 ########################
\begin{frame}
\frametitle{Conclusion}

\begin{itemize}
  \setbeamertemplate{itemize item}[triangle]
  \item \textbf{Exploration CP} Convergence facile et résultats plutôt indépendants des reshapes.
  \bigskip

  \item \textbf{Exploration TT} Meilleurs résultats mais nécessite des réglages d'optimisation et un soin de l'initialisation.
  \bigskip

  \item \textbf{Kronecker pour compression} Résultats similaires à la décomposition TT et pas de reshapes préalable à faire.
  \bigskip

  \item \textbf{Kronecker alternative convolution} Résultats similaires à des réseaux de convolution. Prometteur car il y a encore aucune optimisation utilisée.
  \bigskip

  \bigskip


  \item \textbf{Perspectives} Tester au sein de réseaux plus profonds. \\ Tester avec peu de données.
\end{itemize}

\end{frame}
\end{document}




