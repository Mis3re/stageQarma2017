#!/usr/bin/env python

import numpy as np
import tt
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

T=np.load('W1_1024.npy')#numpy.ndarray 256*100
#T = np.zeros((256,100))
#for i in range(20):
#	T += np.random.rand(256,1).dot(np.random.rand(1,100))
permut = {}
err = {} 
nbParam = {}
mat = T
permut[0]=T
for i in range(1,7):

	P = np.random.permutation(T.shape[0]*T.shape[1])
	permut[i] = mat.flatten()[P].reshape((1024,1024))
	mat = permut[i]

for i in range(0,7):

	tensor = np.reshape(permut[i],(32,32,32,32))
	nbParam[i] = []
	err[i] = []
	for r in range(1,1024):

		approx = tt.tensor(tensor,1e-14,r)
		nbParam[i].append(approx.core.size)
		err[i].append(np.linalg.norm(tensor.flatten()-tt.tensor.full(approx).flatten()))


a, =plt.plot(nbParam[1],err[1], label="permutation 1")
b, =plt.plot(nbParam[2],err[2], label="permutation 2")
c, =plt.plot(nbParam[3],err[3], label="permutation 3")
d, =plt.plot(nbParam[4],err[4], label="permutation 4")
e, =plt.plot(nbParam[5],err[5], label="permutation 5")
f, =plt.plot(nbParam[6],err[6], label="permutation 6")
g, = plt.plot(nbParam[0],err[0], label="originale")

plt.legend(handles = [a, b, c, d, e, f, g])
plt.xlabel('Nombre de parametres (shape = (32,32,32,32))')
plt.ylabel('Norme de l erreur')
plt.savefig('test_1024_S1.png')


