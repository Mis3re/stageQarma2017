#### LORSQUE L'ON FINETUNE SEULEMENT LES COUCHES AU DESSUS ####

cp_input_shape=[49, 16], cp_output_shape=[16, 50], rank=2

param = 3168

Epoch 100 of 100 took 6.187s
  training loss:		1.528102
  validation loss:		1.513034
  validation accuracy:		43.13 %
Final results:
  test loss:			1.496543
  test accuracy:		42.48 %
  test error:		57.52 %
Total time 629.000s


cp_input_shape=[49, 16], cp_output_shape=[16, 50], rank=4

param = (6336,)

Epoch 100 of 100 took 6.781s
  training loss:		0.758703
  validation loss:		0.750830
  validation accuracy:		74.43 %
Final results:
  test loss:			0.766450
  test accuracy:		73.38 %
  test error:		26.62 %
Total time 696.776s

cp_input_shape=[49, 16], cp_output_shape=[16, 50], rank=8

param = (12672,)

Epoch 100 of 100 took 8.542s
  training loss:		0.195412
  validation loss:		0.217293
  validation accuracy:		93.11 %
Final results:
  test loss:			0.231151
  test accuracy:		92.45 %
  test error:		7.55 %
Total time 858.039s

cp_input_shape=[49, 16], cp_output_shape=[16, 50], rank=10

param = (15840,)

Epoch 100 of 100 took 18.229s
  training loss:		0.113041
  validation loss:		0.139751
  validation accuracy:		95.86 %
Final results:
  test loss:			0.148030
  test accuracy:		95.21 %
  test error:		4.79 %
Total time 1728.516s


cp_input_shape=[49, 16], cp_output_shape=[16, 50], rank=20

param = (31680,)

Epoch 100 of 100 took 24.686s
  training loss:		0.037566
  validation loss:		0.076966
  validation accuracy:		97.83 %
Final results:
  test loss:			0.070956
  test accuracy:		97.69 %
  test error:		2.31 %
Total time 2455.977s

cp_input_shape=[4, 49, 4], cp_output_shape=[5,32, 5], rank=2
param = (3216,)

Epoch 100 of 100 took 6.623s
  training loss:		0.336007
  validation loss:		0.319156
  validation accuracy:		91.04 %
Final results:
  test loss:			0.334598
  test accuracy:		90.10 %
  test error:		9.90 %
Total time 657.067s

cp_input_shape=[4, 49, 4], cp_output_shape=[5,32, 5], rank=4

param= 6432
Epoch 100 of 100 took 7.434s
  training loss:		0.226215
  validation loss:		0.214202
  validation accuracy:		94.05 %
Final results:
  test loss:			0.217451
  test accuracy:		93.74 %
  test error:		6.26 %
Total time 693.246s

cp_input_shape=[4, 49, 4], cp_output_shape=[5,32, 5], rank=8
(12864,)
Epoch 100 of 100 took 8.327s
  training loss:		0.158776
  validation loss:		0.155609
  validation accuracy:		95.75 %
Final results:
  test loss:			0.160063
  test accuracy:		95.36 %
  test error:		4.64 %
Total time 844.560s


cp_input_shape=[4, 49, 4], cp_output_shape=[5,32, 5], rank=10
(16080,)

Epoch 100 of 100 took 9.063s
  training loss:		0.146320
  validation loss:		0.148461
  validation accuracy:		95.86 %
Final results:
  test loss:			0.153929
  test accuracy:		95.42 %
  test error:		4.58 %
Total time 895.901s

cp_input_shape=[4, 49, 4], cp_output_shape=[5,32, 5], rank=20
(32160,)
Epoch 100 of 100 took 11.796s
  training loss:		0.100390
  validation loss:		0.114290
  validation accuracy:		96.88 %
Final results:
  test loss:			0.116876
  test accuracy:		96.77 %
  test error:		3.23 %
Total time 1180.837s

cp_input_shape=[7, 16, 7], cp_output_shape=[4,50, 4], rank=2
(1712,)
Epoch 100 of 100 took 6.515s
  training loss:		0.435897
  validation loss:		0.424121
  validation accuracy:		87.27 %
Final results:
  test loss:			0.440136
  test accuracy:		86.04 %
  test error:		13.96 %
Total time 650.555s

cp_input_shape=[7, 16, 7], cp_output_shape=[4,50, 4], rank=4

(3424,)
Epoch 100 of 100 took 6.787s
  training loss:		0.217342
  validation loss:		0.214684
  validation accuracy:		93.74 %
Final results:
  test loss:			0.226814
  test accuracy:		93.10 %
  test error:		6.90 %
Total time 678.448s

cp_input_shape=[7, 16, 7], cp_output_shape=[4,50, 4], rank=8
(6848,)
Epoch 100 of 100 took 8.442s
  training loss:		0.143594
  validation loss:		0.147806
  validation accuracy:		96.05 %
Final results:
  test loss:			0.151830
  test accuracy:		95.42 %
  test error:		4.58 %
Total time 844.711s

cp_input_shape=[7, 16, 7], cp_output_shape=[4,50, 4], rank=10
(8560,)
Epoch 100 of 100 took 9.003s
  training loss:		0.121976
  validation loss:		0.129750
  validation accuracy:		96.54 %
Final results:
  test loss:			0.131054
  test accuracy:		96.13 %
  test error:		3.87 %
Total time 901.114s

cp_input_shape=[7, 16, 7], cp_output_shape=[4,50, 4], rank=20
(17120,)

Epoch 100 of 100 took 11.847s
  training loss:		0.092455
  validation loss:		0.111988
  validation accuracy:		96.86 %
Final results:
  test loss:			0.109093
  test accuracy:		96.65 %
  test error:		3.35 %
Total time 1186.202s

cp_input_shape=[4,7,4,7], cp_output_shape=[5,5,8,4], rank=2

(230,)
Epoch 100 of 100 took 7.362s
  training loss:		0.736015
  validation loss:		0.703785
  validation accuracy:		78.43 %
Final results:
  test loss:			0.717537
  test accuracy:		78.09 %
  test error:		21.91 %
Total time 735.077s

cp_input_shape=[4,7,4,7], cp_output_shape=[5,5,8,4], rank=4
(460,)
Epoch 100 of 100 took 7.627s
  training loss:		0.488267
  validation loss:		0.452117
  validation accuracy:		87.14 %
Final results:
  test loss:			0.476312
  test accuracy:		86.13 %
  test error:		13.87 %
Total time 758.615s

cp_input_shape=[4,7,4,7], cp_output_shape=[5,5,8,4], rank=8
(920,)

Epoch 100 of 100 took 9.331s
  training loss:		0.337816
  validation loss:		0.305476
  validation accuracy:		91.46 %
Final results:
  test loss:			0.322607
  test accuracy:		90.90 %
  test error:		9.10 %
Total time 934.570s


cp_input_shape=[4,7,4,7], cp_output_shape=[5,5,8,4], rank=10
(1150,)

Epoch 100 of 100 took 8.915s
  training loss:		0.300692
  validation loss:		0.273461
  validation accuracy:		92.17 %
Final results:
  test loss:			0.290818
  test accuracy:		92.01 %
  test error:		7.99 %
Total time 890.330s

cp_input_shape=[4,7,4,7], cp_output_shape=[5,5,8,4], rank=20
(2300,)

Epoch 100 of 100 took 11.681s
  training loss:		0.231336
  validation loss:		0.215586
  validation accuracy:		94.16 %
Final results:
  test loss:			0.222863
  test accuracy:		93.57 %
  test error:		6.43 %
Total time 1168.240s

cp_input_shape=[2, 2, 7, 7, 2, 2], cp_output_shape=[2, 4, 5, 5, 2, 2], rank =2
(180,)

Epoch 100 of 100 took 6.695s
  training loss:		1.463765
  validation loss:		1.444763
  validation accuracy:		51.92 %
Final results:
  test loss:			1.449969
  test accuracy:		50.42 %
  test error:		49.58 %
Total time 668.194s

cp_input_shape=[2, 2, 7, 7, 2, 2], cp_output_shape=[2, 4, 5, 5, 2, 2], rank =4
(360,)

Epoch 100 of 100 took 6.984s
  training loss:		0.822947
  validation loss:		0.764497
  validation accuracy:		76.24 %
Final results:
  test loss:			0.787370
  test accuracy:		74.60 %
  test error:		25.40 %
Total time 696.328s

cp_input_shape=[2, 2, 7, 7, 2, 2], cp_output_shape=[2, 4, 5, 5, 2, 2], rank =8
(720,)

Epoch 100 of 100 took 7.747s
  training loss:		0.507503
  validation loss:		0.479318
  validation accuracy:		86.13 %
Final results:
  test loss:			0.488352
  test accuracy:		85.89 %
  test error:		14.11 %
Total time 772.277s

cp_input_shape=[2, 2, 7, 7, 2, 2], cp_output_shape=[2, 4, 5, 5, 2, 2], rank =10
(900,)
Epoch 100 of 100 took 9.373s
  training loss:		0.362304
  validation loss:		0.340059
  validation accuracy:		90.33 %
Final results:
  test loss:			0.341423
  test accuracy:		90.02 %
  test error:		9.98 %
Total time 937.825s

