import numpy as np
import matplotlib.pyplot as plt

nbParam = np.array([627200])
err = np.array([1.83])
time = np.array([3750.257])
#Pour [28,28] [25,32]
nbParam1 = np.array([3192, 6384, 12768, 15960, 31920])
rank1 = np.array([2, 4, 8, 10, 20])
err1 = np.array([4.68, 4.19, 3.22, 4.78, 4.00])
time1 = np.array([574.382, 703.438, 859.855, 921.236, 1462.416])
#Pour [4,49,4] [5,32,5]
nbParam2 = np.array([3216, 6432, 12864, 32160])
rank2 = np.array([2, 4, 8, 20])
err2 = np.array([4.17, 3.89, 4.19, 5.96])
time2 = np.array([495.626, 686.116, 907.706, 2247.096])
#Pour [7,16,7] [4,50,4]
nbParam3 = np.array([1712, 3424, 5136])
rank3 = np.array([2, 4, 6])
err3 = np.array([7.45, 4.01, 3.70])
time3 = np.array([652.764, 871.434, 653.851])
#Pour [4,7,4,7] [5,5,8,4]
nbParam4 = np.array([230, 460, 690, 920, 1150])
rank4 = np.array([2, 4, 6, 8, 10])
err4 = np.array([4.50, 4.09, 3.72, 3.75, 3.08])
time4 = np.array([598.316, 745.495, 905.474, 1061.506, 1217.990])
#Pour [2,2,7,7,2,2] [2,4,5,5,2,2]
nbParam5 = np.array([180,360,540, 720, 900])
rank5 = np.array([2, 4, 6, 8, 10])
err5 = np.array([7.08,4.71,4.00, 4.14, 3.97])
time5 = np.array([994.441,1482.919,2053.105, 2801.900, 2792.625])
#a, =plt.loglog(nbParam,err, 'ro', label="non compressee")
b, =plt.plot(rank1,err1, label="I=[28,28], O=[25,32]")
c, =plt.plot(rank2,err2, label="I=[4,49,4], O=[5,32,5]")
d, = plt.plot(rank3,err3, label="I=[7,16,7], O=[4,50,4]")
e, =plt.plot(rank4,err4,label="I=[4,7,4,7], O=[5,5,8,4]")
f, =plt.plot(rank5,err5,label="I=[2,2,7,7,2,2], O=[2,4,5,5,2,2]")
plt.legend(loc="lower right")
plt.yscale('log')

#plt.legend(handles = [a, b,c, d, e])
plt.xlabel('Rang canonique')
plt.ylabel('Erreur durant la phase de test (en %)')
plt.savefig('CP_rank_err.png')
plt.show()
