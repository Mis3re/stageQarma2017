#!/usr/bin/env python

'''
Compression de la matrice poids de la premiere couche du reseau de base + finetuning (de tous les parametres ou seulement ceux des couches au dessus)
Pour chacun des shapes le programme prend en argument le nombre d epoch et le rang canonique de la compression CP
'''

from __future__ import print_function

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne
from sktensor import ktensor, dtensor, cp_als
from cplayerCP import CPLayer


# ################## Telechargement et prepartion des donnees MNIST ##################
# On recupere les donnees en ligne et on les charge dans des numpy arrays.

def load_dataset():
    # fonction de telechargement
    if sys.version_info[0] == 2:
        from urllib import urlretrieve
    else:
        from urllib.request import urlretrieve

    def download(filename, source='http://yann.lecun.com/exdb/mnist/'):
        print("Downloading %s" % filename)
        urlretrieve(source + filename, filename)

    # fonctions pour le chargement des images MNIST et leur labels.
    import gzip

    def load_mnist_images(filename):
        if not os.path.exists(filename):
            download(filename)
       
        with gzip.open(filename, 'rb') as f:
            data = np.frombuffer(f.read(), np.uint8, offset=16)
        
        data = data.reshape(-1, 1, 28, 28)
       
        return data / np.float32(256)

    def load_mnist_labels(filename):
        if not os.path.exists(filename):
            download(filename)

        with gzip.open(filename, 'rb') as f:
            data = np.frombuffer(f.read(), np.uint8, offset=8)

        return data

    # Telechargement et lecture des ensembles d images et labels des bases de training et test.
    X_train = load_mnist_images('train-images-idx3-ubyte.gz')
    y_train = load_mnist_labels('train-labels-idx1-ubyte.gz')
    X_test = load_mnist_images('t10k-images-idx3-ubyte.gz')
    y_test = load_mnist_labels('t10k-labels-idx1-ubyte.gz')

    # Les 100000 derniers exemples de training sont reserves pour la phase de validation.
    X_train, X_val = X_train[:-10000], X_train[-10000:]
    y_train, y_val = y_train[:-10000], y_train[-10000:]

   
    return X_train, y_train, X_val, y_val, X_test, y_test


# ##################### Construction du modele pour le Fine-tuning #######################


def build_mlp_compressed(W, b, r, input_var=None):
    net={}
    #Input layer
    net['l_in'] = lasagne.layers.InputLayer(shape=(None, 1, 28, 28),
                                     input_var=input_var)
    
    # CP-Layer
    net['l_hid1'] = CPLayer(net['l_in'],
                            cp_input_shape=[2,2,7,7,2,2],
                            cp_output_shape=[2,4,5,5,2,2],
                            rank=r, cores = W['1'], b = b['1'],
                            nonlinearity=lasagne.nonlinearities.rectify)

    param_values = net['l_hid1'].cores_arr.get_value()
    print(param_values.shape)

    # Couche fully-connected suivie d'une ReLU 
    net['l_hid2'] = lasagne.layers.DenseLayer(net['l_hid1'], num_units=800,
            nonlinearity=lasagne.nonlinearities.rectify)
    
    # Couche de sortie fully-connected suivie d'une softmax
    net['l_out'] = lasagne.layers.DenseLayer(
            net['l_hid2'], num_units=10,
            nonlinearity=lasagne.nonlinearities.softmax)
   
    return net

# ############################# Fonction pour mini-batches ###############################
#Fonction qui parcourt les elements de la base d apprentissage en mini-batches de taille 
#particuliere, dans un ordre aleatoire ou non.


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


#Fonction pour les phases de training et test
def train_test(input_var, target_var, net, num_epochs):

    # Chargement des donnees
    print("Loading data...")
    X_train, y_train, X_val, y_val, X_test, y_test = load_dataset()

    # Creation de la fonction loss pour la phase de training -> cross-entropy loss:
    prediction = lasagne.layers.get_output(net['l_out'])
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    loss = loss.mean()

    # Creation de la fonction de mise a jour pour la phase de training -> Stochastic Gradient
    # Descent (SGD) with Nesterov momentum (remplacer les parametres de net['l_hid1'] par [0,0]

    learning_rates = { net['l_in']: [0.01, 0.9], net['l_hid1']: [0.01, 0.9], net['l_hid2']: [0.01, 0.9], net['l_out']: [0.01,0.9]}
    updates = {}
    for layer, learning_rate in learning_rates.items():
    	updates.update(lasagne.updates.nesterov_momentum(loss, layer.get_params(trainable=True), learning_rate[0], learning_rate[1] ))

    # Creation de la fonction loss  pour les phases de validation/test
    test_prediction = lasagne.layers.get_output(net['l_out'], deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    # Expression pour la precision de la classification :
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

    # Compilation d une fonction performant une etape de training sur un mini-batch (en donnant
    # le dictionnaire des mises a jour) et retournant les training loss correspondant:
    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    # Compilation d une seconde fonction calculant la validation loss et la precision:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    # Boucle pour la phase de training.
    print("Starting training...")
    start=time.time()
    # Pour chacune des epochs:
    for epoch in range(num_epochs):
        # on itere sur toute la base de training : 
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, y_train, 500, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1

        # et toute la base de validation:
        val_err = 0
        val_acc = 0
        val_batches = 0
        for batch in iterate_minibatches(X_val, y_val, 500, shuffle=False):
            inputs, targets = batch
            err, acc = val_fn(inputs, targets)
            val_err += err
            val_acc += acc
            val_batches += 1

        #Affichage des resultats pour chaque epoch:
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
        print("  validation accuracy:\t\t{:.2f} %".format(
            val_acc / val_batches * 100))

    

    # Apres la phase de training, on calcule et affiche l erreur pour la phase de test:
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in iterate_minibatches(X_test, y_test, 500, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.2f} %".format(
        test_acc / test_batches * 100))
    print("  test error:\t\t{:.2f} %".format(100 - (test_acc / test_batches *100)))

    # Affichage du temps total
    print("Total time {:.3f}s".format(time.time() - start))

# ############################## Programme principal ################################
def main(r, num_epochs=100):
    #Chargement des parametres du reseau de base 
    W={}
    b={}
    W1=np.load('../../reseau_de_base/poids/W_layer1.npy')
    b1=np.load('../../reseau_de_base/poids/b_layer1.npy')
    W2=np.load('../../reseau_de_base/poids/W_layer2.npy')
    b2=np.load('../../reseau_de_base/poids/b_layer2.npy')
    W3=np.load('../../reseau_de_base/poids/W_layer3.npy')
    b3=np.load('../../reseau_de_base/poids/b_layer3.npy')
    P, fit, itr, exectimes = cp_als(dtensor(np.reshape(W1,(4,8,35,35,4,4))), r,init='random')
    
    cores_arr = []
    cores_arr = np.hstack((P.U[0].flatten(), P.U[1].flatten(), P.U[2].flatten(),P.U[3].flatten(),P.U[4].flatten(),P.U[5].flatten()))
    W['1'] = cores_arr
    b['1'] = b1
    W['2'] = W2
    b['2'] = b2
    W['3'] = W3
    b['3'] = b3
    
    # Variables Theano pour les inputs et targets
    input_c = T.tensor4('inputs')
    target_c = T.ivector('targets')
    # Fine-tuning
    net_comp = build_mlp_compressed( W , b, r, input_c)
    train_test(input_c, target_c, net_comp, num_epochs)

if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [EPOCHS]" % sys.argv[0])
        print()
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['num_epochs'] = int(sys.argv[1])
	    kwargs['r'] = int(sys.argv[2])
        main(**kwargs)
