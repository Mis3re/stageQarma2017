import numpy as np
import matplotlib.pyplot as plt

#### LORSQUE L'ON FINETUNE SEULEMENT LES COUCHES AU DESSUS ####

nbParam = np.array([627200])
err = np.array([1.83])
time = np.array([3750.257])
#Pour [49,16] [16,50]
nbParam1 = np.array([3168, 6336, 12672, 15840, 31680])
rank1 = np.array([2, 4, 8, 10, 20])
err1 = np.array([57.52, 26.62,7.55, 4.79, 2.31])
time1 = np.array([629.000, 696.776, 858.039, 1728.516, 2455.977])
#Pour [4,49,4] [5,32,5]
nbParam2 = np.array([3216, 6432, 12864, 16080, 32160])
rank2 = np.array([2, 4, 8, 10, 20])
err2 = np.array([9.90, 6.26, 4.64, 4.58, 3.23])
time2 = np.array([657.067, 693.246, 844.560, 895.901, 1180.837])
#Pour [7,16,7] [4,50,4]
nbParam3 = np.array([1712, 3424, 6848, 8560, 17120])
rank3 = np.array([2, 4, 8, 10, 20])
err3 = np.array([13.96, 6.90, 4.58, 3.87, 3.35])
time3 = np.array([650.555, 678.448, 844.711, 901.114, 1186.202])
#Pour [4,7,4,7] [5,5,8,4]
nbParam4 = np.array([230, 460, 920, 1150, 2300])
rank4 = np.array([2, 4, 8, 10, 20])
err4 = np.array([21.91, 13.87, 9.10, 7.99, 6.43])
time4 = np.array([735.077, 758.615, 934.570, 890.330, 1168.240])
#Pour [2,2,7,7,2,2] [2,4,5,5,2,2]
nbParam5 = np.array([180, 360, 720, 900])
rank5 = np.array([2, 4, 8, 10])
err5 = np.array([49.58, 25.40, 14.11, 9.98])
time5 = np.array([668.194, 696.328, 772.277, 937.825])
#a, =plt.loglog(nbParam,err, 'ro', label="non compressee")
b, =plt.plot(rank1,err1, label="[784, 800]")
c, =plt.plot(rank2,err2, label="[20, 1568, 20]")
d, = plt.plot(rank3,err3, label="[28, 800, 28]")
e, =plt.plot(rank4,err4,label="[20, 35, 32, 28]")
f, =plt.plot(rank5,err5,label="[4, 8, 35, 35, 4, 4]")
plt.legend(loc="upper right")
plt.yscale('log')
#plt.legend(handles = [a, b,c, d, e])
plt.xlabel('Rang canonique')
plt.ylabel('Erreur durant la phase de test (en %)')
plt.title('Finetuning des couches au-dessus')
plt.savefig('CP_rank_err_f2.png')
plt.show()
