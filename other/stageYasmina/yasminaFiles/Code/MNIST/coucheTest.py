#!/usr/bin/env python

'''
Reseau contenant une couche d entree (images MNIST 16*16), une couche cachee fully-connected (100 neurones) et une couche de sortie fully-connected (10 neurones).
Ce reseau a ete utilise afin d effectue des tests sur la matrice poids de la couche cachee fully-connected.
'''
from __future__ import print_function

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne



# ################## Telechargement et prepartion des donnees MNIST ##################
# On recupere les donnees en ligne et on les charge dans des numpy arrays. 
 

def load_dataset():
    # fonction de telechargement
    if sys.version_info[0] == 2:
        from urllib import urlretrieve
    else:
        from urllib.request import urlretrieve

    def download(filename, source='http://yann.lecun.com/exdb/mnist/'):
        print("Downloading %s" % filename)
        urlretrieve(source + filename, filename)

    # fonctions pour le chargement des images MNIST et leur labels.    
    import gzip

    def load_mnist_images(filename):
        if not os.path.exists(filename):
            download(filename)

        with gzip.open(filename, 'rb') as f:
            data = np.frombuffer(f.read(), np.uint8, offset=16)
        # Images MNIST sous-echantillonnees       
        data = data.reshape(-1, 1, 28, 28)
        data = np.pad(data,((0,0),(0,0),(2,2),(2,2)),'constant')
	X=data[:,:,1:32:2,1:32:2]
        return X / np.float32(256)

    def load_mnist_labels(filename):
        if not os.path.exists(filename):
            download(filename)

        with gzip.open(filename, 'rb') as f:
            data = np.frombuffer(f.read(), np.uint8, offset=8)

        return data

    # Telechargement et lecture des ensembles d images et labels des bases de training et test.
    X_train = load_mnist_images('train-images-idx3-ubyte.gz')
    y_train = load_mnist_labels('train-labels-idx1-ubyte.gz')
    X_test = load_mnist_images('t10k-images-idx3-ubyte.gz')
    y_test = load_mnist_labels('t10k-labels-idx1-ubyte.gz')

    # Les 100000 derniers exemples de training sont reserves pour la phase de validation.
    X_train, X_val = X_train[:-10000], X_train[-10000:]
    y_train, y_val = y_train[:-10000], y_train[-10000:]


    return X_train, y_train, X_val, y_val, X_test, y_test


# ##################### Construction du modele #######################


def build_mlp(input_var=None):
    # MLP de 1 couches cachees contenant 100 neurones suivie d'une couche de sortie contenant 10 neurones. 
    net={}
    
    net['l_in'] = lasagne.layers.InputLayer(shape=(None, 1, 16, 16),
                                     input_var=input_var)
    
    # Couche fully-connected suivie d'une ReLU
    net['l_hid1'] = lasagne.layers.DenseLayer(
            net['l_in'], num_units=100,
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform(), b=None)

    # Couche de sortie fully-connected suivie d'une softmax
    net['l_out'] = lasagne.layers.DenseLayer(
            net['l_hid1'], num_units=10,
            nonlinearity=lasagne.nonlinearities.softmax)
    
    return net




# ############################# Fonction pour mini-batches ###############################
#Fonction qui parcourt les elements de la base d apprentissage en mini-batches de taille 
#particuliere, dans un ordre aleatoire ou non.


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


# ############################## Programme principal ################################


def main(num_epochs=500):
    # Chargement des donnees
    print("Loading data...")
    X_train, y_train, X_val, y_val, X_test, y_test = load_dataset()

    # Preparation des variables Theano pour les inputs et targets
    input_var = T.tensor4('inputs')
    target_var = T.ivector('targets')

    # Creation du modele
    print("Building model and compiling functions...")  
    network = build_mlp(input_var)

    # Creation de la fonction loss pour la phase de training -> cross-entropy loss:
    prediction = lasagne.layers.get_output(network['l_out'])
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    loss = loss.mean()
    # Creation de la fonction de mise a jour pour la phase de training -> Stochastic Gradient
    # Descent (SGD) with Nesterov momentum.
    learning_rates = { network['l_in']: 0.01, network['l_hid1']: 0.01, network['l_out']: 0.01}
    updates = {}
    for layer, learning_rate in learning_rates.items():
    	updates.update(lasagne.updates.nesterov_momentum(loss, layer.get_params(trainable=True), learning_rate, momentum=0.9))

    # Creation de la fonction loss  pour les phases de validation/test
    test_prediction = lasagne.layers.get_output(network['l_out'], deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    # expression pour la precision de la classification :
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

    # Compilation d une fonction performant une etape de training sur un mini-batch (en donnant
    # le dictionnaire des mises a jour) et retournant les training loss correspondant:    
    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    # Compilation d une seconde fonction calculant la validation loss et la precision:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    # Boucle pour la phase de training.
    print("Starting training...")
    start=time.time()
    #Pour chaque epoch:
    for epoch in range(num_epochs):
        # on itere sur toute la base de training : 
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, y_train, 500, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1

        # et toute la base de validation:
        val_err = 0
        val_acc = 0
        val_batches = 0
        for batch in iterate_minibatches(X_val, y_val, 500, shuffle=False):
            inputs, targets = batch
            err, acc = val_fn(inputs, targets)
            val_err += err
            val_acc += acc
            val_batches += 1

        #Affichage des resultats pour chaque epoch:
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
        print("  validation accuracy:\t\t{:.2f} %".format(
            val_acc / val_batches * 100))

    # Apres la phase de training, on calcule et affiche l erreur pour la phase de test:
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in iterate_minibatches(X_test, y_test, 500, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.2f} %".format(
        test_acc / test_batches * 100))

    # Affichage du temps total
    print("Total time {:.3f}s".format(time.time() - start))
    


   ####SAVING FILES###
    #print("Saving parameters ...")
    #np.save('W1_256-100.npy',network['l_hid1'].W.get_value())
    #np.save('b1.npy',network['l_hid1'].b.get_value())
    #np.save('W2.npy',network['l_out'].W.get_value())
    #np.save('b2.npy',network['l_out'].b.get_value())
    #print("Done ! ")


if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [MODEL [EPOCHS]]" % sys.argv[0])
        print()
        print("MODEL: 'mlp' for a simple Multi-Layer Perceptron (MLP),")
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['num_epochs'] = int(sys.argv[1])
        main(**kwargs)



