#!/usr/bin/env python


from __future__ import print_function

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

from ttlayerTT import TTLayer
import tt
import meto_data as md

'''
Compression de la matrice poids de la premiere couche du reseau de base + finetuning (de tous les parametres ou seulement ceux des couches au dessus)
Pour chacun des shapes le programme prend en argument le nombre d epoch et le rang max de la compression Tensor-Train
'''

# ##################### Construction du modele (pour le finetuning) #######################


def build_mlp_compressed(W, b, tt_r, input_var=None):
    
    net={}
    
    #Input layer
    net['l_in'] = lasagne.layers.InputLayer(shape=(None, 3, 15, 56),
                                     input_var=input_var)

    # TTlayer (prenant en parametre la matrice poids compressee avec les TT_ranks)
    net['l_hid1'] = TTLayer(
            net['l_in'], tt_input_shape=[3,15,28,2], tt_output_shape=[4,8,8,4], tt_ranks=tt_r, cores = W['1'], b = b['1'], nonlinearity=lasagne.nonlinearities.rectify)

    print('Nombres de parametre : {!s}'.format(net['l_hid1'].cores_arr.get_value().shape)) 
    
    #Fully_connected
    net['l_hid2'] = lasagne.layers.DenseLayer(
            net['l_hid1'], num_units=1024, W = W['2'], b = b['2'],
            nonlinearity=lasagne.nonlinearities.rectify)

    #Fully_connected
    net['l_out'] = lasagne.layers.DenseLayer(
            net['l_hid2'], num_units= 840, W = W['3'], b = b['3'],
            nonlinearity=lasagne.nonlinearities.identity)

  
    return net

# ############################# Fonction pour minibatchs ###############################


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


# ############################## Programme principal ################################


def train_test(input_var, target_var, net, num_epochs):

    #Chargement des donnees
    print("Loading data...")
    
    X_train = np.load('../10_jeux_de_donnees/X_train.npy')
    Y_train = np.load('../10_jeux_de_donnees/Y_train.npy')
    X_test = np.load('../10_jeux_de_donnees/X_test.npy')
    Y_test = np.load('../10_jeux_de_donnees/Y_test.npy')

    #Loss expression        
    prediction = lasagne.layers.get_output(net['l_out'])
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()

    #Mise a jours des poids couches par couches (remplacer le premier parametre 0.001 par 0 de net['l_hid1'] pour le finetuning des couches au-dessus uniquement)
    learning_rates = { net['l_in']: [0.001, 0.9, 0.999, 1e-08], net['l_hid1']: [0.001, 0.9, 0.999, 1e-08], net['l_hid2']: [0.001, 0.9, 0.999, 1e-08], net['l_out']: [0.001, 0.9, 0.999, 1e-08]}
    updates = {}
    for layer, learning_rate in learning_rates.items():
    	updates.update(lasagne.updates.adam(loss, layer.get_params(trainable=True), learning_rate[0], learning_rate[1], learning_rate[2], learning_rate[3] ))

    #Test loss
    test_prediction = lasagne.layers.get_output(net['l_out'], deterministic=True)
    test_loss = lasagne.objectives.squared_error(test_prediction,  target_var)
    test_loss = test_loss.mean()

    #Test accuracy (RMSE)
    norm = T.sum((test_prediction-target_var)**2)**0.5
    test_acc = T.sqrt(norm**2/target_var.size)

    train_fn = theano.function([input_var, target_var], loss, updates=updates)
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])


    print("Starting training...")
    start = time.time()
    for epoch in range(num_epochs):

        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, Y_train, 2, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1



        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))

    #PHASE DE TEST    
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in iterate_minibatches(X_test, Y_test, 2, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
  	test_acc += acc
        test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.6f}".format(test_acc / test_batches))   
    
    print("Total time {:.3f}s".format(time.time() - start))

def main(r, num_epochs=100):
    
      
    #Chargement des parametres du reseau de base 
    W={}
    b={}
    W1=np.load('../poids/W_layer1.npy')
    b1=np.load('../poids/b_layer1.npy')
    W2=np.load('../poids/W_layer2.npy')
    b2=np.load('../poids/b_layer2.npy')
    W3=np.load('../poids/W_layer3.npy')
    b3=np.load('../poids/b_layer3.npy')

    #compression de la matrice poids de la premiere couche du reseau de base
    param_tensor = W1.copy().reshape((3,15,28,2,4,8,8,4)).transpose((0,4,1,5,2,6,3,7)).reshape((12,120,224,8))
    mytensor = tt.tensor(param_tensor,1e-14,r)
    cores = tt.tensor.to_list(mytensor)
    cores_arr=[]
    #Ajout des cores suivant le nombre de dimension du tenseur
    cores_arr = np.hstack((cores[0].flatten(), cores[1].flatten(), cores[2].flatten(),cores[3].flatten()))#,cores[4].flatten(),cores[5].flatten()))
    #Remplacement de la matrice poids par son approximation TT, cores_arr
    W['1'] = cores_arr
    b['1'] = b1
    W['2'] = W2
    b['2'] = b2
    W['3'] = W3
    b['3'] = b3

    tt_ranks = mytensor.r
    
    input_c = T.dtensor4('inputs')
    target_c = T.dmatrix('targets')
    #Finetuning
    net_comp = build_mlp_compressed( W , b, tt_ranks, input_c)    
    train_test(input_c, target_c, net_comp, num_epochs)
   


if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [EPOCHS]" % sys.argv[0])
        print()
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['num_epochs'] = int(sys.argv[1])
	    kwargs['r'] = int(sys.argv[2])
        main(**kwargs)
