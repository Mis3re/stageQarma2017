#!/usr/bin/env python


from __future__ import print_function

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

from cplayer import CPLayer
import meto_data as md

'''
Reseau similaire au reseau de base, la premier couche Fully_connected est remplacee par une CPLayer.
Le programme prend 4 arguments : le nombre d epochs, vecteur des dimensions du tenseur representant l entree de la CPLayer (cp_input_shape), 
vecteur des dimensions du tenseur representant la sortie de la CPLayer (cp_output_shape) et le rang canonique (rank).

Ex:

python foursquarewCPLayer 100 '3,15,56' '4,64,4' 2

'''

# ##################### Construction du modele #######################

def build_mlp(cp_input_shape, cp_output_shape, rank, input_var=None):
    
    net={}
    
    #Input layer    
    net['l_in'] = lasagne.layers.InputLayer(shape=(None, 3, 15, 56),
                                     input_var=input_var)

    #CPLayer
    net['l_hid1'] = CPLayer(
            net['l_in'], cp_input_shape, cp_output_shape,
            rank,
            nonlinearity=lasagne.nonlinearities.rectify)

    print("Nbre de parametres:\t{!s}".format(net['l_hid1'].cores_arr.get_value().shape))
    
    #Fully_connected
    net['l_hid2'] = lasagne.layers.DenseLayer(
            net['l_hid1'], num_units=1024,
            nonlinearity=lasagne.nonlinearities.rectify)

    #Fully_connected
    net['l_out'] = lasagne.layers.DenseLayer(
            net['l_hid2'], num_units= 840,
            nonlinearity=lasagne.nonlinearities.identity)

  
    return net


# ############################# Fonction pour minibatchs ###############################


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


# ############################## Programme principal ################################


def main(cp_input_shape, cp_output_shape, rank, num_epochs=500):

     #Chargement des donnees
    print("Loading data...")
    X_train = np.load('../10_jeux_de_donnees/X_train.npy')
    Y_train = np.load('../10_jeux_de_donnees/Y_train.npy')
    X_test = np.load('../10_jeux_de_donnees/X_test.npy')
    Y_test = np.load('../10_jeux_de_donnees/Y_test.npy')


    input_var = T.dtensor4('inputs')
    target_var = T.dmatrix('targets')

    #Mise en forme des arguments
    cp_input_shape=cp_input_shape.split(',')
    cp_output_shape=cp_output_shape.split(',')
    cp_input_shape=np.asarray(map(int,cp_input_shape))
    cp_output_shape=np.asarray(map(int,cp_output_shape))
    
    
    print("{!s}\t{!s}\t{!s}".format(cp_input_shape,cp_output_shape,rank))
    print("Building model and compiling functions...")
    network = build_mlp(cp_input_shape, cp_output_shape, rank, input_var)

    #Loss expression  
    prediction = lasagne.layers.get_output(network['l_out'])
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()

    #Mise a jours des poids
    params = lasagne.layers.get_all_params(network['l_out'], trainable=True)
    updates = lasagne.updates.nesterov_momentum(
            loss, params, learning_rate=0.01, momentum=0.9)

    #Test loss
    test_prediction = lasagne.layers.get_output(network['l_out'], deterministic=True)
    test_loss = lasagne.objectives.squared_error(test_prediction,
target_var)
    test_loss = test_loss.mean()

    #Test accuracy (RMSE)
    norm = T.sum((test_prediction-target_var)**2)**0.5
    test_acc = T.sqrt(norm**2/target_var.size)
    

    train_fn = theano.function([input_var, target_var], loss, updates=updates)
    val_fn = theano.function([input_var, target_var], [test_loss,test_acc])


    print("Starting training...")
    start = time.time()
    for epoch in range(num_epochs):

        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, Y_train, 10, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1


	if(epoch == num_epochs -1):
            print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))

    #PHASE DE TEST  
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in iterate_minibatches(X_test, Y_test, 10 , shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.6f} ".format(
        test_acc / test_batches))
    
    print("Total time {:.3f}s".format(time.time() - start))

   


if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [EPOCHS]" % sys.argv[0])
        print()
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['num_epochs'] = int(sys.argv[1])
	    kwargs['cp_input_shape'] = sys.argv[2]
	    kwargs['cp_output_shape'] = sys.argv[3]
	    kwargs['rank'] = int(sys.argv[4])
        main(**kwargs)
