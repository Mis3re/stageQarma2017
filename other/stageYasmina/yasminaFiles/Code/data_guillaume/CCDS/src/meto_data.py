#! /usr/bin/env python
# -*- coding: utf-8 -*-

import scipy.io
import numpy as np
import sktensor as skt
import random
"""
USHCN.mat : dic 
keys : '__globals__' ,'__header__' , '__version__', 'data_series', 'locations',
'variable_names'

fsq_norm.mat : dic
keys : '__globals__' ,'__header__' , '__version__', 'series'

"""
def load_USHCN():
    data = scipy.io.loadmat('data/meteo_rose/USHCN.mat')['data_series']#shape=(17,1)
    T = skt.dtensor([M for M in data[:,0]])#shape=(17,125,156)
    var,loc,time = T.shape
    T = T.unfold(2).T
    T.ten_shape = (time,var,loc)
    return T.fold()#shape=(156,17,125)
   

	
"""
def load_brain():
    data = scipy.io.loadmat('data/MRIdata.mat')
    T = skt.dtensor(data['I0'])
    return T.transpose([2,0,1])


def load_foursquare():
    data = scipy.io.loadmat('data/foursquare/fsq_norm.mat')['series']#shape=(15,1)
    T = skt.dtensor([M for M in data[:,0]])#shape=(15,121,1200)
    for i,j in np.ndindex(15,121):
        mask = np.isnan(T[i,j,:])#true if is nan
        if mask.any() and (not mask.all()): #if there is any true or not any
            T[i,j,:] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), T[i,j,~mask])
#np.flatnonzero(mask) : array contenant les indices qui sont des true ds la 
#version applatie
        if mask.all():
            T[i,j,:] = np.zeros(1200)
    var,loc,time = T.shape
    #T = T.unfold(2).T
    #T.ten_shape = (time,var,loc)
    return T.transpose([2,0,1])#shape=(1200,15,56)

"""
def load_foursquare():
    data = scipy.io.loadmat('data/foursquare/foursquare.mat')['series_obs']
    T = skt.dtensor([M for M in data[:,0]])
    """
    for i,j in np.ndindex(15,121):
        mask = np.isnan(T[i,j,:])
        if mask.any() and (not mask.all()):
            T[i,j,:] = np.interp(np.flatnonzero(mask), np.flatnonzero(~mask), T[i,j,~mask])
        if mask.all():
            T[i,j,:] = np.zeros(1200)
    var,loc,time = T.shape
    #T = T.unfold(2).T
    #T.ten_shape = (time,var,loc)
    """
    
    noise = {}
    data_noise = skt.dtensor(np.zeros((1200,15,56)))
    d = T.transpose([2,0,1])
    #for i in range (7):
    #	noise[i] = skt.dtensor(np.random.normal(0,1,840))
    #	for j in range (1200):
    #data_noise[j,:,:] = d[j,:,:]+np.reshape(noise[i],(15,56))
	
    #	all_data = np.concatenate((d, data_noise), axis=0)
    #	d = all_data
    #return all_data
    return T.transpose([2,0,1])#shape=(1200,15,56)


def load_UK():
    data = scipy.io.loadmat('data/meteo_uk/meteo_tensor.dat')
    return data['data_squeeze'][0,0][0] #shape=(492,16,5)



def forecasting_data(data, N_train, N_test, n_lag, n_pred, random = True):
    """
	Splits the data tensor [data] into a training and testing set. Then tensor data is of order 3 : TIME x VARIABLES x LOCATIONS. The training and test data contain tuples (X,Y) where X is a tensor of size n_lag x VAR x LOC and Y is aa tensor of size n_pred x VAR x LOC. X contains the values of all variables in all locations in the preceeding n_lag time steps and Y the ones for the following n_pred time steps. 
	
    """
    X = []
    Y = []
    for i in range(data.shape[0] - n_pred - n_lag + 1):
        X.append(data[i:i+n_lag,:,:])
        Y.append(data[i+n_lag:i+n_lag+n_pred,:,:])

    print N_train, N_test, len(X)
    assert  N_train + N_test <= len(X), "not enough data..."

    if random:
        idx = np.random.permutation(len(X))
    else:
        idx = range(len(X))

    X_train = skt.dtensor([X[i] for i in idx[0:N_train]])
    X_test  = skt.dtensor([X[i] for i in idx[-N_test:]])

    Y_train = skt.dtensor([Y[i] for i in idx[0:N_train]]).squeeze()
    Y_test  = skt.dtensor([Y[i] for i in idx[-N_test:]]).squeeze()

    return (X_train,Y_train,X_test,Y_test)
