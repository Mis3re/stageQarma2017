#!/usr/bin/env python


from __future__ import print_function

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

from ttlayer import TTLayer
import meto_data as md

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

import cPickle

# ##################### Construction du modele #######################


def build_mlp(tt_input_shape, tt_output_shape, tt_ranks, input_var=None):
    
    net={}
    
    #Couche d'entree FULLY-CONNECTED
    net['l_in'] = lasagne.layers.InputLayer(shape=(None, 3, 15,56),
                                     input_var=input_var)

    #TTLayer
    net['l_hid1'] = TTLayer(
           net['l_in'], tt_input_shape, tt_output_shape, tt_ranks, nonlinearity=lasagne.nonlinearities.rectify)
    print("Nombre de parametres:\t{!s}".format(net['l_hid1'].cores_arr.get_value().shape))

    #FULLY-CONNECTED
    net['l_hid2'] = lasagne.layers.DenseLayer(
            net['l_hid1'], num_units=1024, nonlinearity=lasagne.nonlinearities.rectify)

    #Couche de sortie FULLY-CONNECTED
    net['l_out'] = lasagne.layers.DenseLayer(
            net['l_hid2'], num_units= 840 , nonlinearity=lasagne.nonlinearities.identity)

  
    return net

# ####################### Fonction pour iterer en minibatch ##################


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]


####################### Fonction pour sauvegarder le reseau #####################
def save_network(filename,param_values):
    f = file(filename, 'wb')
    cPickle.dump(param_values,f,protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()

# ############################## Fonction Main ################################


def main(tt_input_shape, tt_output_shape, tt_ranks, num_epochs=500):

    print("Chargement des donnees...")
    N_train = 1080
    N_test = 117
    data = md.load_foursquare()
    (X_train,Y_train,X_test,Y_test) = md.forecasting_data(data,N_train,N_test,3,1)
    Y_train = Y_train.reshape(N_train,Y_train.shape[1]*Y_train.shape[2])
    Y_test = Y_test.reshape(N_test,Y_test.shape[1]*Y_test.shape[2])
   
    np.save('X_test_foursquare.npy', X_test)
    np.save('Y_test_foursquare.npy', Y_test)

    input_var = T.dtensor4('inputs')
    target_var = T.dmatrix('targets')

    #Mise en forme des arguments
    tt_input_shape=tt_input_shape.split(',')
    tt_output_shape=tt_output_shape.split(',')
    tt_ranks=tt_ranks.split(',')
    tt_input_shape=np.asarray(map(int,tt_input_shape))
    tt_output_shape=np.asarray(map(int,tt_output_shape))
    tt_ranks=np.asarray(map(int,tt_ranks))
    
    print("Parametre de la TTLayer :\t{!s}\t{!s}\t{!s}".format(tt_input_shape,tt_output_shape,tt_ranks)) 
    print("Construction du modele...")
    network = build_mlp(tt_input_shape, tt_output_shape, tt_ranks, input_var)

    #Loss function
    prediction = lasagne.layers.get_output(network['l_out'])
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()

    #Mise a jour des parametres
    params = lasagne.layers.get_all_params(network['l_out'], trainable=True)
    updates = lasagne.updates.nesterov_momentum(
            loss, params, learning_rate=0.01, momentum=0.9)   

    train_fn = theano.function([input_var, target_var], loss, updates=updates)


    print("Phase d'apprentissage...")
    start = time.time()

    for epoch in range(num_epochs):

        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, Y_train, 2, shuffle=True):
            inputs, targets = batch
	    train_err += train_fn(inputs, targets)
            train_batches += 1

	#if(epoch == num_epochs - 1): 
        print("Epoch {} of {} took {:.3f}s".format(
        epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))

    #Sauvegarde du reseau entraine
    save_network("foursquarewTTLayer_net",lasagne.layers.get_all_param_values(network['l_out']))
    
    
    
    print("Total time {:.3f}s".format(time.time() - start))
   


if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network using Lasagne.")
        print("Usage: %s [EPOCHS]" % sys.argv[0])
        print()
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['num_epochs'] = int(sys.argv[1])
	    kwargs['tt_input_shape'] = sys.argv[2]
	    kwargs['tt_output_shape'] = sys.argv[3]
	    kwargs['tt_ranks'] = sys.argv[4]
        main(**kwargs)
