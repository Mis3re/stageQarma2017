CONNEXION EN VPN AU CLUSTER FRONTEND DU LIF - MAC

- Soit connecté en cable ethernet, soit sur eduroam avec login=adresse mail etu.univ-msr.fr et password classique.

- Installer Tunnelblick (OpenVPN client) from there: https://tunnelblick.net/index.html

- Glissez-déposez dans la liste des configurations le fichier de configuration "VPN-ClusterLIF.tblk" fournis par les ingénieurs réseaux du LIF (dans le même dossier que ce fichier)

- Puis cliqué sur connecter et rentrez vos identifiants LIF.

- Si vous vous connectez en wifi sur eduroam, il faut vérifier que le port utilisé ne soit pas bloqué. Vous pouvez le trouver dans le fichier de configuration d’OpenVPN. Par exemple le port utilisé (et bloqué) était le n° 11194. Après une demande de redirection vers un port libre, je me suis connecté à partir du n° 1194.

- Puis pour se connecter au fronted, il faut se connecter en ssh comme suit : ssh firstname.name@frontend.lidil.univ-mrs.fr. Perso, j’ai créé un simple fichier de script que je lance à partir d’un alias qui me connecte en ssh et entre mes id et mdp pour me connecter automatiquement. Attention, c’est pas génial niveau sécurité mais c’est transparent niveau utilisation.

- A oui, et perso j’utilise FileZilla pour faire transiter mes fichier entre le local et le cluster. (Dans la barre de connexion rapide : Hôte="sftp://frontend.lidil.univ-mrs.fr », ID=prenom.nom, mdp=xxxxx, port=empty).