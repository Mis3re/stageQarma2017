Hello,

You will find, attached to this email, the files and docs (in french,
but just follow the pictures) for configuring the VPN on ubuntu.

The username/password are the ones from the lab.

Then, when connected, you can access the frontend like this:

ssh firstname.name@frontend.lidil.univ-mrs.fr

Also check this page for informations on the cluster status:
http://frontend.lidil.univ-mrs.fr/

And read the doc about our batch scheduler here:
http://oar.imag.fr/sources/2.5/docs/documentation/OAR-DOCUMENTATION-USER/

PS: if you can't resolve frontend.lidil.univ-mrs.fr , please use the IP
address: 192.168.4.115

Regards,
Manuel