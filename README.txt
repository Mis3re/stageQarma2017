Auteur : Pierre-Antoine MARTRICE, Aix-Marseille Université
Contacts : panim.martrice@gmail.com

Stage de recherche réalisé au sein du Laboratoire d’Informatique Fondamentale
de Marseille (LIF) avec l'acceuil et le soutient de Thierry Artieres,
Stéphane Ayache et Hachem Kadri.
https://qarma.lis-lab.fr/

Contient des fichiers de : Yasmina BOUZBIBA, Polytech Nice-Sophia
(Dans le dossier « other/stageYasmina »)



Résumé

Ce travail porte sur la mise au point de réseaux de neurones économes en calculs et en ressources
mémoire, portable sur des périphériques de faibles puissances. Il s’agit  d’une problématique
particulièrement importante pour ces types de modèles généralement de très grande taille.

Des méthodes de factorisation de tenseurs ont été proposées récemment pour compresser les poids
des couches cachées des réseaux de neurones, totalement connectées ou convolutionnelles. Il s’agit
du point de départ de mon stage. J’ai eu comme objectif premier d’étudier l’impact de certains
paramètres de ces méthodes de décompositions tensorielles quant à l’efficacité des réseaux de
neurones dans lesquels ces décompositions sont utilisées.

Je me suis penché sur trois d’entre elles : la décomposition CP, considérée comme référence,
la décomposition TT ainsi que la décomposition de Kronecker. Dans un second temps, l’étude
de la décomposition de Kronecker nous a amené à faire un parallèle entre son fonctionnement
et l’apprentissage de filtres par un CNN. Nous avons donc développé une nouvelle couche qui
apprend des filtres représentant des caractéristiques d’un ensemble d’images. 



Mots clés

Apprentissage Automatique
Réseaux de neurones profond
Tenseur,
Décomposition CP
Décomposition TT
Produit de Kronecker
CNN
