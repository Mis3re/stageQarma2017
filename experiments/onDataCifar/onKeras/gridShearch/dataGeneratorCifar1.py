#!/usr/bin/env python
# -*- coding: utf-8 -*-

# EXEMPLE : python mnist_2.py "cnn" "cnn" True True 128 0.25 0.25 3 3 3

# typeLayer1(Str): "cnn" ou "kro" pour la 1ere couche
# typeLayer2(Str): "cnn" ou "kro" pour la 2eme couche
# batchNormaisation1(Bool): True ou False, après la 1ere couche
# batchNormaisation2(Bool): True ou False, après la 2eme couche
# nbNeuronDense(int): nombre de neurones de la 1ere denseLayer (la 2eme est la couche de sortie, elle a 10 neurones)
# dropOut1_value(float): 0 / 0,25 / 0,5
# dropOut2_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [1ere couche]
# K2(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [2eme couche]
# K3(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [3eme couche]

fileCnnCnn = open('/Users/pa/Documents/stageQarma2017/expe/onDataCifar/tensorflow/gridShearch/gridOfParametersCnnCnnCifar1.txt', 'w')
fileCnnKro = open('/Users/pa/Documents/stageQarma2017/expe/onDataCifar/tensorflow/gridShearch/gridOfParametersCnnKroCifar1.txt', 'w')
fileKroCnn = open('/Users/pa/Documents/stageQarma2017/expe/onDataCifar/tensorflow/gridShearch/gridOfParametersKroCnnCifar1.txt', 'w')
fileKroKro = open('/Users/pa/Documents/stageQarma2017/expe/onDataCifar/tensorflow/gridShearch/gridOfParametersKroKroCifar1.txt', 'w')

batchNormaisation1 = [False, True]
batchNormaisation2 = [False, True]
relu = [0, 1, 2]
nbNeuronDense = [128, 256]
dropOut1_value = [0, 0.25, 0.5]
dropOut2_value = [0, 0.25, 0.5]
K1 = [16, 32, 64, 128, 256]   # K1 = K2 = K3

# type1 = "cnn" AND type2 = "cnn"
for bool1 in batchNormaisation1:
    for bool2 in batchNormaisation2:
        for activ in relu:
            for nbNeuron in nbNeuronDense:
                for val1 in dropOut1_value:
                    for val2 in dropOut2_value:
                        for K in K1:
                            string = str("cnn") + " " + str("cnn") + " " + str(bool1) + " " + str(bool2) + " " + str(activ) + " " + str(nbNeuron) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K) + " " + str(K) + "\n"
                            fileCnnCnn.write(string)

# type1 = "cnn" AND type2 = "kro"
for bool1 in batchNormaisation1:
    for bool2 in batchNormaisation2:
        for activ in relu:
            for nbNeuron in nbNeuronDense:
                for val1 in dropOut1_value:
                    for val2 in dropOut2_value:
                        for K in K1:
                            string = str("cnn") + " " + str("kro") + " " + str(bool1) + " " + str(bool2) + " " + str(activ) + " " + str(nbNeuron) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K) + " " + str(K) + "\n"
                            fileCnnKro.write(string)

# type1 = "kro" AND type2 = "cnn"
for bool1 in batchNormaisation1:
    for bool2 in batchNormaisation2:
        for activ in relu:
            for nbNeuron in nbNeuronDense:
                for val1 in dropOut1_value:
                    for val2 in dropOut2_value:
                        for K in K1:
                            string = str("kro") + " " + str("cnn") + " " + str(bool1) + " " + str(bool2) + " " + str(activ) + " " + str(nbNeuron) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K) + " " + str(K) + "\n"
                            fileKroCnn.write(string)

# type1 = "kro" AND type2 = "kro"
for bool1 in batchNormaisation1:
    for bool2 in batchNormaisation2:
        for activ in relu:
            for nbNeuron in nbNeuronDense:
                for val1 in dropOut1_value:
                    for val2 in dropOut2_value:
                        for K in K1:
                            string = str("kro") + " " + str("kro") + " " + str(bool1) + " " + str(bool2) + " " + str(activ) + " " + str(nbNeuron) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K) + " " + str(K) + "\n"
                            fileKroKro.write(string)
