#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import sys
import numpy as np
from os import listdir
import operator
from scipy import ndimage
import matplotlib.pyplot as plt

from PIL import Image


print("Start...")
path = '/Users/pa/Documents/stageQarma2017/expe/onDataCifar/tensorflow/gridShearch/img/KroKro/normalGlobal/'
listOf_strFile = listdir(path)

print("Read each files...")
results = []
for strFile in listOf_strFile:
    if "DS_Store" in strFile:
        continue
    img = [[[], []], [[], []]]
    result_file = []
    im = ndimage.imread(path + strFile)


    img[0][0].append(im[0][0])
    img[0][1].append(im[0][1])
    img[1][0].append(im[1][0])
    img[1][1].append(im[0][1])

    img[0][0].append(im[0][2])
    img[0][1].append(im[0][3])
    img[1][0].append(im[1][2])
    img[1][1].append(im[0][3])

    img[0][0].append(im[0][4])
    img[0][1].append(im[0][5])
    img[1][0].append(im[1][4])
    img[1][1].append(im[0][5])

    img = np.array(img)

    #result = Image.fromarray(img.astype(numpy.uint8))
    result = Image.fromarray(img)
    result.save(path + 'c_' + strFile)

    #plt.imshow(im)
    #plt.show()
    #print(im)

# [[[252 162 255]
#   [111  30 150]
#   [119  46 115]
#   [156  91 121]]
#
#  [[186 130 205]
#   [172 122 160]
#   [210 171 156]
#   [206 175 121]]
#
#  [[ 78  64  61]
#   [143 135  96]
#   [167 171  77]
#   [108 120   0]]
#
#  [[ 34  50  37]
#   [121 145  93]
#   [178 211 106]
#   [179 221  75]]]