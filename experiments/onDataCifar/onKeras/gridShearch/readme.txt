[Dossier jumeaux dans 'onDataMnist']

On va effectuer un greadSearch sur un certain nombre de paramêtres et sur plusieurs architectures différentes possédant des kroLayer.
En symétrie on effectue les même tests sur des réseux convolutionnel (ie on remplace les kroLayer par des convLayer).
Pour les détails voir cahier ou photos.
Ici seront stocké les différents version du réseau et dans le dossier 'results' les résultats correspondants.

————————————————————————————————————————————————————————————————————
cifar_1.py

# EXEMPLE : python mnist_2.py "cnn" "cnn" True True 128 0.25 0.25 3 3 3

# typeLayer1(Str): "cnn" ou "kro" pour la 1ere couche
# typeLayer2(Str): "cnn" ou "kro" pour la 2eme couche
# batchNormaisation1(Bool): True ou False, après la 1ere couche
# batchNormaisation2(Bool): True ou False, après la 2eme couche
# nbNeuronDense(int): nombre de neurones de la 1ere denseLayer (la 2eme est la couche de sortie, elle a 10 neurones)
# dropOut1_value(float): 0 / 0,25 / 0,5
# dropOut2_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [1ere couche]
# K2(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [2eme couche]
# K3(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [3eme couche]