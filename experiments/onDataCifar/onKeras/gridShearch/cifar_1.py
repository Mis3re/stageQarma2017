#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import sys
import keras
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.layers import Dense, Dropout, Activation, Flatten
from KroneckerLayer_whileLoop import KroneckerLayer
from keras.layers import Conv2D
from keras.layers.normalization import BatchNormalization
from keras.initializers import RandomUniform

# EXEMPLE : python mnist_2.py "cnn" "cnn" True True 128 0.25 0.25 3 3 3

# typeLayer1(Str): "cnn" ou "kro" pour la 1ere couche
# typeLayer2(Str): "cnn" ou "kro" pour la 2eme couche
# batchNormaisation1(Bool): True ou False, après la 1ere couche
# batchNormaisation2(Bool): True ou False, après la 2eme couche
# relu(int): 0=Non; 1=Oui, juste après les couches; 2=Oui, après la couche de batchNorm
# nbNeuronDense(int): nombre de neurones de la 1ere denseLayer (la 2eme est la couche de sortie, elle a 10 neurones)
# dropOut1_value(float): 0 / 0,25 / 0,5
# dropOut2_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [1ere couche]
# K2(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [2eme couche]
# K3(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [3eme couche]

def main(typeLayer1,
         typeLayer2,
         batchNormaisation1,
         batchNormaisation2,
         relu,
         nbNeuronDense,
         dropOut1_value,
         dropOut2_value,
         K1,
         K2,
         K3,):

    print("\nPARAMETRES:")
    print("typeLayer1 : " + str(typeLayer1))
    print("typeLayer2 : " + str(typeLayer2))
    print("batchNormaisation1 : " + str(batchNormaisation1))
    print("batchNormaisation2 : " + str(batchNormaisation2))
    print("relu : " + str(relu))
    print("nbNeuronDense : " + str(nbNeuronDense))
    print("dropOut1_value : " + str(dropOut1_value))
    print("dropOut2_value : " + str(dropOut2_value))
    print("K1 : " + str(K1))
    print("K2 : " + str(K2))
    print("K3 : " + str(K3))
    print

    optimizer = keras.optimizers.RMSprop()
    batch_size = 50
    epochs = 10
    num_classes = 10
    # input image dimensions
    img_rows, img_cols = 32, 32

    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = cifar10  .load_data()

    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 3, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 3, img_rows, img_cols)
        input_shape = (3, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 3)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 3)
        input_shape = (img_rows, img_cols, 3)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255

    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    x_validation = x_train[:10000]
    y_validation = y_train[:10000]
    x_train = x_train[10000:]
    y_train = y_train[10000:]

    # CREATION DE L'ARCHITECTURE
    model = Sequential()

    if typeLayer1 == "kro":
        model.add(KroneckerLayer((2, 2), K1, input_shape=input_shape, batch_size=batch_size))
        if relu == 1:
            model.add(Activation('relu'))

    else:   # "cnn"
        model.add(Conv2D(K1, (2, 2), strides=(2, 2), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None), input_shape=input_shape))
        if relu == 1:
            model.add(Activation('relu'))

    if batchNormaisation1:
        model.add(BatchNormalization(axis=-1))

    if relu == 2:
        model.add(Activation('relu'))

    if typeLayer2 == "kro":
        model.add(KroneckerLayer((2, 2), K2, batch_size=batch_size))
        if relu == 1:
            model.add(Activation('relu'))

    else:   # "cnn"
        model.add(Conv2D(K2, (2, 2), strides=(2, 2), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None)))
        if relu == 1:
            model.add(Activation('relu'))

    if batchNormaisation2:
        model.add(BatchNormalization(axis=-1))

    if relu == 2:
        model.add(Activation('relu'))

    model.add(Dropout(dropOut1_value))

    model.add(Conv2D(K3, (2, 2), strides=(2, 2), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None)))
    if not relu == 0:
        model.add(Activation('relu'))

    model.add(Dropout(dropOut2_value))

    model.add(Flatten())
    model.add(Dense(nbNeuronDense))
    model.add(Activation('relu'))

    model.add(Dense(num_classes, activation='softmax'))
    model.summary()

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=optimizer,
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=2,
              validation_data=(x_validation, y_validation))

    score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
    print('\nTest loss:', score[0])
    print('Test accuracy:', score[1])


if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['typeLayer1'] = str(sys.argv[1])
        kwargs['typeLayer2'] = str(sys.argv[2])
        kwargs['batchNormaisation1'] = bool(sys.argv[3])
        kwargs['batchNormaisation2'] = bool(sys.argv[4])
        kwargs['relu'] = int(sys.argv[5])
        kwargs['nbNeuronDense'] = int(sys.argv[6])
        kwargs['dropOut1_value'] = float(sys.argv[7])
        kwargs['dropOut2_value'] = float(sys.argv[8])
        kwargs['K1'] = int(sys.argv[9])
        kwargs['K2'] = int(sys.argv[10])
        kwargs['K3'] = int(sys.argv[11])
    main(**kwargs)