#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import keras
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.initializers import RandomUniform
#from KroneckerLayer_wConcatMB_wConcatChan import KroneckerLayer
#from keras.layers.normalization import BatchNormalization

batch_size = 10
num_classes = 10
epochs = 10


# input image dimensions
img_rows, img_cols = 32, 32

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 3, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 3, img_rows, img_cols)
    input_shape = (3, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 3)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 3)
    input_shape = (img_rows, img_cols, 3)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()

# TODO: Pour que la couche de BatchNormalization fonctionne il faut que la batchSize doit être assez grande.

model.add(Conv2D(64, (4, 4), strides=(4, 4), kernel_initializer=RandomUniform(minval=-5, maxval=5, seed=None), input_shape=input_shape))
model.add(Conv2D(64, (2, 2), strides=(2, 2), kernel_initializer=RandomUniform(minval=-5, maxval=5, seed=None)))

#model.add(KroneckerLayer((2,2), 8, input_shape=input_shape, batch_size=batch_size))
#model.add(BatchNormalization(axis=1))
#model.add(KroneckerLayer((8,8), 4, batch_size=batch_size))
#model.add(BatchNormalization(axis=1))

model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))
model.summary()

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
print('\nTest loss:', score[0])
print('Test accuracy:', score[1])

