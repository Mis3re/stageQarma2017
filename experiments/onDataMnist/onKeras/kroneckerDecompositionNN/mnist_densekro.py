#!/usr/bin/env python
#-*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''



from __future__ import print_function
import os
import time
import sys
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras import backend as K
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
from KroLayer import KroLayer

###
# Ce qui ce passe niveau calcul : W étant de tenseur de paramètre, K étant le rang de kronecker
#
#   W = Somme((Ain prod.kro Bin) prod.kro (Aout prod.kro Bout))
#         K
#
#  avec shapeAin et shapeBin qui décompose la dimension d'entrée (pour mnist c'est 784)
#  et   shapeAout et shapeBout qui décompose la dimension de sortie (ici 800 car la denseLayer qui suit à 800 neurones)
###

def main(shapeAin, shapeBin, shapeAout, shapeBout, rank, epochs=20):
    shapeAin = shapeAin.split("-")
    shapeAin = (int(shapeAin[0]), int(shapeAin[1]))
    shapeBin = shapeBin.split("-")
    shapeBin = (int(shapeBin[0]), int(shapeBin[1]))
    shapeAout = shapeAout.split("-")
    shapeAout = (int(shapeAout[0]), int(shapeAout[1]))
    shapeBout = shapeBout.split("-")
    shapeBout = (int(shapeBout[0]), int(shapeBout[1]))
    num_classes = 10
    batch_size = 100

    print("shapeAin = ", shapeAin)
    print("shapeBin = ", shapeBin)
    print("shapeAout = ", shapeAout)
    print("shapeBout = ", shapeBout)
    print("rank = ", rank)
    print("epochs = ", epochs)
    print("batch_size = ", batch_size)
    print("num_classes = ", num_classes)

    # input image dimensions
    img_rows, img_cols = 28, 28

    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    print('order = ', K.image_data_format())
    if K.image_data_format() == 'channels_first':
        print('channels_first')
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    output_dim = 800

    model = Sequential()

    model.add(Flatten(input_shape=input_shape)) # 1 x 784
    model.add(KroLayer(input_shape, output_dim, shapeAin, shapeBin, shapeAout, shapeBout, rank, batch_size))
    model.add(Activation('relu'))

    model.add(BatchNormalization())
    model.add(Dropout(0.25))

    model.add(Dense(800))
    model.add(Activation('relu'))
    # model.add(Dropout(0.25))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    print(model.summary())
    #checkpointer = ModelCheckpoint(filepath="models/weights_dense.h5", verbose=1, save_best_only=True)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=10, min_lr=0.001)


    start_time = time.time()
    model.fit(x_train, y_train,
            batch_size=batch_size,
            epochs=epochs,
            verbose=1,
            validation_data=(x_test, y_test),
            callbacks = [reduce_lr])
    end_time = time.time() - start_time

    #model.load_weights('models/weights_dense.h5')

    score = model.evaluate(x_test, y_test, verbose=0)
    print("AVEC batcnNorm(axis=0) et dropOut(0,25)")
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    print('Time:', end_time)

    return score[0], score[1], end_time

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['shapeAin'] = sys.argv[1]
        kwargs['shapeBin'] = sys.argv[2]
        kwargs['shapeAout'] = sys.argv[3]
        kwargs['shapeBout'] = sys.argv[4]
        kwargs['rank'] = int(sys.argv[5])
        kwargs['epochs'] = int(sys.argv[6])
    main(**kwargs)
