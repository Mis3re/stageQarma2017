#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from mnist_densekro import main as kroMain
import numpy as np

def main(numFold):
    folder_path = os.getcwd() + '/'
    grid = open(folder_path + "gridOfParameters_MNIST_KroneckerLayer_part" + str(numFold) + ".txt", 'r')
    results = open(folder_path + "results_part" + numFold + ".txt", 'w')
    param = grid.readline().split(" ")

    while not param == [""]:
        try:
            shapeAin = param[0]
            shapeBin = param[1]
            shapeAout = param[2]
            shapeBout = param[3]
            rank = int(param[4])
            epochs = int(param[5][:-1])

            print("Param : shapeAin = ", shapeAin)
            print("Param : shapeBin = ", shapeBin)
            print("Param : shapeAout = ", shapeAout)
            print("Param : shapeBout = ", shapeBout)
            print("Param : rank = ", rank)
            print("Param : epochs = ", epochs)

            shapeAinArray = shapeAin.split("-")
            shapeBinArray = shapeBin.split("-")
            shapeAoutArray = shapeAout.split("-")
            shapeBoutArray = shapeBout.split("-")
            nb_param = ((int(shapeAinArray[0]) * int(shapeAinArray[1])) + (int(shapeBinArray[0]) * int(shapeBinArray[1])) +\
                       (int(shapeAoutArray[0]) * int(shapeAoutArray[1])) + (int(shapeBoutArray[0]) * int(shapeBoutArray[1]))) * rank

            res = kroMain(shapeAin, shapeBin, shapeAout, shapeBout, rank, 20)
            print(res)
            loss = res[0]
            acc = res[1]
            time = res[2]
            res = "| " + str(shapeAin) + " | " + str(shapeBin) + " | " + str(shapeAout) + " | " + str(shapeBout) +\
                  " | " + str(rank) + " | " + str(loss) + " | " + str(acc) + " | " + str(time) + " | " + str(nb_param) + " |\n"
            results.write(res)

        except Exception,e:
            print("\n")
            print(str(e))
            print("\n")
            print("\nATTENTION:\n")
            print("\nL'experience suivant a plante:")
            print(param)
            print("\n")
        param = grid.readline().split(" ")

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['numFold'] = str(sys.argv[1])
    main(**kwargs)