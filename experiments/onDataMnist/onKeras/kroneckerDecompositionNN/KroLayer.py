#!/usr/bin/env python
#-*- coding: utf-8 -*-

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import theano as th
from theano import tensor
from theano.tensor.slinalg import kron

class KroLayer(Layer):

    def __init__(self, input_dim, output_dim, shapeAin, shapeBin, shapeAout, shapeBout, rank, batch_size, **kwargs):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.shapeAin = shapeAin
        self.shapeBin = shapeBin
        self.shapeAout = shapeAout
        self.shapeBout = shapeBout
        self.rank = rank
        self.batch_size = batch_size
        super(KroLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.Ain = self.add_weight(name="Ain", shape=(self.rank, self.shapeAin[0], self.shapeAin[1]), initializer='normal', trainable=True)
        self.Bin = self.add_weight(name="Bin", shape=(self.rank, self.shapeBin[0], self.shapeBin[1]), initializer='normal', trainable=True)
        self.Aout = self.add_weight(name="Aout", shape=(self.rank, self.shapeAout[0], self.shapeAout[1]), initializer='normal', trainable=True)
        self.Bout = self.add_weight(name="Bout", shape=(self.rank, self.shapeBout[0], self.shapeBout[1]), initializer='normal', trainable=True)
        super(KroLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        A = K.zeros((self.shapeAin[0]*self.shapeBin[0], self.shapeAin[1]*self.shapeBin[1]))
        B = K.zeros((self.shapeAout[0]*self.shapeBout[0],self.shapeAout[1]*self.shapeBout[1]))
        kernel = K.zeros((self.shapeAin[0]*self.shapeBin[0]*self.shapeAout[0]*self.shapeBout[0], self.shapeAin[1]*self.shapeBin[1]*self.shapeAout[1]*self.shapeBout[1]))
        for k in range(self.rank):
            A = A + th.tensor.slinalg.kron(self.Ain[k], self.Bin[k])
            B = B + th.tensor.slinalg.kron(self.Aout[k], self.Bout[k])
            kernel = kernel + th.tensor.slinalg.kron(A, B)
        kernel = K.reshape(kernel, (-1, self.output_dim))
        output = K.dot(x, kernel)
        
        #output = th.convert_to_tensor(output)
        return output #K.dot(x, self.kernel)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)

