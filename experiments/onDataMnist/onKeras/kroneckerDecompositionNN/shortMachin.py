#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

folder_path = os.getcwd() + '/'
for i in range(34,44):
    print("I read a new file : " + str(i) + "/43")
    file_r = open(folder_path + "stdout/OAR.16196" + str(i) + ".stdout", "r")
    file_w = open(folder_path + "stdout/OAR.16196" + str(i) + ".stdout.short", "w")
    line = file_r.readline()[:-1]
    while not line == "":
        if "('Param : shapeAin = " in line:
            for j in range(0,5):
                file_w.write(line + "\n")
                line = file_r.readline()[:-1]
        if "Test loss:" in line:
            for j in range(0,3):
                file_w.write(line + "\n")
                line = file_r.readline()[:-1]
        line = file_r.readline()[:-1]
