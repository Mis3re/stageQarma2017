#!/usr/bin/env python
# -*- coding: utf-8 -*-

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import tensorflow as tf
from keras.initializers import RandomUniform

# TODO: Chercher à optimiser ce bordel pour égaler un CNN (time, loss et acc)

# TODO: Générer des Bk déjà vectorisé
# TODO: Tester avec l'utilisation des multi-canaux mais sans concaténation des minibatchs (ce qui est le cas pour cette version)
# TODO: Changer l'utilisation des liste suivient d'un convert_to_tensor par un placeholder du bon shape directement.
# TODO: Rename variable + comment the code.
# TODO: Remplacer les appel tf... par K... quand possible pour rendre compatible avec theano.
# TODO: |--> Quand pas possible réfléchir pour rendre cela compatible quand même.

## NOTATIONS ##
# batch_size :                  Nombre d'images dans chaque batch envoyé au réseau
# batch :                       Ensemble de 'batchSize' images
# M :                           Image en entrée
# nb_channels :                 Nombre de canaux de l'image M
# M_channel                     Contenu d'un canal d'une image
# M_c_size :                    Taille du contenu d'un canal d'une image en entrée -> (n*p, m*q)
# reshape_M_c :                 Reshape du j-ieme canal de l'image M -> (n*m, p*q)
# reshape_M :                   Ensemble des canaux reshapés d'une image = image reshapée
# reshape_b :                   Ensemble des images reshapées = batch reshapé
# kronecker_rank :              Rang de kronecker
# B :                           Enesemble des K filtres
# bk_size :                     Taille des filtres bk -> (p, q)
# bk :                          k-ieme filtre
# A :                           Ensemble de K matrices de pondérations
# ak_size :                     Taille des matrices de pondérations -> (n, m)
# Ak :                          k-ieme matrice de ponération


class KroneckerLayer(Layer):
    def reshape(self, M_, m_, n_, p_, q_):
        MM_ = []
        for i in range(m_):
            for j in range(n_):
                MM_.append(tf.reshape(M_[i*p_:(i+1)*p_, j*q_:(j+1)*q_], (p_*q_,1))) # que pour minibactch=1 et graylevel
        tfMM = tf.convert_to_tensor(MM_)
        return tf.reshape(tfMM, (m_*n_, p_*q_))

    def __init__(self, bk_size, kronecker_rank, batch_size, regularizer=None, **kwargs):
        self.bk_size = bk_size
        self.kronecker_rank = kronecker_rank
        self.batch_size = int(batch_size)
        self.regularizer = regularizer
        super(KroneckerLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Nombre de canaux de l'image
        nb_channels = input_shape[-1]
        # Taille de l'image
        M_c_size = (int(input_shape[1]), int(input_shape[2]))

        # Calcul de ak_size = (kronecker_rank, n, m)
        self.ak_size = (self.kronecker_rank, int(M_c_size[0] / self.bk_size[0]), int(M_c_size[1] / self.bk_size[1]))

        # Déclaration du tenseurs contenant les filtres bk = (kronecker_rank, p, q * nb_channels) --> [q * nb_channels] car concaténation des canaux
        #self.B = self.add_weight(shape=(self.kronecker_rank, self.bk_size[0], self.bk_size[1]), initializer='uniform', trainable=True)
        self.B = self.add_weight(shape=(self.kronecker_rank, self.bk_size[0], self.bk_size[1]),
                                 initializer=RandomUniform(minval=-5, maxval=5, seed=None),
                                 trainable=True,
                                 regularizer=self.regularizer)
        super(KroneckerLayer, self).build(input_shape)

    def call(self, batch):
        # Liste de tout les Ak qui ont été calculés
        Aks = []
        #block_R_x = K.placeholder(shape=(self.batch_size, self.ak_size[1] * self.ak_size[2], self.bk_size[0] * self.bk_size[1]))

        # Pour chaque canal
        for j in range(int(batch[0].shape[2])):
            reshape_M = []

            #Pour chaque image
            for i in range(self.batch_size):
                #On recupere l'image du j-ieme canal de l'image i
                M_channel = batch[i][:, :, j]

                # Reshappe de l'image' du j-eme canal de l'image i (nouvelle taille : mn * pq)
                reshape_M_c = self.reshape(M_channel, self.ak_size[1], self.ak_size[2], self.bk_size[0], self.bk_size[1])
                reshape_M.append(reshape_M_c)

            reshape_M = tf.convert_to_tensor(reshape_M)
            # Doit valoir [1960, 4]
            reshape_M = K.reshape(reshape_M, (int(reshape_M.shape[0]) * int(reshape_M.shape[1]), int(reshape_M.shape[2])))

            # Pour toutes les valeurs du rank de kronecker
            Aks_c = []
            for k in range(self.kronecker_rank):
                Bk = self.B[k]
                Bk = tf.reshape(Bk, (-1, 1)) #self.bk_size[0] * self.bk_size[1]))
                BkT = tf.transpose(Bk)
                b_inverse = 1. / K.dot(BkT, Bk)
                dot_product = K.dot(reshape_M, Bk)
                Ak_c = dot_product * b_inverse
                Ak_c = tf.reshape(Ak_c, (self.batch_size, self.ak_size[1], self.ak_size[2]))
                Aks_c.append(Ak_c)
            Aks_c = tf.convert_to_tensor(Aks_c)
            Aks.append(Aks_c)

        Aks = tf.convert_to_tensor(Aks)
        Aks = K.sum(Aks, axis=0)
        Aks = tf.transpose(Aks, perm=(1, 2, 3, 0))
        return Aks

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.ak_size[1], self.ak_size[2], self.ak_size[0])

