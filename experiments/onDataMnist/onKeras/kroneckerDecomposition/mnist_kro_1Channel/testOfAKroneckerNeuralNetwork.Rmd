---
title: "Test of a kronecker neural network"
author: "On MNIST data"
output: pdf_document
---
**Description :** Premiers résultats de classification avec un réseau de neurones contenant une couche de kronecker.

* Apprentissage et test sur les données MNIST.
* Coder avec Keras et TensorFlow.
* Architecture : *Kronecker_layer --> Dropout --> Flatten --> Dense_layer --> Dropout --> Dense_layer*
* Mini-batch d'une taille de 1.
* Les valeurs sont des moyennes sur 3 répétitions d'éxécution.
* Temps d'exécution pour chaque époch : environ 300s (~ 5min)

## Résultats en valeurs

| EPOCHS | LOSS | ACCURACIES | // | EPOCHS | LOSS | ACCURACIES |
|:------:|:-----:|:---------:|:------:|:------:|:-----:|:---------:|
| 0  | 0.2796 | 0.93   |    | 25 | 0.2725 | 0.9418 |
| 1  | 0.2478 | 0.9384 |    | 26 | 0.2721 | 0.9418 |
| 2  | 0.2478 | 0.9395 |    | 27 | 0.2725 | 0.9416 |
| 3  | 0.2527 | 0.9394 |    | 28 | 0.273  | 0.9425 |
| 4  | 0.2533 | 0.9399 |    | 29 | 0.2723 | 0.9421 |
| 5  | 0.2566 | 0.9396 |    | 30 | 0.2689 | 0.9425 |
| 6  | 0.2563 | 0.9407 |    | 31 | 0.2714 | 0.9421 |
| 7  | 0.2591 | 0.9409 |    | 32 | 0.2683 | 0.9434 |
| 8  | 0.2571 | 0.9419 |    | 33 | 0.2729 | 0.9419 |
| 9  | 0.2603 | 0.9408 |    | 34 | 0.2759 | 0.9418 |
| 10 | 0.2635 | 0.9413 |    | 35 | 0.2717 | 0.9424 |
| 11 | 0.264  | 0.9411 |    | 36 | 0.2725 | 0.9418 |
| 12 | 0.2641 | 0.9419 |    | 37 | 0.2719 | 0.9426 |
| 13 | 0.2707 | 0.9402 |    | 38 | 0.2743 | 0.9424 |
| 14 | 0.2668 | 0.9416 |    | 39 | 0.2723 | 0.9417 |
| 15 | 0.2696 | 0.941  |    | 40 | 0.2716 | 0.9423 |
| 16 | 0.2712 | 0.9403 |    | 41 | 0.2713 | 0.943  |
| 17 | 0.2679 | 0.9418 |    | 42 | 0.2724 | 0.9421 |
| 18 | 0.2698 | 0.9409 |    | 43 | 0.2733 | 0.9417 |
| 19 | 0.2715 | 0.9412 |    | 44 | 0.2711 | 0.9424 |
| 20 | 0.2689 | 0.9421 |    | 45 | 0.2704 | 0.9431 |
| 21 | 0.2697 | 0.9419 |    | 46 | 0.2698 | 0.9431 |
| 22 | 0.2681 | 0.9427 |    | 47 | 0.2722 | 0.9427 |
| 23 | 0.2691 | 0.9429 |    | 48 | 0.2693 | 0.9423 |
| 24 | 0.2737 | 0.9415 |    | 49 | 0.27   | 0.9431 |

| TEST LOSS | TEST ACCURACIES |
|:---------:|:---------------:|
| 0.270035217375 | 0.943140625|
\newpage
## Résultats en graphes

![accuracyToEpochs](/Users/pa/Documents/stageQarma2017/expe/onDataMnist/kroneckerDecomposition/accuracyToEpochs.png)

![errorToEpochs ](/Users/pa/Documents/stageQarma2017/expe/onDataMnist/kroneckerDecomposition/errorToEpochs.png)