#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from KroneckerLayer import KroneckerLayer
#from KroneckerLayer_concat import KroneckerLayer
from keras.layers.normalization import BatchNormalization
from PIL import Image
from keras import regularizers
from keras.layers.core import ActivityRegularization
import numpy as np

batch_size = 10
print ("batch_size = " + str(batch_size))
num_classes = 10
epochs = 1


# input image dimensions
img_rows, img_cols = 28, 28

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

print('x_train shape:', x_train.shape)
# create 2nd channel
x_train2 = x_train[..., None] * np.ones(2, dtype=float)
x_train = x_train2.reshape((x_train2.shape[0], x_train2.shape[1], x_train2.shape[2], 2))
x_test2 = x_test[..., None] * np.ones(2, dtype=float)
x_test = x_test2.reshape((x_test2.shape[0], x_test2.shape[1], x_test2.shape[2], 2))
input_shape = (img_rows, img_cols, 2)

print('x_train shape:', x_train.shape)

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()

# TODO: Pour que la couche de BatchNormalization fonctionne il faut que la batchSize doit être assez grande.

model.add( KroneckerLayer((2,2), 8, input_shape=input_shape, batch_size=batch_size, regularizer=None) ) #regularizers.l2(0.01)) )
model.add( KroneckerLayer((2,2), 8, batch_size=batch_size, regularizer=None) ) #regularizers.l2(0.01)) )
#model.add( KroneckerLayer((4,4), 32, input_shape=input_shape, batch_size=batch_size, regularizer=regularizers.l1(0.01)) )

#model.add( ActivityRegularization(l1=0.01) ) # activities (maps) of Kronecker layer will be sparse

#model.add(BatchNormalization(axis=1))
#model.add(Conv2D(32, (3, 3), activation='relu'))
#model.add(BatchNormalization(axis=1))

#model.add(KroneckerLayer((2,2), 3, batch_size=batch_size))
#model.add(BatchNormalization(axis=1))

#model.add(Dropout(0.25))
model.add(Flatten())
#model.add(Dense(128, activation='linear'))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
print('\nTest loss:', score[0])
print('Test accuracy:', score[1])


Bks0 = model.layers[0].get_weights()
Bks0 = Bks0[0]

#Bks1 = model.layers[1].get_weights()
#Bks1 = Bks1[0]

Bks0 = Bks0 * 127. + 127.
#Bks1 = Bks1 * 127. + 127.

for i in range(len(Bks0)):
    im = Image.fromarray(Bks0[i])
    im2 = im.convert('L')
    im2.save('Bks0_'+str(i)+'.jpg')
          
#for i in range(len(Bks1)):
#    im = Image.fromarray(Bks1[i])
#    im2 = im.convert('L')
#    im2.save('Bks1_'+str(i)+'.jpg')

