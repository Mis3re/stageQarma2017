#!/usr/bin/env python
# -*- coding: utf-8 -*-

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import tensorflow as tf

# TODO: Chercher à optimiser ce bordel pour égaler un CNN (time, loss et acc)

# TODO: Générer des Bk déjà vectorisé
# TODO: Tester avec l'utilisation des multi-cannaux mais sans concaténation des minibatchs (ce qui est le cas pour cette version)
# TODO: Changer l'utilisation des liste suivient d'un convert_to_tensor par un placeholder du bon shape directement.
# TODO: Rename variable + comment the code.
# TODO: Remplacer les appel tf... par K... quand possible pour rendre compatible avec theano.
# TODO: |--> Quand pas possible réfléchir pour rendre cela compatible quand même.

## Notation :
# batch : Enesemble de batchSize images
# batchSize : Ensemble d'image que l'on donne d'un coup au réseau.
# M : Image d'entrée de taille (n*p, m*q)
# K : Rang de kronecker
# B: Enesemble des K filtres de taille (p, q)
# bk : k-ieme filtre
# A : Ensemble de K matrice de pondérations de taille (n, m)
# Ak : k-ieme matrice de ponération

class KroneckerLayer(Layer):

    def reshape(self, M_, m_, n_, p_, q_):
        # reshape M -> MM
        #MM_ = tf.convert_to_tensor(np.ndarray((m_*n_, p_*q_)))

        MM_ = []
        for i in range(m_):
            for j in range(n_):
                MM_.append(tf.reshape(M_[i*p_:(i+1)*p_, j*q_:(j+1)*q_], (p_*q_,1))) # que pour minibactch=1 et graylevel
        tfMM = tf.convert_to_tensor(MM_)
        return tf.reshape(tfMM, (m_*n_, p_*q_))

    def __init__(self, bkSize, kroneckerRank, batch_size, **kwargs):
        self.bkSize = bkSize
        self.kroneckerRank = kroneckerRank
        self.batch_size = int(batch_size)
        super(KroneckerLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Nombre de cannaux de l'image
        nbChannels = input_shape[-1]
        print('begin build, nbChannels=', nbChannels)
        # Calcul de akSize à partir de la taille de l'image d'entrée et des bkSize.
        self.akSize = (self.kroneckerRank, int(input_shape[1] / self.bkSize[0]), int(input_shape[2] / self.bkSize[1]))

        # Calcul de la taille des imagettes après concaténation (en ligne, donc il n'y a que les bkSize[1] qui est multiplié par le nombre de cannaux).
        # Remarque : je crois que bkSizeBig n'est utilisé que dans la ligne dessous... à voir.
        self.bkSizeBig = (self.bkSize[0], self.bkSize[1] * nbChannels)
        self.B = self.add_weight(shape=(self.kroneckerRank, self.bkSizeBig[0], self.bkSizeBig[1]), initializer='uniform', trainable=True)
        super(KroneckerLayer, self).build(input_shape)  # Be sure to call this somewhere!
        print('end build!')


    # Appel de cette fonction pour créer le moule (ie compilation de la fonction avec les paramètres custom) (=allocation mémoire?)
    def call(self, x):
        print('begin call...')
        Aks_batch = []
        #block_R_x = K.placeholder(shape=(self.batch_size, self.akSize[1] * self.akSize[2], self.bkSize[0] * self.bkSize[1]))

        # Liste qui contiendra tout les images du minibatch reshappées.
        # En vrai pas les images mais la liste des 'images' sur chaque canaux.
        concat_R_x_batch = []

        # Pour toute les images du mini-batche
        for i in range(self.batch_size):

            # Récupération de la i-eme image du mini-batche
            xx = x[i]

            # Liste qui contiendra les 'images' de chaque cannal
            concat_R_x_channel = []

            # Pour tout les cannaux de l'image i
            for j in range(int(xx.shape[2])):

                # Reshappe de l'image' du j-eme canal de l'image i (nouvelle taille : mn * pq)
                # Le reshappe est indu du calcul des Ak
                R_x = self.reshape(xx[:,:,j], self.akSize[1], self.akSize[2], self.bkSize[0], self.bkSize[1])
                concat_R_x_channel.append(R_x)
            concat_R_x_batch.append(concat_R_x_channel)

        concat_R_x_batch = tf.convert_to_tensor(concat_R_x_batch)

        # Reshape du tenseur obtenu car il faut une matrice pour effectuer le dot_product de la ligne 99
        # qui sert à calculer les Ak.
        # Voir si on peut pas faire cela différement (produit de chaque matrice dans la profondeur?)
        # Dessiner ça pour mieux voir les choses.
        # On se retrouverais avec la variable dot_product de dimenssion 3.
        # On pourrait alors faire un reshappe (+ petit?) à ce moment la pour retrouver les Ak comme précédement.
        concat_R_x_batch = K.reshape(concat_R_x_batch, (int(concat_R_x_batch.shape[0])*int(concat_R_x_batch.shape[2]), int(concat_R_x_batch.shape[3])*int(xx.shape[2])))

        # Liste de tout les Ak qui ont été calculés
        Aks = []
        # TODO: Faire tout les projection (ie calcul des Ak) en meme temps.
        # Pour toutes les valeurs du rank de kronecker
        for k in range(self.kroneckerRank):
            Bk = self.B[k]
            # Reshappe du Bk pour pouvoir trouver les Ak.
            # Le '-1' demande à la fonction reshappe d'inférer sur la taille de Bk pour trouver les dim nécessaire
            # à partir de la valeur de dimenssion qu'on lui donne.
            Bk = tf.reshape(Bk, (-1, 1)) #self.bkSize[0] * self.bkSize[1]))
            BkT = tf.transpose(Bk)

            b_inverse = 1. / K.dot(BkT, Bk)
            dot_product = K.dot(concat_R_x_batch, Bk)
            Ak = dot_product * b_inverse

            # Reshappe du Ak pour être utilisé par le réseau
            Ak = tf.reshape(Ak, (self.batch_size, self.akSize[1], self.akSize[2]))
            Aks.append(Ak)

        # Convert to tensor parce que c'est une liste.
        Aks = tf.convert_to_tensor(Aks)

        # Transpose pour passer la dimenssion de la taille du rang de kronecker en dernière place.
        Aks = tf.transpose(Aks, perm=(1,2,3,0))
        print('end call...')
        return Aks

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.akSize[1], self.akSize[2], self.akSize[0])

