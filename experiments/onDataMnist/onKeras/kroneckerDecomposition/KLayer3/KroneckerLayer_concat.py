#!/usr/bin/env python
# -*- coding: utf-8 -*-
import keras.regularizers
from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import tensorflow as tf
from keras.initializers import RandomUniform

# TODO: Chercher à optimiser ce bordel pour égaler un CNN (time, loss et acc)

# TODO: Générer des Bk déjà vectorisé
# TODO: Tester avec l'utilisation des multi-cannaux mais sans concaténation des minibatchs (ce qui est le cas pour cette version)
# TODO: Changer l'utilisation des liste suivient d'un convert_to_tensor par un placeholder du bon shape directement.
# TODO: Rename variable + comment the code.
# TODO: Remplacer les appel tf... par K... quand possible pour rendre compatible avec theano.
# TODO: |--> Quand pas possible réfléchir pour rendre cela compatible quand même.

## Notation :
# batch : Enesemble de batchSize images
# batchSize : Ensemble d'image que l'on donne d'un coup au réseau.
# M : Image d'entrée de taille (n*p, m*q)
# K : Rang de kronecker
# B: Enesemble des K filtres de taille (p, q)
# bk : k-ieme filtre
# A : Ensemble de K matrice de pondérations de taille (n, m)
# Ak : k-ieme matrice de ponération

class KroneckerLayer(Layer):

    def reshape(self, M_, m_, n_, p_, q_):
        # reshape M -> MM
        #MM_ = tf.convert_to_tensor(np.ndarray((m_*n_, p_*q_)))
        
        MM_ = tf.zeros([0, p_*q_], tf.float32) #tf.placeholder(tf.float32, [m_*n_, p_*q_])
        #MM_ = []
        for i in range(m_):
            for j in range(n_):
                #MM_[i*n_+j] = tf.reshape(M_[i*p_:(i+1)*p_, j*q_:(j+1)*q_], (p_*q_,1)) # que pour minibactch=1 et graylevel
                MM_ = tf.concat([MM_, tf.reshape(M_[i*p_:(i+1)*p_, j*q_:(j+1)*q_], (1,p_*q_))], axis=0) # que pour minibactch=1 et graylevel
       
        #print('MM_.shape=', MM_.shape)
        ##TMM = tf.convert_to_tensor(MM_)
        #TMM = K.concatenate(MM_, axis=0)
        #return K.reshape(TMM, (m_*n_, p_*q_))
        return MM_

    def __init__(self, bkSize, kroneckerRank, batch_size, regularizer=None, **kwargs):
        self.bkSize = bkSize
        self.kroneckerRank = kroneckerRank
        self.batch_size = int(batch_size)
        self.regularizer = regularizer
        super(KroneckerLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Nombre de cannaux de l'image
        nbChannels = input_shape[-1]
        print('begin build, nbChannels=', nbChannels)
        # Calcul de akSize à partir de la taille de l'image d'entrée et des bkSize.
        self.akSize = (self.kroneckerRank, int(input_shape[1] / self.bkSize[0]), int(input_shape[2] / self.bkSize[1]))

        # Calcul de la taille des imagettes après concaténation (en ligne, donc il n'y a que les bkSize[1] qui est multiplié par le nombre de cannaux).
        # Remarque : je crois que bkSizeBig n'est utilisé que dans la ligne dessous... à voir.
        self.bkSizeBig = (self.bkSize[0], self.bkSize[1] * nbChannels)
        self.B = self.add_weight(shape=(self.kroneckerRank, self.bkSizeBig[0], self.bkSizeBig[1]),
        initializer=RandomUniform(minval=-5, maxval=5, seed=None), trainable=True, regularizer=self.regularizer)
        #self.B = self.add_weight(shape=(self.kroneckerRank, self.bkSizeBig[0], self.bkSizeBig[1]), initializer='uniform', trainable=True)
        super(KroneckerLayer, self).build(input_shape)  # Be sure to call this somewhere!
        print('end build!')


    # Appel de cette fonction pour créer le moule (ie compilation de la fonction avec les paramètres custom) (=allocation mémoire?)
    def call(self, x):
        #print('begin call... ', x.shape)
        #block_R_x = K.placeholder(shape=(self.batch_size, self.akSize[1] * self.akSize[2], self.bkSize[0] * self.bkSize[1]))

        # Liste qui contiendra tout les images du minibatch reshappées.
        # En vrai pas les images mais la liste des 'images' sur chaque canaux.
        #concat_R_x_batch = [None] * self.batch_size
        concat_R_x_batch = tf.zeros([0, self.bkSizeBig[0] * self.bkSizeBig[1]], tf.float32) #tf.placeholder(tf.float32, [m_*n_, p_*q_])
        
        m_, n_, p_, q_ = self.akSize[1], self.akSize[2], self.bkSize[0], self.bkSize[1]
        # Pour toute les images du mini-batche
        for i in range(self.batch_size):

            # Récupération de la i-eme image du mini-batche
            xx = x[i]
            #print('xx.shape=',xx.shape)
            # Liste qui contiendra les 'images' de chaque cannal
            #concat_R_x_channel = [None] * int(xx.shape[2])
            concat_R_x_channel = tf.zeros([self.akSize[1]*self.akSize[2], 0], tf.float32)
            
            # Pour tout les cannaux de l'image i
            for j in range(int(xx.shape[2])):

                # Reshappe de l'image' du j-eme canal de l'image i (nouvelle taille : mn * pq)
                # Le reshappe est indu du calcul des Ak
                #R_x = self.reshape(xx[:,:,j], self.akSize[1], self.akSize[2], self.bkSize[0], self.bkSize[1])
                xx_ = xx[:,:,j] 
                R_x = tf.zeros([0, p_*q_], tf.float32) #tf.placeholder(tf.float32, [m_*n_, p_*q_])
                for ii in range(m_):
                    for jj in range(n_):
                        R_x = tf.concat([R_x, tf.reshape(xx_[ii*p_:(ii+1)*p_, jj*q_:(jj+1)*q_], (1,p_*q_))], axis=0) # que pour minibactch=1 et graylevel
                
                concat_R_x_channel = tf.concat([concat_R_x_channel, R_x], axis=1)
            
            #Tconcat_R_x_channel = K.concatenate(concat_R_x_channel, axis=0)
            concat_R_x_batch = tf.concat([concat_R_x_batch, concat_R_x_channel], axis=0)

        Tconcat_R_x_batch = concat_R_x_batch
        #print('Tconcat_R_x_batch.shape=', Tconcat_R_x_batch.shape)
        #Tconcat_R_x_batch = K.concatenate(concat_R_x_batch, axis=0)
        # Reshape du tenseur obtenu car il faut une matrice pour effectuer le dot_product de la ligne 99
        # qui sert à calculer les Ak.
        # Voir si on peut pas faire cela différement (produit de chaque matrice dans la profondeur?)
        # Dessiner ça pour mieux voir les choses.
        # On se retrouverais avec la variable dot_product de dimenssion 3.
        # On pourrait alors faire un reshappe (+ petit?) à ce moment la pour retrouver les Ak comme précédement.
        #Tconcat_R_x_batch = K.reshape(Tconcat_R_x_batch, (int(Tconcat_R_x_batch.shape[0])*int(Tconcat_R_x_batch.shape[2]), int(Tconcat_R_x_batch.shape[3])*int(xx.shape[2])))

        # Liste de tout les Ak qui ont été calculés
        Aks = [None] * self.kroneckerRank
        
        # TODO: Faire tout les projection (ie calcul des Ak) en meme temps.
        # Pour toutes les valeurs du rank de kronecker
        for k in range(self.kroneckerRank):
            Bk = self.B[k]
            # Reshappe du Bk pour pouvoir trouver les Ak.
            # Le '-1' demande à la fonction reshappe d'inférer sur la taille de Bk pour trouver les dim nécessaire
            # à partir de la valeur de dimenssion qu'on lui donne.
            Bk = K.reshape(Bk, (-1, 1)) #self.bkSize[0] * self.bkSize[1]))
            BkT = K.transpose(Bk)

            b_inverse = 1. / K.dot(BkT, Bk)
            dot_product = K.dot(Tconcat_R_x_batch, Bk)
            Ak = dot_product * b_inverse

            # Reshappe du Ak pour être utilisé par le réseau
            Ak = K.reshape(Ak, (self.batch_size, self.akSize[1], self.akSize[2]))
            Aks[k] = Ak
        
        # Convert to tensor parce que c'est une liste.
        TAks = K.concatenate(Aks, axis=0)
        TAks = K.reshape(TAks, (self.kroneckerRank, self.batch_size, self.akSize[1], self.akSize[2]))
        
        # Transpose pour passer la dimenssion de la taille du rang de kronecker en dernière place.
        TAks = K.permute_dimensions(TAks, (1,2,3,0))
        print('end call...')
        return TAks

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.akSize[1], self.akSize[2], self.akSize[0])
    
