from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import tensorflow as tf

class KroneckerLayer(Layer):

    def reshape(self, M_, m_, n_, p_, q_):
	    # reshape M -> MM
        #MM_ = tf.convert_to_tensor(np.ndarray((m_*n_, p_*q_)))
        print('M_.shape = ', M_.shape)

        MM_ = []
        for i in range(m_):
            for j in range(n_):
                MM_.append(tf.reshape(M_[i*p_:(i+1)*p_, j*q_:(j+1)*q_], (p_*q_,1))) # que pour minibactch=1 et graylevel
        tfMM = tf.convert_to_tensor(MM_)
        return tf.reshape(tfMM, (m_*n_, p_*q_))

    def __init__(self, bkSize, kroneckerRank, **kwargs):
        self.bkSize = bkSize
        print("Bk.shape : " + str(self.bkSize))
        self.kroneckerRank = kroneckerRank
        super(KroneckerLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.akSize = (self.kroneckerRank, int(input_shape[1] / self.bkSize[0]), int(input_shape[2] / self.bkSize[1]))
        print("Ak.shape : " + str(self.akSize))
        self.B = self.add_weight(shape=(self.kroneckerRank, self.bkSize[0], self.bkSize[1]), initializer='uniform', trainable=True)
        super(KroneckerLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):

        Aks_batch = []
        for i in range(1): #10 # minibatch naive implementation. Marche pas
            xx = tf.reshape(x[i], (int(x.shape[1]), int(x.shape[2]))) # marche en graylevel uniquement
            R_x = self.reshape(xx, self.akSize[1], self.akSize[2], self.bkSize[0], self.bkSize[1])
        
            Aks = [] 
            for k in range(self.kroneckerRank):
                Bk = self.B[k]
                Bk = tf.reshape(Bk, (-1, 1)) #self.bkSize[0] * self.bkSize[1]))
                BkT = tf.transpose(Bk)

                b_inverse = 1. / K.dot(BkT, Bk)
                dot_product = K.dot(R_x, Bk)
                Ak = dot_product * b_inverse

                Ak = tf.reshape(Ak, (self.akSize[1], self.akSize[2]))
                Aks.append(Ak)
                
            Aks_batch.append(Aks)
        
        #Aks = tf.convert_to_tensor(Aks_batch) # avec minibatch: ne marche pas 
        Aks = tf.convert_to_tensor(Aks_batch[0]) # sans minibatch
        return Aks

    def compute_output_shape(self, input_shape):
        print('output_shape=',(self.akSize[0], self.akSize[1], self.akSize[2]))
        #return (input_shape[0], self.akSize[0], self.akSize[1], self.akSize[2])
        return (self.akSize[0], self.akSize[1], self.akSize[2])

