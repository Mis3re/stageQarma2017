#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import sys
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D
from keras.layers.normalization import BatchNormalization
from keras.initializers import RandomUniform
from PIL import Image
import numpy as np

# EXEMPLE : python mnist_1.py "cnn" True 0.25 0.25 3 3 10 "adam"

# Parametres :
# type(Str): "cnn" ou "kro"
# batchNormaisation(Bool): True ou False
# relu(int): 0=Non; 1=Oui, juste après les couches; 2=Oui, après la couche de batchNorm
# dropOut1_value(float): 0 / 0,25 / 0,5
# dropOut2_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn"
# K2(int): nombre de filtres de la deuxièmre couche (de convolution)
# batch_size(int): Taille du miniBatch
# optimizer(str): Fonction utilisée pour la descente de gradient stochastique "rmsprop" ou "adagrad" ou "adam"


type = "cnn"
batchNormaisation = True
relu = 1
dropOut1_value = 0.5
dropOut2_value = 0.25
K1 = 32
K2 = 32
batch_size = 25
optimizer = "adam"

print("\nPARAMETRES:")
print("type : " + str(type))
print("batchNormaisation : " + str(batchNormaisation))
print("relu : " + str(relu))
print("dropOut1_value : " + str(dropOut1_value))
print("dropOut2_value : " + str(dropOut2_value))
print("K1 : " + str(K1))
print("K2 : " + str(K2))
print("batch_size : " + str(batch_size))
print("optimizer : " + str(optimizer))
print

if optimizer == "rmsprop":
    optimizer=keras.optimizers.RMSprop()
elif optimizer == "adagrad":
    optimizer =keras.optimizers.Adagrad()
else:   # "adam"
    optimizer =keras.optimizers.Adam()

epochs=10
num_classes = 10
nbNeuronDense = 128
# input image dimensions
img_rows, img_cols = 28, 28

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

x_validation = x_train[:10000]
y_validation = y_train[:10000]
x_train = x_train[10000:]
y_train = y_train[10000:]

# CREATION DE L'ARCHITECTURE
model = Sequential()

if type == "kro":
    model.add(KroneckerLayer((4, 4), K1, input_shape=input_shape, batch_size=batch_size))
    if relu == 1:
        model.add(Activation('relu'))
else:   # "cnn"
    model.add(Conv2D(K1, (4, 4), strides=(4, 4), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None), input_shape=input_shape))
    if relu == 1:
        model.add(Activation('relu'))

if batchNormaisation:
    model.add(BatchNormalization(axis=-1))
if relu == 2:
    model.add(Activation('relu'))

model.add(Conv2D(K2, (3, 3), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None)))
model.add(Activation('relu'))

model.add(Dropout(dropOut1_value))

model.add(Flatten())
model.add(Dense(nbNeuronDense))
model.add(Activation('relu'))

model.add(Dropout(dropOut2_value))

model.add(Dense(num_classes, activation='softmax'))
model.summary()

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=optimizer,
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=2,
          validation_data=(x_validation, y_validation))

# Normalisation de chaque image par rapport à elle même
Bks = model.layers[0].get_weights()[0]
print("Bks.shape = ", Bks.shape)
min = np.min(Bks, axis=-1)
max = np.max(Bks, axis=-1)
Bks = (Bks - min) / (max - min)

print(Bks)
Bks = Bks * 255.
Bks = Bks.reshape((4,4,K1))
for i in range(Bks.shape[2]):
    im = Image.fromarray(Bks[:,:,i])
    im2 = im.convert('L')
    im2.save('normalLocal/Filtre_' + str(i) + '.jpg')

# Normalisation de chaque image par rapport à l'ensemble des images
Bks = model.layers[0].get_weights()[0]
min = np.min(Bks)
max = np.max(Bks)
Bks = (Bks - min) / (max - min)
print(Bks)

Bks = Bks * 255.
Bks = Bks.reshape((4,4,K1))
for i in range(Bks.shape[2]):
    im = Image.fromarray(Bks[:,:,i])
    im2 = im.convert('L')
    im2.save('normalGlobal/Filtre_' + str(i) + '.jpg')

score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
print('\nTest loss:', score[0])
print('Test accuracy:', score[1])

