Filtres obtenuent après la première couche de convolution du réseau mnist_1 avec les paramètres suivants : cnn True 1 0.5 0.25 32 32 25 adam (20 epochs)
Pour rappel, l'archi de cette couche possède 3 couches cachées : 
1) couche de convolution avec fenêtre de taille (4,4) et strides (ie pas) de taille (4,4) également (pour la symétrie avec la couche de kronecker que l'on veux comparer).
2) couche de convolution avec fenêtre de taille (3,3) et strides (ie pas) de taille (1,1).
3) couche dense de 128 neurones.

 
