#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Input, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
#from KroneckerLayer_stride import KroneckerLayer
from KroneckerLayer import KroneckerLayer
#from KroneckerLayer_concat import KroneckerLayer
#from KroneckerLayer_v1 import KroneckerLayer
from keras.layers.normalization import BatchNormalization
from PIL import Image
from keras import regularizers
from keras.layers.core import ActivityRegularization
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau
import numpy as np
import tensorflow as tf

sess = tf.Session(config=tf.ConfigProto(intra_op_parallelism_threads=16, inter_op_parallelism_threads=16))
K.set_session(sess)


batch_size = 100
print ("batch_size = " + str(batch_size))
num_classes = 10
epochs = 10


# input image dimensions
img_rows, img_cols = 28, 28

# the data, shuffled and split between train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

print('x_train shape:', x_train.shape)

# create 2nd channel
#x_train2 = x_train[..., None] * np.ones(2, dtype=float)
#x_train = x_train2.reshape((x_train2.shape[0], x_train2.shape[1], x_train2.shape[2], 2))
#x_test2 = x_test[..., None] * np.ones(2, dtype=float)
#x_test = x_test2.reshape((x_test2.shape[0], x_test2.shape[1], x_test2.shape[2], 2))
#input_shape = (img_rows, img_cols, 2)
#print('x_train shape:', x_train.shape)

# crop center to 24x24
x_train = x_train[:,2:-2,2:-2,:]
x_test = x_test[:,2:-2,2:-2,:]
input_shape = x_train[0].shape
print('x_train shape:', input_shape)

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# model = Sequential()


inpu = Input(shape=input_shape)

kro1 = KroneckerLayer((4,4), 128, batch_size=batch_size, activation='linear', regularizer=None)(inpu)
#kro1b = Flatten()(KroneckerLayer((4,4), 32, batch_size=batch_size, activation='relu', regularizer=None)(kro1))
bn1 = Activation('relu')(Flatten()(BatchNormalization(axis=-1)(kro1)))

kro2 = KroneckerLayer((2,2), 256, batch_size=batch_size, activation='linear', regularizer=None)(inpu)
#kro2b = Flatten()(KroneckerLayer((2,2), 32, batch_size=batch_size, activation='relu', regularizer=None)(kro2))
bn2 = Activation('relu')(Flatten()(BatchNormalization(axis=-1)(kro2)))

kro3 = KroneckerLayer((8,8), 64, batch_size=batch_size, activation='linear', regularizer=None)(inpu)
#kro3b = Flatten()(KroneckerLayer((2,2), 32, batch_size=batch_size, activation='relu', regularizer=None)(kro2))
bn3 = Activation('relu')(Flatten()(BatchNormalization(axis=-1)(kro3)))

#kro3 = KroneckerLayer((8,8), 32, batch_size=batch_size, activation='relu', regularizer=None)(inpu)

bn = keras.layers.concatenate([bn1, bn2, bn3])
#bn = BatchNormalization(axis=-1)(concatenated)

#fl = Flatten()
dense1 = Dense(2048, activation='relu')(bn)
dense1 = Dense(512, activation='relu')(dense1)
dropout1 = Dropout(0.5)(dense1)
#dense2 = Dense(128, activation='relu')(dropout1)
#dropout2 = Dropout(0.5)(dense2)
outpu = Dense(num_classes, activation='softmax')(dropout1)

model = Model(inputs=[inpu], outputs=outpu)

print(model.summary())

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

checkpointer = ModelCheckpoint(monitor='val_acc', filepath="weights_kro.h5", verbose=1, save_best_only=True)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=10, min_lr=0.001)


model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test),
          callbacks = [checkpointer, reduce_lr])

#model.load_weights('weights_kro.h5')
#score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
#print('\nTest loss:', score[0])
#print('Test accuracy:', score[1])

modelb = Model(inputs=model.input, outputs=model.layers[1].output)
outputs = modelb.predict(x_test[:100], batch_size=100)[0,:,:,0]
print(outputs)

Bks = model.layers[1].get_weights()[0]
print(Bks)
Bks = Bks * 127. + 127.
for i in range(len(Bks)):
    im = Image.fromarray(Bks[i])
    im2 = im.convert('L')
    im2.save('Bks1_'+str(i)+'.jpg')

Bks = model.layers[3].get_weights()[0]
print(Bks)
Bks = Bks * 127. + 127.
for i in range(len(Bks)):
    im = Image.fromarray(Bks[i])
    im2 = im.convert('L')
    im2.save('Bks2_'+str(i)+'.jpg')

Bks = model.layers[5].get_weights()[0]
print(Bks)
Bks = Bks * 127. + 127.
for i in range(len(Bks)):
    im = Image.fromarray(Bks[i])
    im2 = im.convert('L')
    im2.save('Bks3_'+str(i)+'.jpg')

#for i in range(len(Bks1)):
#    im = Image.fromarray(Bks1[i])
#    im2 = im.convert('L')
#    im2.save('Bks1_'+str(i)+'.jpg')

