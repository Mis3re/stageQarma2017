Filtres obtenuent après la (première) couche de kronecker du réseau mnist_1 avec les paramètres suivants : kro True 1 0.5 0.0 64 64 25 adam (10 epochs)
Pour rappel, l'archi de cette couche possède 3 couches cachées : 
1) couche de kronecker avec fenêtre de taille (4,4)).
2) couche de convolution avec fenêtre de taille (3,3) et strides (ie pas) de taille (1,1).
3) couche dense de 128 neurones.

 
