#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

from __future__ import print_function
import sys
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D
from keras.layers.normalization import BatchNormalization
from keras.initializers import RandomUniform
from PIL import Image
import numpy as np

# EXEMPLE : python mnist_2.py "cnn" True True 128 0.25 3 3

# type(Str): "cnn" ou "kro"
# batchNormaisation1(Bool): True ou False, après la 1ere couche
# batchNormaisation2(Bool): True ou False, après la 2eme couche
# relu(int): 0=Non; 1=Oui, juste après les couches; 2=Oui, après la couche de batchNorm
# nbNeuronDense(int): nombre de neurones de la 1ere denseLayer (la 2eme est la couche de sortie, elle a 10 neurones)
# dropOut_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [1ere couche]
# K2(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [2ere couche]

def main(type,
         batchNormaisation1,
         batchNormaisation2,
         relu,
         nbNeuronDense,
         dropOut_value,
         K1,
         K2):

    print("\nPARAMETRES:")
    print("type : " + str(type))
    print("batchNormaisation1 : " + str(batchNormaisation1))
    print("batchNormaisation2 : " + str(batchNormaisation2))
    print("relu : " + str(relu))
    print("nbNeuronDense : " + str(nbNeuronDense))
    print("dropOut_value : " + str(dropOut_value))
    print("K1 : " + str(K1))
    print("K2 : " + str(K2))
    print

    batch_size = 50
    epochs = 1
    optimizer = keras.optimizers.RMSprop()
    num_classes = 10
    # input image dimensions
    img_rows, img_cols = 28, 28

    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255

    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    x_validation = x_train[:10000]
    y_validation = y_train[:10000]
    x_train = x_train[10000:]
    y_train = y_train[10000:]

    # CREATION DE L'ARCHITECTURE
    model = Sequential()

    if type == "kro":
        model.add(KroneckerLayer((2, 2), K1, input_shape=input_shape, batch_size=batch_size))
        if relu == 1:
            model.add(Activation('relu'))

        if batchNormaisation1:
            model.add(BatchNormalization(axis=-1))

        if relu == 2:
            model.add(Activation('relu'))

        model.add(KroneckerLayer((2, 2), K2, batch_size=batch_size))
        if relu == 1:
            model.add(Activation('relu'))

    else:   # "cnn"
        model.add(Conv2D(K1, (2, 2), strides=(2, 2), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None), input_shape=input_shape))
        if relu == 1:
            model.add(Activation('relu'))

        if batchNormaisation1:
            model.add(BatchNormalization(axis=-1))

        if relu == 2:
            model.add(Activation('relu'))

        model.add(Conv2D(K2, (2, 2), strides=(2, 2), kernel_initializer=RandomUniform(minval=-1, maxval=1, seed=None)))
        if relu == 1:
            model.add(Activation('relu'))

    if batchNormaisation2:
        model.add(BatchNormalization(axis=-1))

    if relu == 2:
        model.add(Activation('relu'))

    model.add(Flatten())
    model.add(Dense(nbNeuronDense))
    model.add(Activation('relu'))

    model.add(Dropout(dropOut_value))

    model.add(Dense(num_classes, activation='softmax'))
    model.summary()


    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=optimizer,
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=2,
              validation_data=(x_validation, y_validation))

    # Normalisation de chaque image par rapport à elle même
    Bks = model.layers[0].get_weights()[0]
    min = np.min(Bks, axis=-1)
    max = np.max(Bks, axis=-1)
    Bks = (Bks - min) / (max - min)

    print(Bks)
    Bks = Bks * 255.
    Bks = Bks.reshape((4, 4, K1))
    for i in range(Bks.shape[2]):
        im = Image.fromarray(Bks[:, :, i])
        im2 = im.convert('L')
        im2.save('normalLocal/Filtre_' + str(i) + '.jpg')

    # Normalisation de chaque image par rapport à l'ensemble des images
    Bks = model.layers[0].get_weights()[0]
    min = np.min(Bks)
    max = np.max(Bks)
    Bks = (Bks - min) / (max - min)
    print(Bks)

    Bks = Bks * 255.
    Bks = Bks.reshape((4, 4, K1))
    for i in range(Bks.shape[2]):
        im = Image.fromarray(Bks[:, :, i])
        im2 = im.convert('L')
        im2.save('normalGlobal/Filtre_' + str(i) + '.jpg')





    # # Normalisation de chaque image par rapport à elle même
    # Bks = model.layers[0].get_weights()[0]
    # min = np.min(Bks, axis=-1)
    # max = np.max(Bks, axis=-1)
    # Bks = (Bks - min) / (max - min)
    #
    # print(Bks)
    # Bks = Bks * 255.
    # Bks = Bks.reshape((4, 4, 32))
    # for i in range(Bks.shape[2]):
    #     im = Image.fromarray(Bks[:, :, i])
    #     im2 = im.convert('L')
    #     im2.save('normalLocal/Filtre1_' + str(i) + '.jpg')
    #
    # # Normalisation de chaque image par rapport à l'ensemble des images
    # Bks = model.layers[0].get_weights()[0]
    # min = np.min(Bks)
    # max = np.max(Bks)
    # Bks = (Bks - min) / (max - min)
    # print(Bks)
    #
    # Bks = Bks * 255.
    # Bks = Bks.reshape((4, 4, 32))
    # for i in range(Bks.shape[2]):
    #     im = Image.fromarray(Bks[:, :, i])
    #     im2 = im.convert('L')
    #     im2.save('normalGlobal/Filtre1_' + str(i) + '.jpg')

    score = model.evaluate(x_test, y_test, batch_size=batch_size, verbose=1)
    print('\nTest loss:', score[0])
    print('Test accuracy:', score[1])


if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['type'] = str(sys.argv[1])
        kwargs['batchNormaisation1'] = bool(sys.argv[2])
        kwargs['batchNormaisation2'] = bool(sys.argv[3])
        kwargs['relu'] = int(sys.argv[4])
        kwargs['nbNeuronDense'] = int(sys.argv[5])
        kwargs['dropOut_value'] = float(sys.argv[6])
        kwargs['K1'] = int(sys.argv[7])
        kwargs['K2'] = int(sys.argv[8])
    main(**kwargs)