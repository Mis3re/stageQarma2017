#!/usr/bin/env python
# -*- coding: utf-8 -*-

# EXEMPLE : python mnist_2.py "cnn" True True 128 0.25 3 3

# type(Str): "cnn" ou "kro"
# batchNormaisation1(Bool): True ou False, après la 1ere couche
# batchNormaisation2(Bool): True ou False, après la 2eme couche
# nbNeuronDense(int): nombre de neurones de la 1ere denseLayer (la 2eme est la couche de sortie, elle a 10 neurones)
# dropOut_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [1ere couche]
# K2(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn" [2ere couche]

fileCnn = open('/Users/pa/Documents/stageQarma2017/expe/onDataMnist/tensorflow/gridShearch/gridOfParametersCnnMnist2.txt', 'w')
fileKro = open('/Users/pa/Documents/stageQarma2017/expe/onDataMnist/tensorflow/gridShearch/gridOfParametersKroMnist2.txt', 'w')

batchNormaisation1 = [False, True]
batchNormaisation2 = [False, True]
relu = [0, 1, 2]
nbNeuronDense = [128, 256]
dropOut_value = [0, 0.25, 0.5]
K1 = [16, 32, 64, 128, 256]   # K1 = K2

# type = "cnn"
for bool1 in batchNormaisation1:
    for bool2 in batchNormaisation2:
        for activ in relu:
            for nbNeuron in nbNeuronDense:
                for val in dropOut_value:
                    for K in K1:
                        string = str("cnn") + " " + str(bool1) + " " + str(bool2) + " " + str(activ) + " " + str(nbNeuron) + " " + str(val) + " " + str(K) + " " + str(K) + "\n"
                        fileCnn.write(string)

# type = "kro"
for bool1 in batchNormaisation1:
    for bool2 in batchNormaisation2:
        for activ in relu:
            for nbNeuron in nbNeuronDense:
                for val in dropOut_value:
                    for K in K1:
                        string = str("kro") + " " + str(bool1) + " " + str(bool2) + " " + str(activ) + " " + str(nbNeuron) + " " + str(val) + " " + str(K) + " " + str(K) + "\n"
                        fileKro.write(string)
