#!/usr/bin/env python
# -*- coding: utf-8 -*-

# EXEMPLE : python mnist_1.py "cnn" True 0.25 0.25 3 3 10 "adam"

# Parametres :
# type(Str): "cnn" ou "kro"
# batchNormaisation(Bool): True ou False
# dropOut1_value(float): 0 / 0,25 / 0,5
# dropOut2_value(float): 0 / 0,25 / 0,5
# K1(int): rang de kronecker si type="kro" ou nombre de filtres de convolution si type="cnn"
# K2(int): nombre de filtres de la deuxièmre couche (de convolution)
# batch_size(int): Taille du miniBatch
# optimizer(str): Fonction utilisée pour la descente de gradient stochastique "rmsprop" ou "adagrad" ou "adam"

fileCnn = open('/Users/pa/Documents/stageQarma2017/expe/onDataMnist/tensorflow/gridShearch/gridOfParametersCnnMnist1.txt', 'w')
fileKro = open('/Users/pa/Documents/stageQarma2017/expe/onDataMnist/tensorflow/gridShearch/gridOfParametersKroMnist1.txt', 'w')

batchNormaisation = [False, True]
relu = [0, 1, 2]
dropOut1_value = [0, 0.25, 0.5]
dropOut2_value = [0, 0.25, 0.5]
K1 = [16, 32, 64, 128, 256]   # K1 = K2 ou K2 = K1/2
optimizer = ["rmsprop", "adagrad", "adam"]
batch_size = [25, 50, 100, 125]

# type = "cnn"
for bool in batchNormaisation:
    for activ in relu:
        for val1 in dropOut1_value:
            for val2 in dropOut2_value:
                for K in K1:
                    for batch_s in batch_size:
                        for opt in optimizer:
                            string1 = str("cnn") + " " + str(bool) + " " + str(activ) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K) + " " + str(batch_s) + " " + str(opt) + "\n"
                            string2 = str("cnn") + " " + str(bool) + " " + str(activ) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K/2) + " " + str(batch_s) + " " + str(opt) + "\n"
                            fileCnn.write(string1)
                            fileCnn.write(string2)

# type = "kro"
for bool in batchNormaisation:
    for activ in relu:
        for val1 in dropOut1_value:
            for val2 in dropOut2_value:
                for K in K1:
                    for batch_s in batch_size:
                        for opt in optimizer:
                            string1 = str("kro") + " " + str(bool) + " " + str(activ) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K) + " " + str(batch_s) + " " + str(opt) + "\n"
                            string2 = str("kro") + " " + str(bool) + " " + str(activ) + " " + str(val1) + " " + str(val2) + " " + str(K) + " " + str(K/2) + " " + str(batch_s) + " " + str(opt) + "\n"
                            fileKro.write(string1)
                            fileKro.write(string2)