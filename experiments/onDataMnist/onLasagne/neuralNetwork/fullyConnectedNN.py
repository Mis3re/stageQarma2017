#!/usr/bin/env python

'''
Reseau de base pour les donnees MNIST
Le programme prend en argument le nombre d epochs (500 par defaut)

'''

from __future__ import print_function
from __future__ import absolute_import      # permet les imports relatifs (cf python3)

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne

from .. import Utils

# ##################### Construction du modele #######################


def build_mlp(input_var=None):
    # MLP de 2 couches cachees contenant 800 neurones chacunes suivies d'une couche de sortie contenant 10 neurones. 

    l_in = lasagne.layers.InputLayer(shape=(None, 1, 28, 28),
                                     input_var=input_var)
    

    # Couche fully-connected suivie d'une ReLU
    l_hid1 = lasagne.layers.DenseLayer(
            l_in, num_units=800,
            nonlinearity=lasagne.nonlinearities.rectify,
            W=lasagne.init.GlorotUniform())

    # Couche fully-connected suivie d'une ReLU
    l_hid2 = lasagne.layers.DenseLayer(
            l_hid1, num_units=800,
            nonlinearity=lasagne.nonlinearities.rectify)

    # Couche fully-connected suivie d'une ReLU
    l_hid3 = lasagne.layers.DenseLayer(
            l_hid2, num_units=800,
            nonlinearity=lasagne.nonlinearities.rectify)


    # Couche de sortie fully-connected suivie d'une softmax
    l_out = lasagne.layers.DenseLayer(
            l_hid3, num_units=10,
            nonlinearity=lasagne.nonlinearities.softmax)
    

    return l_out

# ############################## Programme principal ################################

def main(num_epochs=500):
    # Chargement des donnees
    print("Loading data...")
    X_train, y_train, X_val, y_val, X_test, y_test = Utils.load_dataset()
    np.save('../jeux/X_train.npy',X_train)
    np.save('../jeux/y_train.npy',y_train)
    np.save('../jeux/X_val.npy',X_val)
    np.save('../jeux/y_val.npy',y_val)
    np.save('../jeux/X_test.npy',X_test)
    np.save('../jeux/y_test.npy',y_test)

    # Preparation des variables Theano pour les inputs et targets
    input_var = T.tensor4('inputs')
    target_var = T.ivector('targets')

    # Creation du modele
    print("Building model and compiling functions...")
    
    network = build_mlp(input_var)

    # Creation de la fonction loss pour la phase de training -> cross-entropy loss:
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    loss = loss.mean()

    # Creation de la fonction de mise a jour pour la phase de training -> Stochastic Gradient
    # Descent (SGD) with Nesterov momentum.
    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.nesterov_momentum(
            loss, params, learning_rate=0.01, momentum=0.9)

    # Creation de la fonction loss  pour les phases de validation/test
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var)
    test_loss = test_loss.mean()
    # expression pour la precision de la classification :
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

    # Compilation d une fonction performant une etape de training sur un mini-batch (en donnant
    # le dictionnaire des mises a jour) et retournant les training loss correspondant:
    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    # Compilation d une seconde fonction calculant la validation loss et la precision:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    # Boucle pour la phase de training.
    print("Starting training...")
    start=time.time()
    #Pour chaque epoch:
    for epoch in range(num_epochs):
        # on itere sur toute la base de training : 
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in Utils.iterate_minibatches(X_train, y_train, 500, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1

        # et toute la base de validation:
        val_err = 0
        val_acc = 0
        val_batches = 0
        for batch in Utils.iterate_minibatches(X_val, y_val, 500, shuffle=False):
            inputs, targets = batch
            err, acc = val_fn(inputs, targets)
            val_err += err
            val_acc += acc
            val_batches += 1

        #Affichage des resultats pour chaque epoch:
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
        print("  validation accuracy:\t\t{:.2f} %".format(
            val_acc / val_batches * 100))

    # Apres la phase de training, on calcule et affiche l erreur pour la phase de test:
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in Utils.iterate_minibatches(X_test, y_test, 500, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.2f} %".format(
        test_acc / test_batches * 100))

    # Affichage du temps total
    print("Total time {:.3f}s".format(time.time() - start))

    ###Sauvegarde des parametres###
    
    print("Saving parameters ...")
    all_params = lasagne.layers.get_all_params(network)
    np.save('../poids/W_layer1.npy', all_params[0].get_value())
    np.save('../poids/b_layer1.npy', all_params[1].get_value())
    np.save('../poids/W_layer2.npy', all_params[2].get_value())
    np.save('../poids/b_layer2.npy', all_params[3].get_value())
    np.save('../poids/W_layer3.npy', all_params[4].get_value())
    np.save('../poids/b_layer3.npy', all_params[5].get_value())
    print("Done ! ")
    

if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [MODEL [EPOCHS]]" % sys.argv[0])
        print()
        print("MODEL: 'mlp' for a simple Multi-Layer Perceptron (MLP),")
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['num_epochs'] = int(sys.argv[1])
        main(**kwargs)



