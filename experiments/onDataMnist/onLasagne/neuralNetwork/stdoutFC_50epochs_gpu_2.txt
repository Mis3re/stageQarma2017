Loading data...
Building model and compiling functions...
Starting training...
Epoch 1 of 50 took 16.081s
  training loss:		0.939565
  validation loss:		0.327657
  validation accuracy:		90.80 %
Epoch 2 of 50 took 16.015s
  training loss:		0.314917
  validation loss:		0.254213
  validation accuracy:		92.78 %
Epoch 3 of 50 took 16.207s
  training loss:		0.254570
  validation loss:		0.219762
  validation accuracy:		93.67 %
Epoch 4 of 50 took 16.017s
  training loss:		0.216858
  validation loss:		0.191042
  validation accuracy:		94.65 %
Epoch 5 of 50 took 16.014s
  training loss:		0.188519
  validation loss:		0.170817
  validation accuracy:		95.39 %
Epoch 6 of 50 took 16.030s
  training loss:		0.166274
  validation loss:		0.158772
  validation accuracy:		95.70 %
Epoch 7 of 50 took 16.035s
  training loss:		0.148177
  validation loss:		0.141775
  validation accuracy:		96.22 %
Epoch 8 of 50 took 16.018s
  training loss:		0.132907
  validation loss:		0.132495
  validation accuracy:		96.46 %
Epoch 9 of 50 took 16.011s
  training loss:		0.120130
  validation loss:		0.124670
  validation accuracy:		96.64 %
Epoch 10 of 50 took 16.056s
  training loss:		0.108720
  validation loss:		0.119060
  validation accuracy:		96.69 %
Epoch 11 of 50 took 16.011s
  training loss:		0.098651
  validation loss:		0.111291
  validation accuracy:		96.96 %
Epoch 12 of 50 took 16.006s
  training loss:		0.089969
  validation loss:		0.105816
  validation accuracy:		96.99 %
Epoch 13 of 50 took 16.040s
  training loss:		0.082688
  validation loss:		0.102278
  validation accuracy:		97.13 %
Epoch 14 of 50 took 16.003s
  training loss:		0.076152
  validation loss:		0.101303
  validation accuracy:		97.16 %
Epoch 15 of 50 took 16.010s
  training loss:		0.069609
  validation loss:		0.095455
  validation accuracy:		97.28 %
Epoch 16 of 50 took 16.009s
  training loss:		0.064079
  validation loss:		0.092739
  validation accuracy:		97.29 %
Epoch 17 of 50 took 16.015s
  training loss:		0.059460
  validation loss:		0.088502
  validation accuracy:		97.42 %
Epoch 18 of 50 took 16.022s
  training loss:		0.054586
  validation loss:		0.088483
  validation accuracy:		97.45 %
Epoch 19 of 50 took 15.991s
  training loss:		0.050490
  validation loss:		0.086295
  validation accuracy:		97.54 %
Epoch 20 of 50 took 16.008s
  training loss:		0.046984
  validation loss:		0.086849
  validation accuracy:		97.45 %
Epoch 21 of 50 took 16.015s
  training loss:		0.043710
  validation loss:		0.082111
  validation accuracy:		97.61 %
Epoch 22 of 50 took 16.022s
  training loss:		0.039981
  validation loss:		0.082693
  validation accuracy:		97.55 %
Epoch 23 of 50 took 16.016s
  training loss:		0.037486
  validation loss:		0.086679
  validation accuracy:		97.58 %
Epoch 24 of 50 took 15.992s
  training loss:		0.034684
  validation loss:		0.083012
  validation accuracy:		97.61 %
Epoch 25 of 50 took 16.007s
  training loss:		0.032177
  validation loss:		0.079233
  validation accuracy:		97.64 %
Epoch 26 of 50 took 16.017s
  training loss:		0.029458
  validation loss:		0.079587
  validation accuracy:		97.66 %
Epoch 27 of 50 took 15.999s
  training loss:		0.027305
  validation loss:		0.079534
  validation accuracy:		97.69 %
Epoch 28 of 50 took 16.024s
  training loss:		0.025533
  validation loss:		0.079117
  validation accuracy:		97.70 %
Epoch 29 of 50 took 16.024s
  training loss:		0.023507
  validation loss:		0.075834
  validation accuracy:		97.76 %
Epoch 30 of 50 took 15.999s
  training loss:		0.021812
  validation loss:		0.077451
  validation accuracy:		97.81 %
Epoch 31 of 50 took 16.003s
  training loss:		0.020207
  validation loss:		0.077650
  validation accuracy:		97.77 %
Epoch 32 of 50 took 16.000s
  training loss:		0.018650
  validation loss:		0.077087
  validation accuracy:		97.78 %
Epoch 33 of 50 took 16.011s
  training loss:		0.017223
  validation loss:		0.077502
  validation accuracy:		97.80 %
Epoch 34 of 50 took 16.001s
  training loss:		0.016151
  validation loss:		0.078429
  validation accuracy:		97.76 %
Epoch 35 of 50 took 16.010s
  training loss:		0.015150
  validation loss:		0.077453
  validation accuracy:		97.80 %
Epoch 36 of 50 took 15.998s
  training loss:		0.013820
  validation loss:		0.080338
  validation accuracy:		97.71 %
Epoch 37 of 50 took 16.010s
  training loss:		0.013083
  validation loss:		0.077437
  validation accuracy:		97.81 %
Epoch 38 of 50 took 16.002s
  training loss:		0.012252
  validation loss:		0.076683
  validation accuracy:		97.87 %
Epoch 39 of 50 took 15.998s
  training loss:		0.011268
  validation loss:		0.078744
  validation accuracy:		97.80 %
Epoch 40 of 50 took 16.010s
  training loss:		0.010599
  validation loss:		0.079479
  validation accuracy:		97.79 %
Epoch 41 of 50 took 15.998s
  training loss:		0.009896
  validation loss:		0.077740
  validation accuracy:		97.88 %
Epoch 42 of 50 took 15.992s
  training loss:		0.009303
  validation loss:		0.077249
  validation accuracy:		97.89 %
Epoch 43 of 50 took 15.993s
  training loss:		0.008573
  validation loss:		0.077914
  validation accuracy:		97.90 %
Epoch 44 of 50 took 15.987s
  training loss:		0.008039
  validation loss:		0.080070
  validation accuracy:		97.84 %
Epoch 45 of 50 took 15.981s
  training loss:		0.007775
  validation loss:		0.080470
  validation accuracy:		97.92 %
Epoch 46 of 50 took 15.981s
  training loss:		0.007203
  validation loss:		0.080118
  validation accuracy:		97.86 %
Epoch 47 of 50 took 15.992s
  training loss:		0.006746
  validation loss:		0.079650
  validation accuracy:		97.83 %
Epoch 48 of 50 took 15.993s
  training loss:		0.006431
  validation loss:		0.079155
  validation accuracy:		98.02 %
Epoch 49 of 50 took 15.997s
  training loss:		0.005984
  validation loss:		0.080481
  validation accuracy:		97.85 %
Epoch 50 of 50 took 15.995s
  training loss:		0.005675
  validation loss:		0.079495
  validation accuracy:		97.89 %
Final results:
  test loss:			0.068651
  test accuracy:		98.10 %
Total time 801.696s
Saving parameters ...
Done ! 
