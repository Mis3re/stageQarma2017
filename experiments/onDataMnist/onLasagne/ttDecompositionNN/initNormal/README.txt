Info à savoir : la grille des paramètres 'gridSearch' pour cette archi avec TTLayer sur les données mnist à été générée à partir des 5 meilleurs jeux de paramètres (en val) des expés réalisées sur mnist avec CPLayer.
Dans l'urgence j'ai généré cette grille de paramètres au sein du fichier "generate_best_and_worsts_results.py" qui se trouve dans ../CPDecomposition/exploit/

Deux expés ici : la première comme décrite ci dessus avec une initialization des core-TT avec une loi normale sur [0, 1]. La deuxième avec seulement le reshape « input_shape = 2-2-14-14 et output_shape = 5-5-8-4 » (gridSearch sur les rangs) avec cette fois ci une initialization des core-TT avec une loi uniforme sur [-1, 1].

edit: j'ai séparé dans 2 dossiers différents les expes ou l'initialisation suit une loi normal entre 0 et 1 et celles ou l'init suit une loi uniforme entre -1 et 1.


Rmq: dans le fichier result.txt se trouve concaténés tout les résultats part_x