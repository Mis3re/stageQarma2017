#!/usr/bin/env python
#-*- coding: utf-8 -*-

from mnistwTTLayer import main

folder_path = os.getcwd() + "/"
grid = open(folder_path + "gridOfParameters/gridOfParametersForTTLayer.txt", 'r')
results = open(folder_path + "results.txt", 'w')
param = grid.readline()

while not param == "":
    param = param.split(" ")
    num_epochs = param[0]
    tt_input_shape = param[1]
    tt_output_shape = param[2]
    tt_ranks = param[3]
    (loss, acc, time) = main(tt_input_shape, tt_output_shape, tt_ranks, num_epochs)
    res = "| " + str(tt_input_shape) + " | " + str(tt_output_shape) + " | " + str(tt_ranks) + " | " + str(num_epochs)\
               + " | " + str(loss) + " | " + str(acc) + " | " + str(time) + " |\n"
    results.write(res)
    param = grid.readline()
