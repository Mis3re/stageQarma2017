#!/usr/bin/env python
#-*- coding: utf-8 -*-

from mnistwTTLayer_initUniform import main as mnistwTTLayerMain
import sys
import os


def main(num_part):
    folder_path = os.getcwd() + "/"
    grid = open(folder_path + "gridOfParameters/gridOfParametersForTTLayer_initUniform_part" + str(num_part) + ".txt", 'r')
    results = open(folder_path + "results/results_initUniform_part" + str(num_part) + ".txt", 'w')
    param = grid.readline()

    while not param == "":
        print("param : ", param)
        param = param.split(" ")
        num_epochs = 5
        tt_input_shape = param[1]
        tt_output_shape = param[2]
        tt_ranks = param[3]
        (loss, acc, time) = mnistwTTLayerMain(tt_input_shape, tt_output_shape, tt_ranks, num_epochs)
        res = "| " + str(tt_input_shape) + " | " + str(tt_output_shape) + " | " + str(tt_ranks) + " | " + str(num_epochs)\
                   + " | " + str(loss) + " | " + str(acc) + " | " + str(time) + " |\n"
        results.write(res)
        param = grid.readline()


if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['num_part'] = sys.argv[1]
    network = main(**kwargs)
