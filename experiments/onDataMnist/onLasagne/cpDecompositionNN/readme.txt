README OF cpDecompositionNN folder

- cplayer.py : Couche lasagne de décomposition CP écrit par Yasmina.

- utils.py : fonctions utilitaires.

- neuralNetworkWithCPLayer.py : Programme qui apprend un réseau de neurones avec un couche CP et calcul le taux d’erreur, la précision, le temps d’execution.

- parameters_generator.py : Génère la liste des paramètres pour l’expérience à réaliser.

- gridOfParameters.txt : Liste des paramètres.

- submit.sh : lance une commande OAR qui génère autant d’exécution de ‘neuralNetworkWithCPLayer.py’ qu’il y a de paramètres différents dans ‘gridOfParameters.txt’.

- totalResult.txt : Résultats obtenues (après raffinage) de l’expérience.

- exploit_graph_accuracyToNbParam.py : Génère le graphe des précisions par rapport aux nombres de paramètres à partir des résultats de l’experience.

- exploit_graph_accuracyByDecreasingOrder.py : Génère le graphe précisions classées par ordres décroissant à partir des résultats de l’experience.

- exploit_file.py : Extrait des résultats de l’experience les 25 meilleurs et pires résultats dans chaque catégories.