#!/usr/bin/env python
#-*- coding: utf-8 -*-


'''Reseau similaire au reseau de base, la premier couche Fully_connected est remplacee par une CPLayer.
Le programme prend 4 arguments : le nombre d epochs, vecteur des dimensions du tenseur representant l entree de la CPLayer (cp_input_shape), 
vecteur des dimensions du tenseur representant la sortie de la CPLayer (cp_output_shape) et le rang canonique (rank).

Exemples:
                        cp_imput_shape | cp_ouput_shape | rank | nb_epoch
python mnistwCPLayer    '28-28'          '25-32'          10   | 5
'''

from __future__ import print_function
from __future__ import absolute_import      # permet les imports relatifs (cf python3)

import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T
import lasagne
from lasagne.layers import batch_norm

import Utils
from cplayer import CPLayer


def main(cp_input_shape, cp_output_shape, rank, nb_epochs):
    def build_mlp(cp_input_shape, cp_output_shape, rank, input_var=None):
        """Construction du modele :
        MLP d'un couche CP, d'un couche cachee contenant 800 neurones suivies d'une couche de sortie contenant 10 neurones.
        """
        network = {}

        # Couche d'entree
        network['l_in'] = lasagne.layers.InputLayer(shape=(None, 1, 28, 28), input_var=input_var)

        # Couche CP
        network['l_hid1'] = CPLayer(network['l_in'], cp_input_shape, cp_output_shape, rank)
        print("Nombre de parametres de la couche CP :\t{!s}".format(network['l_hid1'].cores_arr.get_value().shape))
        network['l_bn1'] = batch_norm(network['l_hid1'])

        network['l_do'] = lasagne.layers.DropoutLayer(network['l_bn1'], p=0.25)

        # Couche fully-connected suivie d'une ReLU
        network['l_hid2'] = lasagne.layers.DenseLayer(network['l_do'], num_units=800)

        # Couche de sortie fully-connected suivie d'une softmax
        network['l_out'] = lasagne.layers.DenseLayer(network['l_hid2'],
                                                     num_units=10,
                                                     nonlinearity=lasagne.nonlinearities.softmax)
        return network

    X_train, y_train, X_val, y_val, X_test, y_test = Utils.load_dataset()

    # Preparation des variables Theano pour les inputs et targets
    input_var = T.tensor4('inputs')
    target_var = T.ivector('targets')

    # Mise en forme des arguments
    to_print_input_shape = cp_input_shape
    to_print_output_shape = cp_output_shape

    cp_input_shape = cp_input_shape.split('-')
    cp_output_shape = cp_output_shape.split('-')
    cp_input_shape = np.asarray(map(int,cp_input_shape))
    cp_output_shape = np.asarray(map(int,cp_output_shape))

    # Creation du modele
    # print("Building model and compiling functions...")
    network = build_mlp(cp_input_shape, cp_output_shape, rank, input_var)

    # Creation de la fonction loss pour la phase de training -> cross-entropy loss:
    prediction = lasagne.layers.get_output(network['l_out'])
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var).mean()

    # Creation de la fonction de mise a jour pour la phase de training -> Stochastic Gradient
    # Descent (SGD) with Nesterov momentum.
    params = lasagne.layers.get_all_params(network['l_out'], trainable=True)
    updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate=0.01, momentum=0.9)

    # Creation de la fonction loss  pour les phases de validation/test
    test_prediction = lasagne.layers.get_output(network['l_out'], deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var).mean()

    # Expression pour la precision de la classification :
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var), dtype=theano.config.floatX)

    # Compilation d une fonction performant une etape de training sur un mini-batch (en donnant
    # le dictionnaire des mises a jour) et retournant les training loss correspondant:
    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    # Compilation d une seconde fonction calculant la validation loss et la precision:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    # Boucle pour la phase de training.
    start_time = time.time()
    # Pour chaque epoch:
    for epoch in range(nb_epochs):
        # on itere sur toute la base de training : 
        train_err = 0
        train_batches = 0
        for batch in Utils.iterate_minibatches(X_train, y_train, 500, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1

        # et toute la base de validation:
        val_err = 0
        val_acc = 0
        val_batches = 0
        for batch in Utils.iterate_minibatches(X_val, y_val, 500, shuffle=False):
            inputs, targets = batch
            err, acc = val_fn(inputs, targets)
            val_err += err
            val_acc += acc
            val_batches += 1

    # Apres la phase de training, on calcule et affiche l erreur pour la phase de test:
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in Utils.iterate_minibatches(X_test, y_test, 500, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1

    end_time = time.time() - start_time

    result = [[], []]
    result[0].append(to_print_input_shape)
    result[0].append(to_print_output_shape)
    result[0].append(rank)
    result[0].append(nb_epochs)

    result[1].append(test_err / test_batches)              # erreur sur le jeux de test
    result[1].append(test_acc / test_batches * 100)        # precision en % (precision et erreur des valeurs opposees)
    result[1].append(end_time)                             # temps d'execution

    print(result)

    return result


if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [EPOCHS]" % sys.argv[0])
        print()
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['cp_input_shape'] = sys.argv[1]
            kwargs['cp_output_shape'] = sys.argv[2]
            kwargs['rank'] = int(sys.argv[3])
            kwargs['nb_epochs'] = int(sys.argv[4])
        network = main(**kwargs)
