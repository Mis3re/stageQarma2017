#!/usr/bin/env python
#-*- coding: utf-8 -*-

import numpy as np
import theano
import theano.tensor as T
import lasagne
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class KroneckerLayer(lasagne.layers.Layer):
    def __init__(self,
                 incoming,
                 bkSize,        # La taille des Bk va définir la taille des Ak
                 K,             # kroneckerRank (ie = nb de Bk = nb d'Ak = nb images en sortie)
                 nonlinearity=lasagne.nonlinearities.identity,
                 **kwargs):

        super(KroneckerLayer, self).__init__(incoming, **kwargs)
        # A supprimer si non utiliser
        self.nonlinearity = nonlinearity

        K = int(K)
        if isinstance(K, int) != True:
            raise ValueError("The rank should be an integer.")
        self.K = K

        bkSize = (int(bkSize[0]), int(bkSize[1]))
        if not (isinstance(bkSize, tuple) == True and len(bkSize) == 2
                                                  and isinstance(bkSize[0], int)
                                                  and isinstance(bkSize[1], int)):
            raise ValueError("Bk should be a tuple of integer of size two.")
        self.bkSize = bkSize


        local_B = _generate_B(bkSize, K)
        self.B = self.add_param(local_B, local_B.shape, name='B', trainable=True)


    def get_output_for(self, input, **kwargs):
        if input.ndim > 2:
            input = input.flatten(2)

        # On retourne K images qui correspondent aux (Ak * Bk) pour k de 1 à K.
        images = []

        # On définit la taille des Ak à partir de la taille mSize de l'image en input et de la taille des Bk.
        mSize = (self.input_shape[2], self.input_shape[3])
        akSize = (mSize[0] / self.bkSize[0], mSize[1] / self.bkSize[1])

        # On reshape l'image d'entrée (de taille np * mq) en nm * pq.
        # Remarque : n = akSize[0] ; m = akSize[1] ; p = bkSize[0] ; q = bkSize[1].

        # input = np.array(input)
        # print "type input : " + str(type(input))


    # TENTATIVE DE RESHAPE INTELLIGENT
    #     akSize = (3, 4)
    #     bkSize = (6, 2)
    #     input = np.ones((akSize[0] * bkSize[0], akSize[1] * bkSize[1]))
    #
    #     if not (bkSize[0] == akSize[1]):
    #         input_reshape = []
    #
    #         # Notre but est de faire un reshape qui perd le moins la structure
    #
    #
    #         #             p   ==   m
    #         if not (bkSize[0] == akSize[1]):
    #             input_reshape = []
    #             # Si on augmente le nombre de lignes et on diminu le nombre de colonnes.
    #             #        p   <   m
    #             if bkSize[0] < akSize[1]:
    #                 row = []
    #                 #                       p   *   q
    #                 new_column_size = bkSize[0] * bkSize[1]
    #                 # On découper les lignes en k colonnes.
    #                 #                    m   /   p
    #                 for k in range(akSize[1] / bkSize[0]):
    #                     # Pour chaque ligne.
    #                     #                    n   *   p
    #                     for j in range(akSize[0] * bkSize[0]):
    #                         # On récupère les valeurs qui correspondent à la k-ieme colonne.
    #                         for i in range(new_column_size * k, new_column_size * (k+1)):
    #                             row.append(input[j][i])
    #                         input_reshape.append(row)
    #                         row = []
    #             # Si on augmente le nombre de colonnes et on diminu le nombre de lignes.
    #             else:   # ie p > m
    #                 row = []
    #                 #                           n   *   m
    #                 new_number_of_lines = akSize[0] * akSize[1]
    #                 # Pour chaque nouvelles lignes.
    #                 for k in range(new_number_of_lines):
    #                     # On regarde les lignes correspondantes dans la matrices d'entrée.
    #                     j = k
    #                     #                n   *   p
    #                     while j < (akSize[0] * bkSize[0]):
    #                         # Et on récupère les valeurs de toutes les colonnes.
    #                         #                    m   *   q
    #                         for i in range(akSize[1] * bkSize[1]):
    #                             row.append(input[j][i])
    #                         j += new_number_of_lines
    #                     input_reshape.append(row)
    #                     row = []
    #
    #
    # input_reshape = np.array(input_reshape)
    # print "akSize : " + str(akSize)
    # print "bkSize : " + str(bkSize)
    # print "\ninput ="
    # for e in input:
    #     print e
    # print "\ninput_reshape ="
    # for e in input_reshape:
    #     print e


        # input_reshape = input
        input_reshape = T.reshape(input, (akSize[0] * akSize[1], self.bkSize[0] * self.bkSize[1]))

        # input_reshape = np.reshape(input, (akSize[0] * akSize[1], self.bkSize[0] * self.bkSize[1]))

        for k in range(self.K):
            # On vectorise Bk (de taille p * q) en un vecteur (de taille pq * 1)
            # Le reshape (vectorisation) est fait à la main pour garder la structure : on concatène chaque ligne.
            Bk = []
            for e in self.B.get_value()[k]:
                for f in e:
                    Bk.append(f)
            Bk = np.array(Bk)

            b_inverse = 1. / np.dot(Bk, Bk.transpose())
            dot_product = np.dot(input_reshape, Bk)
            Ak = dot_product * b_inverse

            # On rshape la matrice Ak
            Ak_reshape = []
            row = []
            for e in Ak:
                row.append(e)
                if len(row) >= akSize[1]:
                    Ak_reshape.append(row)
                    row = []

            # Reconstruction de la k_ieme image en effectuant Ak 'kronecker' Bk.
            image_K = np.kron(Ak_reshape, self.B.get_value()[k])
            images.append(image_K)

        images = np.array(images)
        toReturn = images[0]
        for image in images[1:]:
            toReturn += image

        toReturn = np.matrix(toReturn)
        return self.nonlinearity(toReturn)

    def get_output_shape_for(self, input_shape):
        return self.K, input_shape[2], input_shape[3]


# Génère K matrices de taille bkSize initialiser aléatoirement.
def _generate_B(bkSize, K):
    B = []
    for i in range(K):
        B.append(lasagne.random.get_rng().uniform(-1, 1, size=bkSize))
    return np.array(B)
