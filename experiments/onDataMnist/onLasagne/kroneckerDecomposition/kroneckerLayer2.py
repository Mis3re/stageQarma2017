#!/usr/bin/env python
#-*- coding: utf-8 -*-

import numpy as np
import theano
import theano.tensor as T
import lasagne
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class KroneckerLayer2(lasagne.layers.Layer):
    def __init__(self,
                 incoming,
                 bkSize,
                 kroneckerRank,
                 nonlinearity=lasagne.nonlinearities.rectify,
                 **kwargs):
        super(KroneckerLayer2, self).__init__(incoming, **kwargs)
        if not (isinstance(bkSize, tuple) and len(bkSize) == 2
                                          and isinstance(bkSize[0], int)
                                          and isinstance(bkSize[1], int)):
            raise ValueError("The size of bk should be an 2-tuple of integer.")
        kroneckerRank = int(kroneckerRank)
        if isinstance(kroneckerRank, int) != True:
            raise ValueError("The rank should be an integer.")

        self.bkSize = bkSize
        self.kroneckerRank = kroneckerRank
        self.nonlinearity = nonlinearity
        local_B = _generate_B(bkSize, self.kroneckerRank)
        self.B = self.add_param(local_B, local_B.shape, name='B', trainable=True)

        # Calcul de la taille des matrices Ak
        self.akSize = (kroneckerRank,
                       incoming.output_shape[2] / self.bkSize[0],
                       incoming.output_shape[3] / self.bkSize[1])
        print "akSize : " + str(self.akSize)

        self.A = np.zeros(self.akSize)

    def get_output_for(self, input, **kwargs):
        if input.ndim > 2:
            input = input.flatten(2)
        # images = []

        # Taille de l'image en entrée
        mSize = (self.input_shape[2], self.input_shape[3])
        print "mSize : " + str(mSize)

        # Reshape de l'image en entrée
        # TODO: faire un reshape qui garde la structure de l'image.
        input_reshape = T.reshape(input, (self.akSize[1] * self.akSize[2], self.bkSize[0] * self.bkSize[1]))
        print('input_reshape.shape = ', input_reshape.shape)

        input_reshape = np.array(input_reshape)
        print('input_reshape.shape after convert to np.array() = ', input_reshape.shape)
        print('type(input_reshape) = ', type(input_reshape))



        for k in range(self.kroneckerRank):
            # On vectorise Bk (de taille p * q) en un vecteur (de taille pq * 1)
            # Le reshape (vectorisation) est fait à la main pour garder la structure : on concatène chaque ligne.
            '''
            Bk = []
            for e in self.B.get_value()[k]:
                for f in e:
                    Bk.append(f)
            Bk = np.array(Bk)
            '''
            Bk = self.B.get_value()[k]
            print('Bk.shape = ', Bk.shape)
            Bk = Bk.reshape((1, self.bkSize[0] * self.bkSize[1]))
            print('Bk.shape = ', Bk.shape)


            b_inverse = 1. / np.dot(Bk, Bk.transpose())
            dot_product = np.dot(input_reshape, Bk)
            Ak = dot_product * b_inverse

            print('Ak.shape=', Ak.shape)
            Ak = Ak.reshape((self.akSize[1], self.akSize[2]))
            print('Ak.shape=', Ak.shape)
            '''
            # On rshape la matrice Ak
            Ak_reshape = []
            row = []
            for e in Ak:
                row.append(e)
                if len(row) >= self.akSize[2]:
                    Ak_reshape.append(row)
                    row = []
            '''

            # Reconstruction de la k_ieme image en effectuant Ak 'kronecker' Bk.
            self.A[k] = Ak# np.array(Ak_reshape)
            # image_K = np.kron(Ak_reshape, self.B.get_value()[k])
            # images.append(image_K)

        # toReturn = images[0]
        # for image in images:
        #   toReturn += image

        #self.A = np.array(self.A)

        #print
        #print self.nonlinearity(self.A)
        #   print

        return self.nonlinearity(self.A)


    def get_output_shape_for(self, input_shape):
        return self.akSize


# Génère K matrices de taille bkSize initialiser aléatoirement.
def _generate_B(bkSize, kroneckerRank):
    B = []
    for i in range(kroneckerRank):
        B.append(lasagne.random.get_rng().uniform(-1, 1, size=bkSize))
    return np.array(B)



