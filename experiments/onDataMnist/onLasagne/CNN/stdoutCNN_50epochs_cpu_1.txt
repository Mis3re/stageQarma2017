Loading data...
Building model and compiling functions...
Starting training...
Epoch 1 of 50 took 60.016s
  training loss:		0.839884
  validation loss:		0.226129
  validation accuracy:		93.62 %
Epoch 2 of 50 took 59.839s
  training loss:		0.201525
  validation loss:		0.152290
  validation accuracy:		95.74 %
Epoch 3 of 50 took 59.760s
  training loss:		0.140249
  validation loss:		0.122518
  validation accuracy:		96.63 %
Epoch 4 of 50 took 59.774s
  training loss:		0.108611
  validation loss:		0.101535
  validation accuracy:		97.12 %
Epoch 5 of 50 took 59.773s
  training loss:		0.088587
  validation loss:		0.092743
  validation accuracy:		97.20 %
Epoch 6 of 50 took 59.769s
  training loss:		0.072927
  validation loss:		0.083032
  validation accuracy:		97.60 %
Epoch 7 of 50 took 59.792s
  training loss:		0.061411
  validation loss:		0.080397
  validation accuracy:		97.63 %
Epoch 8 of 50 took 59.768s
  training loss:		0.052589
  validation loss:		0.073796
  validation accuracy:		97.84 %
Epoch 9 of 50 took 59.795s
  training loss:		0.045742
  validation loss:		0.073218
  validation accuracy:		97.88 %
Epoch 10 of 50 took 59.793s
  training loss:		0.039379
  validation loss:		0.073336
  validation accuracy:		97.98 %
Epoch 11 of 50 took 59.779s
  training loss:		0.034607
  validation loss:		0.067581
  validation accuracy:		98.17 %
Epoch 12 of 50 took 59.823s
  training loss:		0.029975
  validation loss:		0.074051
  validation accuracy:		97.77 %
Epoch 13 of 50 took 59.805s
  training loss:		0.026148
  validation loss:		0.067602
  validation accuracy:		98.18 %
Epoch 14 of 50 took 59.807s
  training loss:		0.023203
  validation loss:		0.071055
  validation accuracy:		97.96 %
Epoch 15 of 50 took 59.789s
  training loss:		0.020648
  validation loss:		0.065433
  validation accuracy:		98.21 %
Epoch 16 of 50 took 59.758s
  training loss:		0.018340
  validation loss:		0.064692
  validation accuracy:		98.14 %
Epoch 17 of 50 took 59.568s
  training loss:		0.015874
  validation loss:		0.064364
  validation accuracy:		98.27 %
Epoch 18 of 50 took 59.819s
  training loss:		0.014042
  validation loss:		0.066161
  validation accuracy:		98.24 %
Epoch 19 of 50 took 59.709s
  training loss:		0.012244
  validation loss:		0.065283
  validation accuracy:		98.30 %
Epoch 20 of 50 took 59.790s
  training loss:		0.010325
  validation loss:		0.065290
  validation accuracy:		98.21 %
Epoch 21 of 50 took 59.643s
  training loss:		0.009275
  validation loss:		0.064951
  validation accuracy:		98.27 %
Epoch 22 of 50 took 59.848s
  training loss:		0.008401
  validation loss:		0.064119
  validation accuracy:		98.34 %
Epoch 23 of 50 took 59.904s
  training loss:		0.007234
  validation loss:		0.065017
  validation accuracy:		98.33 %
Epoch 24 of 50 took 59.684s
  training loss:		0.006302
  validation loss:		0.067397
  validation accuracy:		98.31 %
Epoch 25 of 50 took 59.904s
  training loss:		0.005647
  validation loss:		0.064663
  validation accuracy:		98.32 %
Epoch 26 of 50 took 59.766s
  training loss:		0.004850
  validation loss:		0.064871
  validation accuracy:		98.38 %
Epoch 27 of 50 took 59.819s
  training loss:		0.004733
  validation loss:		0.066939
  validation accuracy:		98.30 %
Epoch 28 of 50 took 59.575s
  training loss:		0.004077
  validation loss:		0.070290
  validation accuracy:		98.28 %
Epoch 29 of 50 took 59.823s
  training loss:		0.003534
  validation loss:		0.064864
  validation accuracy:		98.37 %
Epoch 30 of 50 took 59.642s
  training loss:		0.003231
  validation loss:		0.070355
  validation accuracy:		98.36 %
Epoch 31 of 50 took 59.841s
  training loss:		0.002950
  validation loss:		0.066785
  validation accuracy:		98.40 %
Epoch 32 of 50 took 59.743s
  training loss:		0.002636
  validation loss:		0.067487
  validation accuracy:		98.40 %
Epoch 33 of 50 took 59.896s
  training loss:		0.002428
  validation loss:		0.066397
  validation accuracy:		98.41 %
Epoch 34 of 50 took 59.626s
  training loss:		0.002393
  validation loss:		0.069014
  validation accuracy:		98.34 %
Epoch 35 of 50 took 59.794s
  training loss:		0.002217
  validation loss:		0.069081
  validation accuracy:		98.44 %
Epoch 36 of 50 took 59.692s
  training loss:		0.001970
  validation loss:		0.068303
  validation accuracy:		98.41 %
Epoch 37 of 50 took 59.852s
  training loss:		0.002052
  validation loss:		0.068472
  validation accuracy:		98.44 %
Epoch 38 of 50 took 60.964s
  training loss:		0.001774
  validation loss:		0.070307
  validation accuracy:		98.35 %
Epoch 39 of 50 took 60.209s
  training loss:		0.001672
  validation loss:		0.068589
  validation accuracy:		98.47 %
Epoch 40 of 50 took 59.594s
  training loss:		0.001550
  validation loss:		0.070203
  validation accuracy:		98.40 %
Epoch 41 of 50 took 59.911s
  training loss:		0.001472
  validation loss:		0.070840
  validation accuracy:		98.42 %
Epoch 42 of 50 took 59.655s
  training loss:		0.001433
  validation loss:		0.071022
  validation accuracy:		98.42 %
Epoch 43 of 50 took 59.776s
  training loss:		0.001441
  validation loss:		0.070823
  validation accuracy:		98.40 %
Epoch 44 of 50 took 59.579s
  training loss:		0.001286
  validation loss:		0.071577
  validation accuracy:		98.40 %
Epoch 45 of 50 took 59.879s
  training loss:		0.001232
  validation loss:		0.070945
  validation accuracy:		98.43 %
Epoch 46 of 50 took 59.820s
  training loss:		0.001148
  validation loss:		0.072311
  validation accuracy:		98.38 %
Epoch 47 of 50 took 59.589s
  training loss:		0.001119
  validation loss:		0.071843
  validation accuracy:		98.39 %
Epoch 48 of 50 took 59.878s
  training loss:		0.001089
  validation loss:		0.072029
  validation accuracy:		98.46 %
Epoch 49 of 50 took 59.504s
  training loss:		0.001043
  validation loss:		0.071798
  validation accuracy:		98.40 %
Epoch 50 of 50 took 59.698s
  training loss:		0.000996
  validation loss:		0.074140
  validation accuracy:		98.40 %
Final results:
  test loss:			0.062722
  test accuracy:		98.35 %
Total time 2993.388s
Saving parameters ...
Done ! 
