Vieux dossier de test de CNN.

-> test de comparaison en performance et en rapidité entre un apprentissage réalisé sur CPU et un apprentissage réalisé sur GPU (3x).
En performance les scores sont quasiment identiques. En temps on passe d’environ 3000s pour CPU à environ 1700s (rmq: une expérience à 2950s).