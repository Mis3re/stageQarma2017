Loading data...
Building model and compiling functions...
Starting training...
Epoch 1 of 50 took 34.288s
  training loss:		0.566849
  validation loss:		0.218500
  validation accuracy:		94.05 %
Epoch 2 of 50 took 34.277s
  training loss:		0.193506
  validation loss:		0.152945
  validation accuracy:		95.71 %
Epoch 3 of 50 took 34.184s
  training loss:		0.140091
  validation loss:		0.117930
  validation accuracy:		96.77 %
Epoch 4 of 50 took 34.203s
  training loss:		0.109885
  validation loss:		0.108132
  validation accuracy:		96.90 %
Epoch 5 of 50 took 34.200s
  training loss:		0.089779
  validation loss:		0.096106
  validation accuracy:		97.38 %
Epoch 6 of 50 took 34.213s
  training loss:		0.076316
  validation loss:		0.088939
  validation accuracy:		97.55 %
Epoch 7 of 50 took 34.202s
  training loss:		0.064758
  validation loss:		0.082118
  validation accuracy:		97.74 %
Epoch 8 of 50 took 34.182s
  training loss:		0.055963
  validation loss:		0.076817
  validation accuracy:		97.89 %
Epoch 9 of 50 took 34.187s
  training loss:		0.048780
  validation loss:		0.075450
  validation accuracy:		97.94 %
Epoch 10 of 50 took 34.184s
  training loss:		0.041854
  validation loss:		0.071135
  validation accuracy:		97.93 %
Epoch 11 of 50 took 34.189s
  training loss:		0.036987
  validation loss:		0.070525
  validation accuracy:		97.91 %
Epoch 12 of 50 took 34.171s
  training loss:		0.032583
  validation loss:		0.067842
  validation accuracy:		98.06 %
Epoch 13 of 50 took 34.198s
  training loss:		0.028853
  validation loss:		0.066895
  validation accuracy:		98.06 %
Epoch 14 of 50 took 34.179s
  training loss:		0.024267
  validation loss:		0.073618
  validation accuracy:		97.88 %
Epoch 15 of 50 took 34.170s
  training loss:		0.022610
  validation loss:		0.068861
  validation accuracy:		98.12 %
Epoch 16 of 50 took 34.217s
  training loss:		0.019569
  validation loss:		0.067717
  validation accuracy:		98.20 %
Epoch 17 of 50 took 34.248s
  training loss:		0.017641
  validation loss:		0.072753
  validation accuracy:		98.03 %
Epoch 18 of 50 took 34.208s
  training loss:		0.015507
  validation loss:		0.065729
  validation accuracy:		98.16 %
Epoch 19 of 50 took 34.173s
  training loss:		0.013252
  validation loss:		0.066484
  validation accuracy:		98.12 %
Epoch 20 of 50 took 34.181s
  training loss:		0.011554
  validation loss:		0.064744
  validation accuracy:		98.25 %
Epoch 21 of 50 took 34.222s
  training loss:		0.010297
  validation loss:		0.064240
  validation accuracy:		98.20 %
Epoch 22 of 50 took 34.160s
  training loss:		0.009176
  validation loss:		0.065904
  validation accuracy:		98.22 %
Epoch 23 of 50 took 34.162s
  training loss:		0.007951
  validation loss:		0.068522
  validation accuracy:		98.12 %
Epoch 24 of 50 took 34.177s
  training loss:		0.007257
  validation loss:		0.066648
  validation accuracy:		98.18 %
Epoch 25 of 50 took 34.118s
  training loss:		0.006461
  validation loss:		0.066852
  validation accuracy:		98.18 %
Epoch 26 of 50 took 34.188s
  training loss:		0.005746
  validation loss:		0.066873
  validation accuracy:		98.30 %
Epoch 27 of 50 took 34.163s
  training loss:		0.005083
  validation loss:		0.067624
  validation accuracy:		98.32 %
Epoch 28 of 50 took 34.146s
  training loss:		0.004725
  validation loss:		0.070281
  validation accuracy:		98.23 %
Epoch 29 of 50 took 34.134s
  training loss:		0.004441
  validation loss:		0.067837
  validation accuracy:		98.26 %
Epoch 30 of 50 took 34.125s
  training loss:		0.004182
  validation loss:		0.067691
  validation accuracy:		98.26 %
Epoch 31 of 50 took 34.104s
  training loss:		0.003500
  validation loss:		0.067020
  validation accuracy:		98.30 %
Epoch 32 of 50 took 34.147s
  training loss:		0.002974
  validation loss:		0.068924
  validation accuracy:		98.25 %
Epoch 33 of 50 took 34.108s
  training loss:		0.002751
  validation loss:		0.070077
  validation accuracy:		98.28 %
Epoch 34 of 50 took 34.148s
  training loss:		0.002672
  validation loss:		0.070250
  validation accuracy:		98.30 %
Epoch 35 of 50 took 34.115s
  training loss:		0.002385
  validation loss:		0.069582
  validation accuracy:		98.30 %
Epoch 36 of 50 took 34.098s
  training loss:		0.002141
  validation loss:		0.069936
  validation accuracy:		98.32 %
Epoch 37 of 50 took 34.095s
  training loss:		0.002063
  validation loss:		0.069343
  validation accuracy:		98.31 %
Epoch 38 of 50 took 34.420s
  training loss:		0.001937
  validation loss:		0.071218
  validation accuracy:		98.33 %
Epoch 39 of 50 took 33.992s
  training loss:		0.001826
  validation loss:		0.070900
  validation accuracy:		98.28 %
Epoch 40 of 50 took 34.049s
  training loss:		0.001714
  validation loss:		0.071192
  validation accuracy:		98.32 %
Epoch 41 of 50 took 34.043s
  training loss:		0.001670
  validation loss:		0.070833
  validation accuracy:		98.32 %
Epoch 42 of 50 took 34.088s
  training loss:		0.001549
  validation loss:		0.072453
  validation accuracy:		98.30 %
Epoch 43 of 50 took 34.160s
  training loss:		0.001539
  validation loss:		0.071195
  validation accuracy:		98.35 %
Epoch 44 of 50 took 34.062s
  training loss:		0.001383
  validation loss:		0.073089
  validation accuracy:		98.31 %
Epoch 45 of 50 took 33.803s
  training loss:		0.001353
  validation loss:		0.072644
  validation accuracy:		98.33 %
Epoch 46 of 50 took 33.960s
  training loss:		0.001236
  validation loss:		0.072126
  validation accuracy:		98.34 %
Epoch 47 of 50 took 34.113s
  training loss:		0.001182
  validation loss:		0.074240
  validation accuracy:		98.33 %
Epoch 48 of 50 took 34.113s
  training loss:		0.001147
  validation loss:		0.073964
  validation accuracy:		98.32 %
Epoch 49 of 50 took 34.118s
  training loss:		0.001106
  validation loss:		0.073317
  validation accuracy:		98.31 %
Epoch 50 of 50 took 34.123s
  training loss:		0.001048
  validation loss:		0.073185
  validation accuracy:		98.37 %
Final results:
  test loss:			0.065257
  test accuracy:		98.36 %
Total time 1709.546s
Saving parameters ...
Done ! 
