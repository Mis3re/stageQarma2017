Loading data...
Building model and compiling functions...
Starting training...
Epoch 1 of 50 took 58.886s
  training loss:		0.658490
  validation loss:		0.215689
  validation accuracy:		94.15 %
Epoch 2 of 50 took 58.854s
  training loss:		0.195090
  validation loss:		0.153079
  validation accuracy:		95.96 %
Epoch 3 of 50 took 58.863s
  training loss:		0.139064
  validation loss:		0.117030
  validation accuracy:		96.93 %
Epoch 4 of 50 took 58.869s
  training loss:		0.107859
  validation loss:		0.102088
  validation accuracy:		97.14 %
Epoch 5 of 50 took 58.866s
  training loss:		0.088436
  validation loss:		0.091218
  validation accuracy:		97.46 %
Epoch 6 of 50 took 58.868s
  training loss:		0.073391
  validation loss:		0.088490
  validation accuracy:		97.57 %
Epoch 7 of 50 took 58.877s
  training loss:		0.063088
  validation loss:		0.077331
  validation accuracy:		97.76 %
Epoch 8 of 50 took 58.869s
  training loss:		0.053064
  validation loss:		0.079578
  validation accuracy:		97.72 %
Epoch 9 of 50 took 58.892s
  training loss:		0.047355
  validation loss:		0.079211
  validation accuracy:		97.77 %
Epoch 10 of 50 took 58.905s
  training loss:		0.040783
  validation loss:		0.068767
  validation accuracy:		98.06 %
Epoch 11 of 50 took 58.914s
  training loss:		0.035312
  validation loss:		0.071456
  validation accuracy:		97.99 %
Epoch 12 of 50 took 58.891s
  training loss:		0.031886
  validation loss:		0.066855
  validation accuracy:		98.06 %
Epoch 13 of 50 took 58.912s
  training loss:		0.027717
  validation loss:		0.065621
  validation accuracy:		98.05 %
Epoch 14 of 50 took 58.905s
  training loss:		0.024308
  validation loss:		0.068711
  validation accuracy:		98.05 %
Epoch 15 of 50 took 58.921s
  training loss:		0.021927
  validation loss:		0.064318
  validation accuracy:		98.24 %
Epoch 16 of 50 took 58.904s
  training loss:		0.018865
  validation loss:		0.062325
  validation accuracy:		98.18 %
Epoch 17 of 50 took 58.887s
  training loss:		0.017264
  validation loss:		0.064163
  validation accuracy:		98.18 %
Epoch 18 of 50 took 58.895s
  training loss:		0.014405
  validation loss:		0.064753
  validation accuracy:		98.28 %
Epoch 19 of 50 took 58.897s
  training loss:		0.012857
  validation loss:		0.064758
  validation accuracy:		98.27 %
Epoch 20 of 50 took 58.861s
  training loss:		0.011391
  validation loss:		0.062799
  validation accuracy:		98.31 %
Epoch 21 of 50 took 58.897s
  training loss:		0.010262
  validation loss:		0.063694
  validation accuracy:		98.27 %
Epoch 22 of 50 took 58.914s
  training loss:		0.008998
  validation loss:		0.066167
  validation accuracy:		98.24 %
Epoch 23 of 50 took 58.871s
  training loss:		0.008035
  validation loss:		0.062973
  validation accuracy:		98.35 %
Epoch 24 of 50 took 58.852s
  training loss:		0.006605
  validation loss:		0.063504
  validation accuracy:		98.38 %
Epoch 25 of 50 took 58.828s
  training loss:		0.005888
  validation loss:		0.063403
  validation accuracy:		98.26 %
Epoch 26 of 50 took 58.819s
  training loss:		0.005299
  validation loss:		0.068912
  validation accuracy:		98.30 %
Epoch 27 of 50 took 58.826s
  training loss:		0.004765
  validation loss:		0.065528
  validation accuracy:		98.33 %
Epoch 28 of 50 took 58.858s
  training loss:		0.004310
  validation loss:		0.063133
  validation accuracy:		98.41 %
Epoch 29 of 50 took 58.837s
  training loss:		0.003915
  validation loss:		0.065927
  validation accuracy:		98.29 %
Epoch 30 of 50 took 58.807s
  training loss:		0.003690
  validation loss:		0.067055
  validation accuracy:		98.43 %
Epoch 31 of 50 took 58.824s
  training loss:		0.003190
  validation loss:		0.068396
  validation accuracy:		98.34 %
Epoch 32 of 50 took 58.825s
  training loss:		0.003049
  validation loss:		0.066055
  validation accuracy:		98.41 %
Epoch 33 of 50 took 58.808s
  training loss:		0.002716
  validation loss:		0.066463
  validation accuracy:		98.39 %
Epoch 34 of 50 took 58.820s
  training loss:		0.002512
  validation loss:		0.067518
  validation accuracy:		98.34 %
Epoch 35 of 50 took 58.803s
  training loss:		0.002401
  validation loss:		0.067006
  validation accuracy:		98.43 %
Epoch 36 of 50 took 58.830s
  training loss:		0.002223
  validation loss:		0.068058
  validation accuracy:		98.36 %
Epoch 37 of 50 took 58.850s
  training loss:		0.001947
  validation loss:		0.069053
  validation accuracy:		98.42 %
Epoch 38 of 50 took 59.358s
  training loss:		0.001850
  validation loss:		0.068410
  validation accuracy:		98.40 %
Epoch 39 of 50 took 59.354s
  training loss:		0.001716
  validation loss:		0.067587
  validation accuracy:		98.47 %
Epoch 40 of 50 took 59.341s
  training loss:		0.001639
  validation loss:		0.070928
  validation accuracy:		98.35 %
Epoch 41 of 50 took 59.302s
  training loss:		0.001556
  validation loss:		0.068272
  validation accuracy:		98.46 %
Epoch 42 of 50 took 59.329s
  training loss:		0.001519
  validation loss:		0.069130
  validation accuracy:		98.40 %
Epoch 43 of 50 took 59.339s
  training loss:		0.001395
  validation loss:		0.068520
  validation accuracy:		98.43 %
Epoch 44 of 50 took 59.306s
  training loss:		0.001303
  validation loss:		0.070230
  validation accuracy:		98.45 %
Epoch 45 of 50 took 59.310s
  training loss:		0.001247
  validation loss:		0.070267
  validation accuracy:		98.45 %
Epoch 46 of 50 took 59.323s
  training loss:		0.001197
  validation loss:		0.070712
  validation accuracy:		98.47 %
Epoch 47 of 50 took 59.346s
  training loss:		0.001163
  validation loss:		0.071740
  validation accuracy:		98.38 %
Epoch 48 of 50 took 59.340s
  training loss:		0.001114
  validation loss:		0.070713
  validation accuracy:		98.43 %
Epoch 49 of 50 took 59.296s
  training loss:		0.001056
  validation loss:		0.071267
  validation accuracy:		98.41 %
Epoch 50 of 50 took 59.336s
  training loss:		0.001020
  validation loss:		0.071304
  validation accuracy:		98.48 %
Final results:
  test loss:			0.059854
  test accuracy:		98.46 %
Total time 2952.864s
Saving parameters ...
Done ! 
