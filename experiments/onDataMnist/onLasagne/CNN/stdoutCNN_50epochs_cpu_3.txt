Loading data...
Building model and compiling functions...
Starting training...
Epoch 1 of 50 took 66.773s
  training loss:		0.944482
  validation loss:		0.237125
  validation accuracy:		93.25 %
Epoch 2 of 50 took 66.685s
  training loss:		0.209882
  validation loss:		0.158743
  validation accuracy:		95.67 %
Epoch 3 of 50 took 66.696s
  training loss:		0.146015
  validation loss:		0.125564
  validation accuracy:		96.60 %
Epoch 4 of 50 took 66.706s
  training loss:		0.112900
  validation loss:		0.103983
  validation accuracy:		97.11 %
Epoch 5 of 50 took 66.701s
  training loss:		0.091260
  validation loss:		0.092897
  validation accuracy:		97.42 %
Epoch 6 of 50 took 66.704s
  training loss:		0.076404
  validation loss:		0.088816
  validation accuracy:		97.51 %
Epoch 7 of 50 took 66.704s
  training loss:		0.065465
  validation loss:		0.082374
  validation accuracy:		97.61 %
Epoch 8 of 50 took 66.703s
  training loss:		0.057015
  validation loss:		0.079231
  validation accuracy:		97.83 %
Epoch 9 of 50 took 66.701s
  training loss:		0.049676
  validation loss:		0.073136
  validation accuracy:		97.75 %
Epoch 10 of 50 took 66.716s
  training loss:		0.043333
  validation loss:		0.081458
  validation accuracy:		97.66 %
Epoch 11 of 50 took 66.721s
  training loss:		0.038280
  validation loss:		0.075693
  validation accuracy:		97.93 %
Epoch 12 of 50 took 66.731s
  training loss:		0.033831
  validation loss:		0.073945
  validation accuracy:		97.83 %
Epoch 13 of 50 took 66.712s
  training loss:		0.029714
  validation loss:		0.069580
  validation accuracy:		98.02 %
Epoch 14 of 50 took 66.721s
  training loss:		0.026141
  validation loss:		0.066454
  validation accuracy:		98.08 %
Epoch 15 of 50 took 66.707s
  training loss:		0.023133
  validation loss:		0.065556
  validation accuracy:		98.00 %
Epoch 16 of 50 took 66.717s
  training loss:		0.020717
  validation loss:		0.067769
  validation accuracy:		98.04 %
Epoch 17 of 50 took 66.712s
  training loss:		0.018209
  validation loss:		0.067630
  validation accuracy:		98.07 %
Epoch 18 of 50 took 66.702s
  training loss:		0.016556
  validation loss:		0.064210
  validation accuracy:		98.22 %
Epoch 19 of 50 took 66.693s
  training loss:		0.014275
  validation loss:		0.067929
  validation accuracy:		98.13 %
Epoch 20 of 50 took 66.711s
  training loss:		0.012403
  validation loss:		0.069508
  validation accuracy:		98.11 %
Epoch 21 of 50 took 66.711s
  training loss:		0.010567
  validation loss:		0.067190
  validation accuracy:		98.18 %
Epoch 22 of 50 took 66.713s
  training loss:		0.009788
  validation loss:		0.068613
  validation accuracy:		98.16 %
Epoch 23 of 50 took 66.708s
  training loss:		0.008195
  validation loss:		0.065583
  validation accuracy:		98.33 %
Epoch 24 of 50 took 66.700s
  training loss:		0.007294
  validation loss:		0.070602
  validation accuracy:		98.18 %
Epoch 25 of 50 took 66.697s
  training loss:		0.006614
  validation loss:		0.066842
  validation accuracy:		98.33 %
Epoch 26 of 50 took 66.697s
  training loss:		0.006150
  validation loss:		0.067889
  validation accuracy:		98.32 %
Epoch 27 of 50 took 66.681s
  training loss:		0.005334
  validation loss:		0.069338
  validation accuracy:		98.20 %
Epoch 28 of 50 took 66.681s
  training loss:		0.004997
  validation loss:		0.067732
  validation accuracy:		98.18 %
Epoch 29 of 50 took 66.691s
  training loss:		0.004163
  validation loss:		0.066687
  validation accuracy:		98.29 %
Epoch 30 of 50 took 66.706s
  training loss:		0.004245
  validation loss:		0.068304
  validation accuracy:		98.30 %
Epoch 31 of 50 took 66.692s
  training loss:		0.003761
  validation loss:		0.068341
  validation accuracy:		98.26 %
Epoch 32 of 50 took 66.693s
  training loss:		0.003374
  validation loss:		0.068100
  validation accuracy:		98.34 %
Epoch 33 of 50 took 66.692s
  training loss:		0.002952
  validation loss:		0.069237
  validation accuracy:		98.27 %
Epoch 34 of 50 took 66.687s
  training loss:		0.002720
  validation loss:		0.069460
  validation accuracy:		98.37 %
Epoch 35 of 50 took 66.681s
  training loss:		0.002530
  validation loss:		0.070168
  validation accuracy:		98.31 %
Epoch 36 of 50 took 66.693s
  training loss:		0.002402
  validation loss:		0.070094
  validation accuracy:		98.36 %
Epoch 37 of 50 took 66.674s
  training loss:		0.002261
  validation loss:		0.069474
  validation accuracy:		98.35 %
Epoch 38 of 50 took 66.700s
  training loss:		0.002348
  validation loss:		0.071324
  validation accuracy:		98.26 %
Epoch 39 of 50 took 66.684s
  training loss:		0.001921
  validation loss:		0.071096
  validation accuracy:		98.34 %
Epoch 40 of 50 took 66.688s
  training loss:		0.001798
  validation loss:		0.070816
  validation accuracy:		98.38 %
Epoch 41 of 50 took 66.677s
  training loss:		0.001702
  validation loss:		0.072135
  validation accuracy:		98.37 %
Epoch 42 of 50 took 66.686s
  training loss:		0.001570
  validation loss:		0.071836
  validation accuracy:		98.41 %
Epoch 43 of 50 took 66.692s
  training loss:		0.001496
  validation loss:		0.073009
  validation accuracy:		98.38 %
Epoch 44 of 50 took 66.687s
  training loss:		0.001408
  validation loss:		0.072477
  validation accuracy:		98.37 %
Epoch 45 of 50 took 66.671s
  training loss:		0.001419
  validation loss:		0.072316
  validation accuracy:		98.37 %
Epoch 46 of 50 took 66.682s
  training loss:		0.001293
  validation loss:		0.073796
  validation accuracy:		98.35 %
Epoch 47 of 50 took 66.679s
  training loss:		0.001251
  validation loss:		0.073545
  validation accuracy:		98.37 %
Epoch 48 of 50 took 66.675s
  training loss:		0.001185
  validation loss:		0.073245
  validation accuracy:		98.38 %
Epoch 49 of 50 took 66.680s
  training loss:		0.001173
  validation loss:		0.074399
  validation accuracy:		98.42 %
Epoch 50 of 50 took 66.675s
  training loss:		0.001157
  validation loss:		0.074179
  validation accuracy:		98.33 %
Final results:
  test loss:			0.062419
  test accuracy:		98.40 %
Total time 3339.019s
Saving parameters ...
Done ! 
