#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import os
from neuralNetwork_CPLayer import main as CPMain
import numpy as np

def main(numPart, nb_neurons):
    folder_path = os.getcwd() + "/"

    grid = open(folder_path + "gridOfParams_Foursquare_CPLayer_" + str(nb_neurons) + "neurons_part" + str(numPart) + ".txt", 'r')
    results = open(folder_path + "results" + str(nb_neurons) + "neurons_part" + str(numPart) + ".txt", 'w')
    param = grid.readline().split(" ")

    while not param == [""]:
        #try:
        print(">> NEW EXPE <<")
        print("param = ", param)
        # 0 5-15 17-5 5-5 5-8 0 0.2 Adam
        # codeArchi, shapeA1, shapeB1, shapeA2, shapeB2, rank, dropOutValue, num_part, optimizer)
        codeArchi = int(param[0])
        shapeA = str(param[1])
        shapeB = str(param[2])
        rank = int(param[3])
        dropOutValue = float(param[4])
        optimizer = str(param[5])
        if str(param[6])[-1] == "\n":
            output_dim = str(param[6])[:-1]
        else:
            output_dim = str(param[6])

        print("codeArchi : " + str(codeArchi))
        print("shapeA : " + str(shapeA))
        print("shapeB : " + str(shapeB))
        print("rank : " + str(rank))
        print("dropOutValue : " + str(dropOutValue))
        print("optimizer : " + str(optimizer))
        print("output_dim : " + str(output_dim))


        # Apprentissage du réseau avec cross validation k-fold, k = 10
        lossArray = []
        rmseArray = []
        timeArray = []
        nb_paramsArray = []

        for num_part in range(1, 11):
            print(">>>> CROSS-VALIDATION : " + str(num_part) + "/10 <<<<")

            res = CPMain(codeArchi, shapeA, shapeB, rank, dropOutValue, num_part, optimizer, int(output_dim))
            print(res)
            lossArray.append(res[0])
            rmseArray.append(res[1])
            timeArray.append(res[2])
            nb_paramsArray.append(res[3])

        loss = np.mean(lossArray)
        rmse = np.mean(rmseArray)
        time = np.mean(timeArray)
        nb_params = np.mean(nb_paramsArray)
        res = "| " + str(codeArchi) + " | " + str(shapeA) + " | " + str(shapeB) + " | " + str(rank) +\
              " | " +str(dropOutValue) + " | " + str(optimizer) + " | " + str(loss)[0:9] +\
              " | " + str(rmse)[0:9] + " | " + str(time)[0:9] + " | " + str(nb_params)[0:9] + " |\n"
        results.write(res)

        # except Exception,e:
        #     print("\n")
        #     print("/!\\ ATTENTION:")
        #     print("L'experience suivante a plante:")
        #     print(param)
        #     print("Message d'erreur : ")
        #     print(str(e))
        #     print("\n")
        param = grid.readline().split(" ")

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['numPart'] = str(sys.argv[1])

        kwargs['nb_neurons'] = str(sys.argv[2])
    main(**kwargs)