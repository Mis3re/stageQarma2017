echo "Lancement des expes avec 1000 neurones sur les grilles de parametres part 0 a 9..."
for i in `seq 0 9`;
do
	oarsub -p "gpu is null" -l walltime=150 "python launcherExperimentsOnGridPart.py $i 1000"
done


echo "Lancement des expes avec 500 neurones sur les grilles de parametres part 0 a 9..."
for i in `seq 0 9`;
do
	oarsub -p "gpu is null" -l walltime=150 "python launcherExperimentsOnGridPart.py $i 500"
done
echo "Done!"
