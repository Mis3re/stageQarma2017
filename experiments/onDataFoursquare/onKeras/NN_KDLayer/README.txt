A mettre en relation avec les expés CP-décomposition et TT-décompositon sur CCDS.

- - - - - - - - - - - - - - - - - - - - - - - - 
By Stephane Ayaches

En rapide : ça marche bien !

Plus en détail : 

Une couche dense de n neurones dont l'entrée à m neurones apprend une matrice de poids W de taille (n x m), telle que Y = X . W

En supposant une structure potentiellement "en blocs" sur les neurones d'entrées et de sorties, on décompose W via le produit suivant :

W = ∑_k ( A_input^k ⊗ B_input^k ⊗ A_output^k ⊗ B_output^k )

avec dim(A_input ⊗ B_input) = n et dim(A_output ⊗ B_output) = m  où dim() retourne le nombre d'éléments. Si les shapes correspondant sont bien choisis alors W sera de taille (n x m), sinon le nombre de paramètres est le même mais W nécessite un reshape final pour être reformer en (n x m).

Exemple : 
- A_input_shape = (2, 2) ; B_input_shape = (14, 14) ; A_output_shape = (4, 4) ; B_output_shape = (7, 7) => pas de reshape nécessaire
- A_input_shape = (2, 2) ; B_input_shape = (14, 14) ; A_output_shape = (5, 5) ; B_output_shape = (8, 4) => reshape nécessaire

Dans les deux cas, ça marche... Y a surement plusieurs façons de faire, celle là à l'avantage de marcher ;)

En plus du choix des shapes, le nombre de paramètres dépend du rank (le k de la somme). Au final, le nombre de paramètres peut être très petit pour des accuracy sympa. 

J'ai constaté rapidement : 
- qu'on atteint facile une accuracy supérieure à 0.97 avec très peu de paramètres
- plus le rank est élevé, plus l'accuracy augmente. A voir jusqu'à quand..
- plus le rank est élevé, plus ça apprend rapidement (moins d'epoch nécessaires)

Le code est en pièce jointe. Ca marche avec Theano car Tensorflow n'implémente pas la fonction kron (pour le produit de kronecker)..

Un exemple avec une couche de 784 neurones, A_in_shape = (4,4), B_in_shape = (7,7), A_out_shape = (4,4), B_out_shape = (7,7), rank = 1
	=> nb_param = 130
	=> accuracy = 0.9756 (après 20 epochs)

Reste à ajouter le biais et essayer d'autres manières de décomposer..

A+