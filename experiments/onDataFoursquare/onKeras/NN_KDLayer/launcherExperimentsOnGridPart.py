#!/usr/bin/env python
#-*- coding: utf-8 -*-

'''
Lit la grille de paramètres dont le numéros est en paramètre et exécute pour chaque ligne (ie chaque jeu de paramètres)
10 expériences pour chacun des 10 folds des données puis réalise la moyenne des résultats.

Ecrit dans un fichier de résultats une ligne pour chaque jeu de paramètre avec les détails du jeu en question suivit des
scores, du temps d'exécution et du nombre de paramètres de l'ensemble du réseau.
'''

import sys
import os
from neuralNetwork_KDLayer import main as KDMain
import numpy as np

def main(numPart, nb_neurons):
    folder_path = os.getcwd() + "/"

    grid = open(folder_path + "gridOfParams/gridOfParams_Foursquare_KDLayer_" + str(nb_neurons) + "neurons_part" + str(numPart) + ".txt", 'r')
    results = open(folder_path + "results/results" + str(nb_neurons) + "neurons_part" + numPart + ".txt", 'w')
    param = grid.readline().split(" ")

    while not param == [""]
        print(">> NEW EXPE <<")
        ### 0 5-15 17-5 5-5 5-8 0 0.2 Adam
        ### codeArchi, shapeA1, shapeB1, shapeA2, shapeB2, rank, dropOutValue, num_part, optimizer)
        codeArchi = int(param[0])
        shapeA1 = str(param[1])
        shapeB1 = str(param[2])
        shapeA2 = str(param[3])
        shapeB2 = str(param[4])
        rank = int(param[5])
        dropOutValue = float(param[6])
        optimizer = str(param[7])
        if str(param[8])[-1] == "\n":
            output_dim = str(param[8])[:-1]
        else:
            output_dim = str(param[8])

        print("codeArchi : " + str(codeArchi))
        print("shapeA1 : " + str(shapeA1))
        print("shapeB1 : " + str(shapeB1))
        print("shapeA2 : " + str(shapeA2))
        print("shapeB2 : " + str(shapeB2))
        print("rank : " + str(rank))
        print("dropOutValue : " + str(dropOutValue))
        print("optimizer : " + str(optimizer))
        print("output_dim : " + str(output_dim))

        lossArray = []
        rmseArray = []
        timeArray = []
        nb_paramsArray = []

        # Apprentissage du réseau avec cross validation k-fold, k = 10
        for num_part in range(1, 11):
            print(">>>> CROSS-VALIDATION : " + str(num_part) + "/10 <<<<")
            res = KDMain(codeArchi, shapeA1, shapeB1, shapeA2, shapeB2, rank, dropOutValue, num_part, optimizer, output_dim)
            print(res)
            lossArray.append(res[0])
            rmseArray.append(res[1])
            timeArray.append(res[2])
            nb_paramsArray.append(res[3])

        loss = np.mean(lossArray)
        rmse = np.mean(rmseArray)
        time = np.mean(timeArray)
        nb_params = np.mean(nb_paramsArray)
        res = "| " + str(codeArchi) + " | " + str(shapeA1) + " | " + str(shapeB1) + " | " + str(shapeA2) +\
              " | " + str(shapeB2) + " | " + str(rank) + " | " + str(dropOutValue) + " | " + str(optimizer) +\
              " | " + str(loss)[0:9] + " | " + str(rmse)[0:9] + " | " + str(time)[0:9] + " | " + str(nb_params)[0:9] + " |\n"
        results.write(res)
        param = grid.readline().split(" ")

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['numPart'] = str(sys.argv[1])
        kwargs['nb_neurons'] = str(sys.argv[2])
    main(**kwargs)