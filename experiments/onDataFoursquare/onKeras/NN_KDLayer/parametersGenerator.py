#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Génération d'un ensemble de jeu de paramètres suivant le gridSearch suivant:

codeArchis:
    - 0 : Les trois dense layers ne sont pas compressées
    - 1 : la première des 3 couches denses est KD-compressée, les deux autres non
    - 2 : les deux premières couches dense sont KD-compressée, la dernière non
    - 3 : les trois couches denses sont KD-compressée
    - 4 : la deuxièmre couche dense seulement est compressée
rank: décomposition de rang 1 à 10
dropout_values: test avec 3 valeurs de dropout (0.0 = abscence de dropout)
optimizers: 3 fonctions différentes pour le calcul de déscente de gradient
shape: nombre et valeurs des dimenssions du tenseurs qui sera compressé (reshape de la matrice W des paramètres)
       formé à partir des diviseurs les plus proches de la racine quatrième des dimension des données d'entrée. (racine
       quatrième parce que tensor d'ordre 4 ici).
nb_neurones: nombre de neurones dans les couches denses. Soit 500, soit 1000.

Les jeux de paramètres ainsi constitués sont stockés dans 20 fichiers différents dans le but d'exécuter 20 jobs en
simultané sur le cluster à partir du fichier "launcherExperimentsOnGridPart.py".

Remarque: Le gridSearch n'est pas exhaustif pour réduire le nombre d'expériences. On fige certains paramètres
à l'identique entre les diférentes couches denses compressées ou non du même réseau.
'''

import sys
import os
import scipy
import pylab
import time
import random

codeArchis = [0, 1, 2, 3, 4]
ranks = range(1, 11)
dropout_values  = [0, 0.2, 0.4]
optimizers = ["Adam", "Adagrad", "RMSprop"]
shape = ["5-6 7-4", "4-5 6-7"]

grid1000 = []
grid500 = []
for codeArchi in codeArchis:
    for rank in ranks:
        for dropOutValue in dropout_values:
            for optimizer in optimizers:
                if codeArchi == 0:
                    grid1000.append(str(codeArchi) + " 0-0 0-0 0-0 0-0 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(1000))
                    grid500.append(str(codeArchi) + " 0-0 0-0 0-0 0-0 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(500))
                if codeArchi == 4:
                    grid1000.append(str(codeArchi) + " 0-0 0-0 " + "5-5 5-8 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(1000))
                    grid500.append(str(codeArchi) + " 0-0 0-0 " + "5-5 5-4 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(500))
                if (codeArchi == 1) or (codeArchi == 2) or (codeArchi == 3):
                    for In1 in shape:
                        grid1000.append(str(codeArchi) + " " + In1 + " 5-5 5-8 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(1000))
                        grid500.append(str(codeArchi) + " " + In1 + " 5-5 5-4 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(500))

path = os.getcwd() + "/"
file500 = open(path + "gridOfParams/gridOfParams_Foursquare_KDLayer_500neurons.txt", "w")
file1000 = open(path + "gridOfParams/gridOfParams_Foursquare_KDLayer_1000neurons.txt", "w")

print("len(grid500) = ", len(grid500))
for e in grid500:
    file500.write(e + "\n")

print("len(grid1000) = ", len(grid1000))
for e in grid1000:
    file1000.write(e + "\n")

counter = 0
for numPart in range(0,10):
    print("File " + str(numPart+1) + " /10")
    gridPart = open(path + "gridOfParams/gridOfParams_Foursquare_KDLayer_500neurons_part" + str(numPart) + ".txt", 'w')

    numEndValue = counter + 72
    print "[" + str(counter) + " -> " + str(numEndValue) + "]"
    for param in grid500[counter: numEndValue]:
        gridPart.write(param + "\n")
    counter += 72

print("\n")
counter = 0
for numPart in range(0, 10):
    print("File " + str(numPart) + " /10")
    gridPart = open(path + "gridOfParams/gridOfParams_Foursquare_KDLayer_1000neurons_part" + str(numPart) + ".txt", 'w')

    numEndValue = counter + 72
    print "[" + str(counter) + " -> " + str(numEndValue) + "]"
    for param in grid1000[counter: numEndValue]:
        gridPart.write(param + "\n")
    counter += 72

print("Done!")
