#!/usr/bin/env python
#-*- coding: utf-8 -*-

'''
Classe qui implémente un réseau de neurone construit en fonction des différents paramètres fournis.
Elle renvoie les scores obtenues, le temps d'exécution ainsi que le nombre de paramètres.
'''

from __future__ import print_function
import os
import time
import sys
import keras
import numpy as np
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras import backend as K
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from KDLayer import KDLayer

def size(model): # Compute number of params in a model (the actual number of floats)
    return sum([np.prod(K.get_value(w).shape) for w in model.trainable_weights])

def main(codeArchi,
         shapeA1,
         shapeB1,
         shapeA2,
         shapeB2,
         rank,
         dropOutValue,
         num_part,
         optimizer,
         output_dim):

    shapeA1 = shapeA1.split("-")
    shapeA1 = (int(shapeA1[0]), int(shapeA1[1]))
    shapeB1 = shapeB1.split("-")
    shapeB1 = (int(shapeB1[0]), int(shapeB1[1]))
    shapeA2 = shapeA2.split("-")
    shapeA2 = (int(shapeA2[0]), int(shapeA2[1]))
    shapeB2 = shapeB2.split("-")
    shapeB2 = (int(shapeB2[0]), int(shapeB2[1]))
    epochs = 100
    batch_size = 2
    output_dim = int(output_dim)

    print("shapeA1 = ", shapeA1)
    print("shapeB1 = ", shapeB1)
    print("shapeA2 = ", shapeA2)
    print("shapeB2 = ", shapeB2)
    print("rank = ", rank)
    print("epochs = ", epochs)
    print("optimizer = ", optimizer)
    print("batch_size = ", batch_size)

    if optimizer == "Adam":
        optimizer = keras.optimizers.Adam()
    if optimizer == "Adagrad":
        optimizer = keras.optimizers.Adagrad()
    if optimizer == "RMSprop":
        optimizer = keras.optimizers.RMSprop()

    x_train = np.load('../../dataFoursquare_10Folds/X_train_' + str(num_part)+'.npy')
    y_train = np.load('../../dataFoursquare_10Folds/Y_train_' + str(num_part)+'.npy')
    x_test = np.load('../../dataFoursquare_10Folds/X_test_' + str(num_part)+'.npy')
    y_test = np.load('../../dataFoursquare_10Folds/Y_test_' + str(num_part)+'.npy')

    print("x_train.shape = ", x_train.shape)
    print("y_train.shape = ", y_train.shape)
    print("x_test.shape = ", x_test.shape)
    print("y_test.shape = ", y_test.shape)

    input_shape = (3, 15, 56)

    model = Sequential()

    model.add(Flatten(input_shape=input_shape)) # 1 x 2520
    if (codeArchi == 0) or (codeArchi == 4):
        model.add(Dense(output_dim))
    else:
        model.add(KDLayer(output_dim, shapeA1, shapeB1, shapeA2, shapeB2, rank, batch_size, input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(Dropout(dropOutValue))

    if (codeArchi == 0) or (codeArchi == 1):
        model.add(Dense(output_dim))
    else:
        model.add(KDLayer(output_dim, shapeA2, shapeB2, shapeA2, shapeB2, rank, batch_size))
    model.add(Activation('relu'))
    model.add(Dropout(dropOutValue))

    if not codeArchi == 3:
        model.add(Dense(output_dim))
    else:
        model.add(KDLayer(output_dim, shapeA2, shapeB2, shapeA2, shapeB2, rank, batch_size))
    model.add(Activation('relu'))
    model.add(Dropout(dropOutValue))
    model.add(Dense(840, activation='linear'))
    nb_params = int(size(model))

    model.compile(loss='mse',
                  optimizer = optimizer,
                  metrics=['mse'])
    print(model.summary())
    start_time = time.time()

    #lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=0, mode='auto', epsilon=0.0001, cooldown=0, min_lr=0)
    es = EarlyStopping(monitor='val_loss', min_delta=0.005, patience=3, verbose=1, mode='auto')
    model.fit(x_train,
              y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test),
              #callbacks=[lr, es])
              callbacks=[es])
    end_time = time.time() - start_time

    score = model.evaluate(x_test, y_test, verbose=1)
    score[0] = np.sqrt(score[0])
    score[1] = np.sqrt(score[1])
    print('Test loss:', score[0])
    print('Test RMSE:', score[1])
    print('Time:', end_time)

    return score[0], score[1], end_time, nb_params

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 0:
        kwargs['codeArchi'] = int(sys.argv[1])
        kwargs['shapeA1'] = str(sys.argv[2])
        kwargs['shapeB1'] = str(sys.argv[3])
        kwargs['shapeA2'] = str(sys.argv[4])
        kwargs['shapeB2'] = str(sys.argv[5])
        kwargs['rank'] = int(sys.argv[6])
        kwargs['dropOutValue'] = float(sys.argv[7])
        kwargs['num_part'] = int(sys.argv[8])
        kwargs['optimizer'] = str(sys.argv[9])
        kwargs['output_dim'] = int(sys.argv[10])
    main(**kwargs)