| 3 | 5-7-8-9 | 5-5-5-8 | 1-4-4-4-1 | 0.4 | Adagrad | nan | nan | 56.584434 | 844740.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-5-5-5-1 | 0.4 | Adagrad | nan | nan | 57.632410 | 846590.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-6-6-6-1 | 0.4 | Adagrad | nan | nan | 58.810333 | 848790.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-7-7-7-1 | 0.4 | Adagrad | nan | nan | 60.127044 | 851340.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-8-8-8-1 | 0.4 | Adagrad | 0.1271397 | 0.1271397 | 60.720176 | 854240.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-9-9-9-1 | 0.4 | Adagrad | nan | nan | 59.414191 | 857490.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-10-10-10-1 | 0.4 | Adagrad | 0.1266839 | 0.1266839 | 64.272807 | 861090.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-1-1-1-1 | 0.4 | Adagrad | nan | nan | 53.502650 | 841290.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-2-2-2-1 | 0.4 | Adagrad | 0.1278964 | 0.1278964 | 54.668636 | 842070.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-3-3-3-1 | 0.4 | Adagrad | nan | nan | 56.197623 | 843180.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-4-4-4-1 | 0.4 | Adagrad | 0.1276599 | 0.1276599 | 56.447387 | 844620.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-5-5-5-1 | 0.4 | Adagrad | nan | nan | 57.841897 | 846390.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-6-6-6-1 | 0.4 | Adagrad | 0.1276780 | 0.1276780 | 59.567787 | 848490.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-7-7-7-1 | 0.4 | Adagrad | 0.1277126 | 0.1277126 | 60.773626 | 850920.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-8-8-8-1 | 0.4 | Adagrad | 0.1273640 | 0.1273640 | 61.549740 | 853680.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-9-9-9-1 | 0.4 | Adagrad | 0.1271670 | 0.1271670 | 64.327530 | 856770.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-10-10-10-1 | 0.4 | Adagrad | nan | nan | 65.546380 | 860190.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-1-1-1-1 | 0.4 | RMSprop | nan | nan | 63.722653 | 841290.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-2-2-2-1 | 0.4 | RMSprop | nan | nan | 98.133272 | 842090.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-3-3-3-1 | 0.4 | RMSprop | nan | nan | 132.93417 | 843240.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-4-4-4-1 | 0.4 | RMSprop | 0.1289361 | 0.1289361 | 97.676364 | 844740.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-5-5-5-1 | 0.4 | RMSprop | 0.1290455 | 0.1290455 | 133.48321 | 846590.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-6-6-6-1 | 0.4 | RMSprop | 0.1288281 | 0.1288281 | 142.28494 | 848790.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-7-7-7-1 | 0.4 | RMSprop | nan | nan | 90.697053 | 851340.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-8-8-8-1 | 0.4 | RMSprop | 0.1286113 | 0.1286113 | 115.60125 | 854240.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-9-9-9-1 | 0.4 | RMSprop | 0.1285795 | 0.1285795 | 128.68561 | 857490.0 |
| 3 | 5-7-8-9 | 5-5-5-8 | 1-10-10-10-1 | 0.4 | RMSprop | nan | nan | 79.028168 | 861090.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-1-1-1-1 | 0.4 | RMSprop | nan | nan | 66.446892 | 841290.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-2-2-2-1 | 0.4 | RMSprop | nan | nan | 94.141191 | 842070.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-3-3-3-1 | 0.4 | RMSprop | nan | nan | 166.39276 | 843180.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-4-4-4-1 | 0.4 | RMSprop | 0.1291632 | 0.1291632 | 141.11699 | 844620.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-5-5-5-1 | 0.4 | RMSprop | 0.1289859 | 0.1289859 | 133.29201 | 846390.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-6-6-6-1 | 0.4 | RMSprop | 0.1287196 | 0.1287196 | 105.18480 | 848490.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-7-7-7-1 | 0.4 | RMSprop | nan | nan | 130.73092 | 850920.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-8-8-8-1 | 0.4 | RMSprop | 0.1286662 | 0.1286662 | 144.55474 | 853680.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-9-9-9-1 | 0.4 | RMSprop | 0.1285462 | 0.1285462 | 124.24420 | 856770.0 |
| 3 | 7-8-5-9 | 5-5-5-8 | 1-10-10-10-1 | 0.4 | RMSprop | nan | nan | 129.90348 | 860190.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-1-1-1-1 | 0.0 | Adam | 0.1234972 | 0.1234972 | 410.21836 | 4362979.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-2-2-2-1 | 0.0 | Adam | 0.1236606 | 0.1236606 | 408.71244 | 4363218.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-3-3-3-1 | 0.0 | Adam | 0.1236666 | 0.1236666 | 391.35721 | 4363557.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-4-4-4-1 | 0.0 | Adam | 0.1236810 | 0.1236810 | 386.71196 | 4363996.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-5-5-5-1 | 0.0 | Adam | 0.1238054 | 0.1238054 | 386.17652 | 4364535.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-6-6-6-1 | 0.0 | Adam | nan | nan | 371.70010 | 4365174.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-7-7-7-1 | 0.0 | Adam | 0.1236855 | 0.1236855 | 381.98101 | 4365913.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-8-8-8-1 | 0.0 | Adam | nan | nan | 359.46563 | 4366752.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-9-9-9-1 | 0.0 | Adam | 0.1236613 | 0.1236613 | 359.80754 | 4367691.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-10-10-10-1 | 0.0 | Adam | 0.1237132 | 0.1237132 | 357.75472 | 4368730.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-1-1-1-1 | 0.0 | Adagrad | 0.1229857 | 0.1229857 | 182.52117 | 4362979.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-2-2-2-1 | 0.0 | Adagrad | 0.1231909 | 0.1231909 | 183.24247 | 4363218.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-3-3-3-1 | 0.0 | Adagrad | 0.1231204 | 0.1231204 | 182.31145 | 4363557.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-4-4-4-1 | 0.0 | Adagrad | nan | nan | 177.09056 | 4363996.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-5-5-5-1 | 0.0 | Adagrad | 0.1233295 | 0.1233295 | 182.46941 | 4364535.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-6-6-6-1 | 0.0 | Adagrad | 0.1235036 | 0.1235036 | 184.29223 | 4365174.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-7-7-7-1 | 0.0 | Adagrad | 0.1233163 | 0.1233163 | 183.49446 | 4365913.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-8-8-8-1 | 0.0 | Adagrad | 0.1233367 | 0.1233367 | 183.93870 | 4366752.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-9-9-9-1 | 0.0 | Adagrad | 0.1232984 | 0.1232984 | 183.70202 | 4367691.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-10-10-10-1 | 0.0 | Adagrad | nan | nan | 182.16489 | 4368730.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-1-1-1-1 | 0.0 | RMSprop | 0.1239944 | 0.1239944 | 369.60488 | 4362979.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-2-2-2-1 | 0.0 | RMSprop | nan | nan | 352.52650 | 4363218.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-3-3-3-1 | 0.0 | RMSprop | 0.1240148 | 0.1240148 | 367.90144 | 4363557.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-4-4-4-1 | 0.0 | RMSprop | 0.1240173 | 0.1240173 | 366.69549 | 4363996.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-5-5-5-1 | 0.0 | RMSprop | 0.1239700 | 0.1239700 | 364.31789 | 4364535.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-6-6-6-1 | 0.0 | RMSprop | nan | nan | 333.89828 | 4365174.0 |
| 4 | 0-0-0-0 | 5-5-5-8 | 1-7-7-7-1 | 0.0 | RMSprop | 0.1241923 | 0.1241923 | 350.53935 | 4365913.0 |
