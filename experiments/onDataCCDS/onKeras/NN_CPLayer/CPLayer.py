#!/usr/bin/env python
# -*- coding: utf-8 -*-OAR.1620807.stdout

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import theano
import theano.tensor as T
import numpy as np
import theano as th
from theano import tensor
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class CPLayer(Layer):
    def __init__(self, cp_input_shape, cp_output_shape, rank, **kwargs):

        super(CPLayer, self).__init__(**kwargs)
        cp_input_shape = np.array(cp_input_shape)
        cp_output_shape = np.array(cp_output_shape)
        rank = int(rank)

        if cp_input_shape.shape[0] != cp_output_shape.shape[0]:
            raise ValueError("The number of input and output dimensions should be the same.")

        if isinstance(rank, int) != True:
            raise ValueError("The rank should be an integer.")

        # print("Hello depuis le constructeur")
        self.cp_input_shape = cp_input_shape
        self.cp_output_shape = cp_output_shape
        self.rank = rank
        self.num_dim = cp_input_shape.shape[0]



    def build(self, input_shape):       # Create a trainable weight variable for this layer.
        super(CPLayer, self).build(input_shape)  # Be sure to call this somewhere!

        cp_input_shape = np.array(self.cp_input_shape)
        cp_output_shape = np.array(self.cp_output_shape)
        rank = int(self.rank)
        ranks = np.empty(cp_input_shape.shape[0])
        ranks.fill(rank)
        cores_arr_len = int(np.sum(cp_input_shape * cp_output_shape * ranks))
        print("cores_arr_len = ", cores_arr_len)

        self.cores_arr = self.add_weight(name="cores_array", shape=(cores_arr_len,), initializer='glorot_uniform', trainable=True)

    def call(self, input):
        t = self.cp_input_shape * self.cp_output_shape
        W = T.zeros(t.tolist())
        for r in range(self.rank):
            idx=0
            A = T.ones((1,))
            for i in range(self.num_dim):
                shape = (self.cp_input_shape[i] * self.cp_output_shape[i], self.rank)
                G = self.cores_arr[idx:idx + T.prod(shape)].reshape(shape)
                idx += T.prod(shape)
                A = T.outer(A, G[:, r])
            A = A.reshape((self.cp_input_shape * self.cp_output_shape))
            W += A

        if input.ndim > 2:
            input = input.flatten(2)

        res = T.dot(input,W.reshape((np.prod(self.cp_input_shape), -1)))

        return res

    def compute_output_shape(self, input_shape):
        return input_shape[0], np.prod(self.cp_output_shape)
