#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import os
from neuralNetwork_CPLayer import main as CPMain
import numpy as np

def main(numPart):
    folder_path = os.getcwd() + "/"

    grid = open(folder_path + "/gridOfParams_CCDS_KDLayer_part" + str(numPart) + ".txt", 'r')
    results = open(folder_path + "results1000_part" + numPart + ".txt", 'w')
    param = grid.readline().split(" ")

    while not param == ["\n"]:
        try:
            print(">> NEW EXPE <<")
            # 0 5-15 17-5 5-5 5-8 0 0.2 Adam
            # codeArchi, shapeA1, shapeB1, shapeA2, shapeB2, rank, dropOutValue, num_part, optimizer)
            codeArchi = int(param[0])
            shapeA1 = str(param[1])
            shapeB1 = str(param[2])
            shapeA2 = str(param[3])
            shapeB2 = str(param[4])
            rank = int(param[5])
            dropOutValue = int(param[6])
            optimizer = str(param[7])[:-1]
            output_dim = 1000


            # Apprentissage du réseau avec cross validation k-fold, k = 10
            lossArray = []
            rmseArray = []
            timeArray = []
            nb_paramsArray = []

            for num_part in range(1, 11):
                print(">>>> CROSS-VALIDATION : " + str(num_part) + "/10 <<<<")

                res = CPMain(codeArchi, shapeA1, shapeB1, shapeA2, shapeB2, rank, dropOutValue, num_part, optimizer, output_dim)
                print(res)
                lossArray.append(res[0])
                rmseArray.append(res[1])
                timeArray.append(res[2])
                nb_paramsArray.append(res[3])

            loss = np.mean(lossArray)
            rmse = np.mean(rmseArray)
            time = np.mean(timeArray)
            nb_params = np.mean(nb_paramsArray)
            res = "| " + str(codeArchi) + " | " + str(shapeA1) + " | " + str(shapeB1) + " | " + str(shapeA2) +\
                  " | " + str(shapeB2) + " | " + str(rank) + " | " + str(dropOutValue) + " | " + str(optimizer) +\
                  " | " + str(loss)[0:8] + " | " + str(rmse)[0:8] + " | " + str(time)[0:8] + " | " + str(nb_params)[0:8] + " |\n"
            results.write(res)

        except Exception,e:
            print("\n")
            print("/!\\ ATTENTION:")
            print("L'experience suivante a plante:")
            print(param)
            print("Message d'erreur : ")
            print(str(e))
            print("\n")
        param = grid.readline().split(" ")

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['numPart'] = str(sys.argv[1])
    main(**kwargs)