#!/usr/bin/env python
#-*- coding: utf-8 -*-

from __future__ import print_function
import os
import time
import sys
import keras
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras import backend as K
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping
from CPLayer import CPLayer

def size(model): # Compute number of params in a model (the actual number of floats)
    return sum([np.prod(K.get_value(w).shape) for w in model.trainable_weights])

def main(codeArchi,
         shapeA,
         shapeB,
         rank,
         dropOutValue,
         num_part,
         optimizer,
         output_dim):

    shapeA = shapeA.split("-")
    shapeB = shapeB.split("-")

    counter = 0
    for e in shapeA:
        shapeA[counter] = int(e)
        counter += 1

    counter = 0
    for e in shapeB:
        shapeB[counter] = int(e)
        counter += 1

    shapeA = tuple(shapeA)
    shapeB = tuple(shapeB)

    epochs = 100
    batch_size = 6

    print("shapeA = ", shapeA)
    print("shapeB = ", shapeB)
    print("rank = ", rank)
    print("epochs = ", epochs)
    print("optimizer = ", optimizer)
    print("batch_size = ", batch_size)

    if optimizer == "Adam":
        optimizer = keras.optimizers.Adam()
    if optimizer == "Adagrad":
        optimizer = keras.optimizers.Adagrad()
    if optimizer == "RMSprop":
        optimizer = keras.optimizers.RMSprop()

    x_train = np.load('../../onLasagne/10_jeux_de_donnees/X_train_'+str(num_part)+'.npy')
    y_train = np.load('../../onLasagne/10_jeux_de_donnees/Y_train_'+str(num_part)+'.npy')
    x_test = np.load('../../onLasagne/10_jeux_de_donnees/X_test_'+str(num_part)+'.npy')
    y_test = np.load('../../onLasagne/10_jeux_de_donnees/Y_test_'+str(num_part)+'.npy')

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')

    input_shape = (3, 17, 125)

    model = Sequential()

    model.add(Flatten(input_shape=input_shape)) # 1 x 6375
    if (codeArchi == 0) or (codeArchi == 4):
        model.add(Dense(output_dim))
    else:
        model.add(CPLayer(shapeA, shapeB, rank))
    model.add(Activation('relu'))
    model.add(Dropout(dropOutValue))

    if (codeArchi == 0) or (codeArchi == 1):
        model.add(Dense(output_dim))
    else:
        model.add(CPLayer(shapeB, shapeB, rank))
    model.add(Activation('relu'))
    model.add(Dropout(dropOutValue))

    if not codeArchi == 3:
        model.add(Dense(output_dim))
    else:
        model.add(CPLayer(shapeB, shapeB, rank))
    model.add(Activation('relu'))
    model.add(Dropout(dropOutValue))
    model.add(Dense(2125, activation='linear'))
    nb_params = int(size(model))

    model.compile(loss='mse',
                  optimizer = optimizer,
                  metrics=['mse'])
    print(model.summary())
    start_time = time.time()

    #lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=0, mode='auto', epsilon=0.0001, cooldown=0, min_lr=0)
    es = EarlyStopping(monitor='val_loss', min_delta=0.005, patience=3, verbose=1, mode='auto')
    model.fit(x_train,
              y_train,
              batch_size=batch_size,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test),
              #callbacks=[lr, es])
              callbacks=[es])
    end_time = time.time() - start_time

    score = model.evaluate(x_test, y_test, verbose=1)
    score[0] = np.sqrt(score[0])
    score[1] = np.sqrt(score[1])
    print("AVEC batcnNorm(axis=0) et dropOut(0,25)")
    print('Test loss:', score[0])
    print('Test RMSE:', score[1])
    print('Time:', end_time)

    return score[0], score[1], end_time, nb_params

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 0:
        kwargs['codeArchi'] = int(sys.argv[1])
        kwargs['shapeA'] = str(sys.argv[2])
        kwargs['shapeB'] = str(sys.argv[3])
        kwargs['rank'] = int(sys.argv[4])
        kwargs['dropOutValue'] = float(sys.argv[5])
        kwargs['num_part'] = int(sys.argv[6])
        kwargs['optimizer'] = str(sys.argv[7])
        kwargs['output_dim'] = int(sys.argv[8])
    main(**kwargs)