#!/usr/bin/env python
# -*- coding: utf-8 -*-OAR.1620807.stdout

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import theano
import theano.tensor as T
import numpy as np
import theano as th
from theano import tensor
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class TTLayer(Layer):
    def __init__(self, tt_input_shape, tt_output_shape, ranks, **kwargs):

        super(TTLayer, self).__init__(**kwargs)
        tt_input_shape = np.array(tt_input_shape)
        tt_output_shape = np.array(tt_output_shape)
        ranks = np.array(ranks)

        if tt_input_shape.shape[0] != tt_output_shape.shape[0]:
            raise ValueError("The number of input and output dimensions should be the same.")

        for r in ranks:
            if isinstance(r, int) != True:
                raise ValueError("The rank should be an tuple of integer.")

        # print("Hello depuis le constructeur")
        self.tt_input_shape = tt_input_shape
        self.tt_output_shape = tt_output_shape
        self.ranks = ranks
        self.num_dim = tt_input_shape.shape[0]

    def build(self, input_shape):       # Create a trainable weight variable for this layer.

        tt_input_shape = np.array(self.tt_input_shape)
        tt_output_shape = np.array(self.tt_output_shape)
        ranks = np.array(self.ranks)
        cores_arr_len = np.sum(tt_input_shape * tt_output_shape * ranks[1:] * ranks[:-1])
        print("cores_arr_len = ", cores_arr_len)
        self.cores_arr = self.add_weight(name="cores_array", shape=(cores_arr_len,), initializer='glorot_normal', trainable=True)

        super(TTLayer, self).build(input_shape)  # Be sure to call this somewhere!


    def call(self, input):
        res = input
        core_arr_idx = 0
        for k in range(self.num_dim - 1, -1, -1):
            curr_shape = (self.tt_input_shape[k] * self.ranks[k + 1], self.ranks[k] * self.tt_output_shape[k])
            curr_core = self.cores_arr[core_arr_idx:core_arr_idx+T.prod(curr_shape)].reshape(curr_shape)
            res = T.dot(res.reshape((-1, curr_shape[0])), curr_core)
            res = T.transpose(res.reshape((-1, self.tt_output_shape[k])))
            core_arr_idx += T.prod(curr_shape)

        res = T.transpose(res.reshape((-1, input.shape[0])))

        return res

    def compute_output_shape(self, input_shape):
        return (input_shape[0], np.prod(self.tt_output_shape))
