#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import scipy
import pylab
import time
import random

codeArchis = [0, 1, 2, 3, 4]
rValues = range(1, 11)
dropOutValues  = [0, 0.2, 0.4]
optimizers = ["Adam", "Adagrad", "RMSprop"]
shape1 = ["5-15-17-5", "17-5-5-15"]

ranks = []
for r in rValues:
    rank = [1, r, r, r, 1]
    ranks.append(rank)

grid1000 = []
grid500 = []
for codeArchi in codeArchis:
    for rank in ranks:
        for dropOutValue in dropOutValues:
            for optimizer in optimizers:
                if codeArchi == 0:
                    grid1000.append(str(codeArchi) + " 0-0-0-0 0-0-0-0 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(1000))
                    grid500.append(str(codeArchi) + " 0-0-0-0 0-0-0-0 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(500))
                if codeArchi == 4:
                    grid1000.append(str(codeArchi) + " 0-0-0-0 " + "5-5-5-8 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(1000))
                    grid500.append(str(codeArchi) + " 0-0-0-0 " + "5-5-5-4 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(500))
                if (codeArchi == 1) or (codeArchi == 2) or (codeArchi == 3):
                    for In1 in shape1:
                        grid1000.append(str(codeArchi) + " " + In1 + " 5-5-5-8 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(1000))
                        grid500.append(str(codeArchi) + " " + In1 + " 5-5-5-4 " + str(rank) + " " + str(dropOutValue) + " " + optimizer + " " + str(500))

path = os.getcwd() + "/"
file500 = open(path + "gridOfParams_CCDS_TTLayer_500neurons.txt", "w")
file1000 = open(path + "gridOfParams_CCDS_TTLayer_1000neurons.txt", "w")

print("len(grid500) = ", len(grid500))
for e in grid500:
    file500.write(e + "\n")

print("len(grid1000) = ", len(grid1000))
for e in grid1000:
    file1000.write(e + "\n")

counter = 0
for numPart in range(0,10):
    print("File " + str(numPart+1) + " /10")
    gridPart = open(path + "gridOfParams_CCDS_TTLayer_500neurons_part" + str(numPart) + ".txt", 'w')

    numEndValue = counter + 72
    print "[" + str(counter) + " -> " + str(numEndValue) + "]"
    for param in grid500[counter: numEndValue]:
        gridPart.write(param + "\n")
    counter += 72

print("\n")
counter = 0
for numPart in range(0, 10):
    print("File " + str(numPart) + " /10")
    gridPart = open(path + "gridOfParams_CCDS_TTLayer_1000neurons_part" + str(numPart) + ".txt", 'w')

    numEndValue = counter + 72
    print "[" + str(counter) + " -> " + str(numEndValue) + "]"
    for param in grid1000[counter: numEndValue]:
        gridPart.write(param + "\n")
    counter += 72

print("Done!")
