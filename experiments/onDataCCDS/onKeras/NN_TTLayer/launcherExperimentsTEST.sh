echo "Lancement des expes avec 1000 neurones sur les grilles de parametres part 11 a 11..."
for i in `seq 11 11`;
do
	oarsub -p "gpu is null" -l walltime=120 "python launcherExperimentsOnGridPart.py $i 1000"
done


echo "Lancement des expes avec 500 neurones sur les grilles de parametres part 11 a 11..."
for i in `seq 11 11`;
do
	oarsub -p "gpu is null" -l walltime=120 "python launcherExperimentsOnGridPart.py $i 500"
done
echo "Done!"