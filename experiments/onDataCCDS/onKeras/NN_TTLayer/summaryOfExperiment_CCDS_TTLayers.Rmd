---
title: "TTLayers"
author: "On CCDS data"
output: pdf_document
---
# Description

Retranscription et analyse des résultats des expériences sur le corpus CCDS et une architecture utilisant des TTLayers (couches denses compressées avec la méthode de TT-décomposition).

Expériences réalisées en gridSearch sur les paramètres suivants:

* codeArchis (cA):
    - 0 : Les trois dense layers ne sont pas compressées (ie MLP)
    - 1 : la première des 3 couches denses est KD-compressée, les deux autres non
    - 2 : les deux premières couches dense sont KD-compressée, la dernière non
    - 3 : les trois couches denses sont KD-compressée
    - 4 : la deuxièmre couche dense seulement est compressée
* rank (r): décomposition de rang 1 à 10
* dropout_values (doutV): test avec 3 valeurs de dropout (0.0 = abscence de dropout)
* optimizers (opt): 3 fonctions différentes pour le calcul de déscente de gradient
* shape (sA): nombre et valeurs des dimenssions du tenseurs qui sera compressé (reshape de la matrice W des paramètres)
       formé à partir des diviseurs les plus proches de la racine quatrième des dimensions des données d'entrée. (racine
       quatrième parce que tensor d'ordre 4 ici).
* nb_neurones (nbN): nombre de neurones dans les couches denses. Soit 500, soit 1000.

# Liste des 20 meilleurs résultats

| cA | sA | sB | r | doutV | opt | rmse | time | nb_params | nbN |
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
| 3 | 5-15-17-5 | 5-5-5-8 | 6 | 0.4 | Adagrad | **0.7836708** | 24.2 | 2137943 | 1000 |
| 3 | 5-15-17-5 | 5-5-5-8 | 9 | 0.2 | Adagrad | 0.7838767 | 24.26 | 2150372 | 1000 |
| 2 | 17-5-5-15 | 5-5-5-8 | 5 | 0.4 | Adagrad | 0.7862427 | 24.78 | 3132095 | 1000 |
| 3 | 5-15-17-5 | 5-5-5-8 | 9 | 0.4 | Adagrad | 0.7863833 | 33.29 | 2150372 | 1000 |
| 3 | 5-15-17-5 | 5-5-5-8 | 8 | 0.4 | Adagrad | 0.7866421 | 31.35 | 2145709 | 1000 |
| 3 | 5-15-17-5 | 5-5-5-8 | 6 | 0.2 | Adagrad | 0.7866796 | 18.8 | 2137943 | 1000 |
| 2 | 5-15-17-5 | 5-5-5-4 | 6 | 0.2 | Adagrad | 0.7867341 | 16.99 | 1323201 | 500 |
| 3 | 17-5-5-15 | 5-5-5-8 | 8 | 0.4 | Adagrad | 0.7868172 | 30.46 | 2139789 | 1000 |
| 2 | 5-15-17-5 | 5-5-5-4 | 5 | 0.4 | RMSprop | 0.7868619 | 18.51 | 1320805 | 500 |
| 1 | 17-5-5-15 | 5-5-5-4 | 1 | 0.4 | Adagrad | 0.7868639 | 17.93 | 1572075 | 500 |
| 2 | 5-15-17-5 | 5-5-5-8 | 7 | 0.4 | Adagrad | 0.7868964 | 24.87 | 3139493 | 1000 |
| 3 | 17-5-5-15 | 5-5-5-8 | 1 | 0.4 | RMSprop | 0.7871816 | 24.21 | 2145955 | 1000 |
| 3 | 5-15-17-5 | 5-5-5-8 | 1 | 0.4 | RMSprop | 0.7872007 | 28.58 | 2155555 | 1000 |
| 2 | 17-5-5-15 | 5-5-5-4 | 8 | 0.4 | Adagrad | 0.7872634 | 18.2 | 1323013 | 500 |
| 2 | 5-15-17-5 | 5-5-5-8 | 4 | 0.4 | Adagrad | 0.7874509 | 24.3 | 3132101 | 1000 |
| 2 | 5-15-17-5 | 5-5-5-8 | 1 | 0.4 | Adagrad | 0.7875742 | 28.41 | 3150665 | 1000 |
| 3 | 17-5-5-15 | 5-5-5-8 | 1 | 0.4 | Adam | 0.7876499 | 24.63 | 2145955 | 1000 |
| 2 | 5-15-17-5 | 5-5-5-4 | 6 | 0.4 | Adagrad | 0.7878606 | 19.22 | 1323201 | 500 |
| 3 | 5-15-17-5 | 5-5-5-8 | 5 | 0.4 | Adagrad | 0.7880067 | 23.48 | 2134840 | 1000 |
| 2 | 5-15-17-5 | 5-5-5-4 | 7 | 0.4 | RMSprop | 0.7881664 | 20.69 | 1326017 | 500 |

\newpage

# Graphique

![Erreur RMSE triées par ordre croissante](graphs/RMSEbyTopDown_AllArchitecture.png)

Dans le dossier *orderResults* se trouve la liste de toutes les expériences et leurs résultats dans l'ordre de performance:

* pour toutes les architectures confondues
* pour chacune des architectures séparément

Dans le dossier *graphs* se trouve un ensemble de graphiques qui tracent la valeur d'erreur RMSE de chaque expérience par rapport à une composante du gridSearch (à chaque fois pour l'ensemble des architectures puis chacune séparément) :

* valeur de dropout
* type de fonction d'optimizer
* valeur du rank  

Se trouve aussi de la même manière les graphiques des valeurs d'erreur RMSE classées dans l'ordre croissant.

\newpage

![Résultats de Guillaume Rabusseau](resultsRabuseau.png)

# Conclusion

Les deux seuls paramètres du gridSearch qui se démarque sont la fonction d'optimizer *Adagrad* et dans une moindre mesure le nombre de 1000 neurones au sein des couches dense compressées ou non.
Le meilleur jeu est celui la : 

| cA | sA | sB | r | doutV | opt | rmse | time | nb_params | nbN |
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
| 3 | 5-15-17-5 | 5-5-5-8 | 6 | 0.4 | Adagrad | 0.7836708 | 24.2 | 2137943 | 1000 |

avec une erreur de **0.7837**. C'est un peu mieux que les résultats de Guillaume Rabuseau.

Pour information, les meilleurs résultats pour chaque architecture sont les suivants.
La dernière colonne correspond à leur rang dans le classement de l'ensemble des résultats.

| cA | sA | sB | r | doutV | opt | rmse | time | nb_params | nbN | place |
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
| 0 | 0-0-0-0 | 0-0-0-0 | 1 | 0.0 | Adagrad | **0.8050487** | 36.47 | 4753625 | 500 | 677e |
| 1 | 17-5-5-15 | 5-5-5-4 | 1 | 0.4 | Adagrad | **0.7868639** | 17.93 | 1572075 | 500 | 10e |
| 2 | 17-5-5-15 | 5-5-5-8 | 5 | 0.4 | Adagrad | **0.7862427** | 24.78 | 3132095 | 1000 | 3e |
| 3 | 5-15-17-5 | 5-5-5-8 | 6 | 0.4 | Adagrad | **0.7836708** | 24.2 | 2137943 | 1000 | 1e |
| 4 | 0-0-0-0 | 5-5-5-4 | 9 | 0.2 | Adagrad | **0.7909555** | 18.79 | 4507544 | 500 | 77e |




