#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import scipy
import pylab
import time
from random import randrange


def main(nb_input_neurons=6375, nb_output_neurons=1024, rank_min=1, rank_max=10, nb_epochs=5):

    def isPrime(n):
        """
        Teste si n est premier ou non.

        :param n:
        :type n: int
        :return: boolean
        """

        Dmax = scipy.sqrt(n)
        if n == 2:
            return True
        if n % 2 == 0:
            return False
        d = 3
        while n % d != 0 and d <= Dmax:
            d += 2
        return d > Dmax

    def get_decompositions(nb):
        """
        Renvois la liste des decompositions possible de nb (valeur en parametre) sous forme de tuples.

        :param nb: Nombre à decomposer
        :type nb: int
        :return: Une liste de tuples.
        :rtype: liste
        """

        diviseur = 1
        to_return = []
        while diviseur < (nb/2 +1):
            diviseur += 1
            result = nb/float(diviseur)
            if not (result.is_integer()):
                continue
            result = int(result)
            if isPrime(result):
                to_return.append((diviseur, result))
            else:
                to_return.append((diviseur, result))
                for tuple in get_decompositions(result):
                    to_return.append((diviseur,) + tuple)
        return to_return

    def get_grid_of_parameter(input_decompositions, output_decompositions):
        """
        Renvois un tableau de n lignes et 4 colonnes contenant l'ensembles des parametres qui serviront aux experiences.

        :param input_decompositions: Liste des decompositions possibles en entree.
        :param output_decompositions: Liste des decompositions possibles en sortie.

        :type  input_decompositions: liste
        :type  output_decompositions: liste

        :return: Renvois une liste de 4 listes
        :rtype: liste. Les 4 sous-listes contiennent respectivement : des tuples, des tuples, des int et des int.
        """

        # Dans l'ordre des colonnes : input_shape, output_shape, rank, nb_epochs
        gridOfParameters = [[], [], [], []]

        # Classement des decompositions output
        output_decomp_classified_by_length = {}
        for input_decomposition in output_decompositions:
            length = len(input_decomposition)
            if not (length in output_decomp_classified_by_length.keys()):
                output_decomp_classified_by_length[length] = []
                output_decomp_classified_by_length[length].append(input_decomposition)
            else:
                output_decomp_classified_by_length[length].append(input_decomposition)


        # Remplissage de la grille
        counter = 0
        for input_decomposition in input_decompositions:
            length = len(input_decomposition)
            if length in output_decomp_classified_by_length.keys():
                for output_decomposition in output_decomp_classified_by_length[length]:

                    ranks = []
                    nbDimShape = len(input_decomposition)
                    if nbDimShape == 2:
                        for i in range(rank_min, rank_max + 1):
                            ranks.append([1, i, 1])
                    if nbDimShape == 3:
                        for i in range(rank_min, rank_max + 1):
                            for j in range(rank_min, rank_max + 1):
                                ranks.append([1, i, j, 1])
                    if nbDimShape == 4:
                        for i in range(rank_min, rank_max + 1):
                            for j in range(rank_min, rank_max + 1):
                                for k in range(rank_min, rank_max + 1):
                                    ranks.append([1, i, j, k, 1])
                    if nbDimShape == 5:
                        for i in range(rank_min, rank_max + 1):
                            for j in range(rank_min, rank_max + 1):
                                for k in range(rank_min, rank_max + 1):
                                    for l in range(rank_min, rank_max + 1):
                                        ranks.append([1, i, j, k, l, 1])
                    if nbDimShape == 6:
                        for i in range(rank_min, rank_max + 1):
                            for j in range(rank_min, rank_max + 1):
                                for k in range(rank_min, rank_max + 1):
                                    for l in range(rank_min, rank_max + 1):
                                        for m in range(rank_min, rank_max + 1):
                                            ranks.append([1, i, j, k, l, m, 1])
                    if (nbDimShape > 6):
                        print("\nAJOUTER UNE DECOMPOSITION DE RANK\n")
                        exit()

                    for r in ranks:
                        gridOfParameters[0].append(input_decomposition)
                        gridOfParameters[1].append(output_decomposition)
                        gridOfParameters[2].append(r)
                        gridOfParameters[3].append(nb_epochs)
        return gridOfParameters

    # Corps de la fonction main
    print("Calcul des decompositions...")
    input_decompositions = get_decompositions(nb_input_neurons)
    input_decompositions.append((nb_input_neurons, 1))
    input_decompositions.append((1, nb_input_neurons))

    output_decompositions = get_decompositions(int(nb_output_neurons))
    output_decompositions.append((nb_output_neurons, 1))
    output_decompositions.append((1, nb_output_neurons))

    print("Ecriture de la grille des paramètres...")
    gridOfParameters = get_grid_of_parameter(input_decompositions, output_decompositions)

    for n in range(0,11):
        print("Ecriture dans le fichier numeros " + str(n) + " de 300 parametres...")
        # Ecriture de la grille des paramètres dans le fichier 'gridOfParameters.txt'.
        nb_exp = len(gridOfParameters[0])
        file = open(os.getcwd() + "/gridOfParameters_CCDS_TTLayer_part" + str(n) + ".txt", "w")

        num_exp = []
        for i in range(0, 301):
            num_exp.append(randrange(1,nb_exp))

        for num in num_exp:
            input_shape = ""
            output_shape = ""
            rank = ""

            for elmt in (gridOfParameters[0][num])[:-1]:
                input_shape += str(elmt) + "-"
            input_shape += str((gridOfParameters[0][num])[-1]) + ""

            for elmt in (gridOfParameters[1][num])[:-1]:
                output_shape += str(elmt) + "-"
            output_shape += str((gridOfParameters[1][num])[-1]) + ""

            for elmt in gridOfParameters[2][num]:
                rank += str(elmt) + "-"
            rank = rank[:-1]

            line = input_shape + " " + output_shape + " " + rank + " " + str(gridOfParameters[3][num])
            file.write(line + "\n")
        file.close()

    print("Done!")

if __name__ == '__main__':
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['nb_input_neurons'] = int(sys.argv[1])
            kwargs['nb_output_neurons'] = int(sys.argv[2])
            kwargs['rank_max'] = int(sys.argv[3])
            kwargs['nb_epochs'] = int(sys.argv[4])
        main(**kwargs)