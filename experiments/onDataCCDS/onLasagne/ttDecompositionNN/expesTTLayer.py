#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from USHCNwTTLayer import main as USHCNmain
import numpy as np

def _calcul_val(input_shape, output_shape, rank, accuracy):
    nb_param_denseLayer = 3*17*125

    input_shape_array = input_shape.split("-")
    output_shape_array = output_shape.split("-")
    rank_array = rank.split("-")

    nb_param = 0
    for i in range(0, len(input_shape_array)):
        nb_param += int(rank_array[i]) * int(input_shape_array[i]) * int(output_shape_array[i]) * int(rank_array[i+1])

    taux_de_compression = float(nb_param) / nb_param_denseLayer
    val = (float(accuracy) / 100) * (1 - taux_de_compression)
    return (nb_param, val)

def main(numFold):
    folder_path = '/data1/home/pierre-antoine.martrice/stageQarma2017/expe/onDataCCDS/theano/ttDecompositionNN/'
    grid = open(folder_path + "gridOfParameters_CCDS_TTLayer_part" + str(numFold) + ".txt", 'r')
    results = open(folder_path + "results_part" + numFold + ".txt", 'w')
    param = grid.readline().split(" ")

    while not param == [""]:
        try:
            print("### param = ", param)
            input_shape = param[0]
            output_shape = param[1]
            rank = param[2]
            num_epochs = 5

            # Apprentissage du réseau avec cross validation k-fold, k = 10
            lossArray = []
            accArray = []
            timeArray = []
            valArray = []
            print(">> NEW EXPE <<")
            for i in range(1, 11):
                print(">>>> CROSS-VALIDATION : " + str(i) + "/10 <<<<")
                res = USHCNmain(input_shape, output_shape, rank, i, num_epochs)
                print(res)
                if res[0] < 10000:
                    lossArray.append(res[0])
                else:
                    print("loss > 10000 !")
                    lossArray.append(10000)

                accArray.append(res[1])
                timeArray.append(res[2])

                nb_param, localVal =  _calcul_val(input_shape, output_shape, rank, res[1])
                valArray.append(localVal)
                print("nb_param = ", nb_param)
                print("localVal = ", localVal)

            loss = np.mean(lossArray)
            acc = np.mean(accArray)
            time = np.mean(timeArray)
            val = np.mean(valArray)

            if val < 1:
                res = "| " + str(input_shape) + " | " + str(output_shape) + " | " + str(rank) + " | " + str(loss) +\
                      " | " + str(acc) + " | " + str(time) + " | " + str(nb_param) + " | " + str(val) + " |\n"
                results.write(res)
                print(res)
        except:
            print("\nATTENTION:\n")
            print("\nL'experience suivant a plante:")
            print(param)
            print("\n")
        param = grid.readline().split(" ")

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['numFold'] = str(sys.argv[1])
    main(**kwargs)