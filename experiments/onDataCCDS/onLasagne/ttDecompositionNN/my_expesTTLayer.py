#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from USHCNwTTLayer import main as USHCNmain
import numpy as np

folder_path = '/data1/home/pierre-antoine.martrice/stageQarma2017/expe/onDataCCDS/theano/ttDecompositionNN/'
results = open(folder_path + "my_results.txt", 'w')

input_shape = "5-255-5"
output_shape = "8-16-8"
rank = "1-4-4-1"
num_epochs = 5

input_shape_array = input_shape.split("-")
output_shape_array = output_shape.split("-")
rank_array = rank.split("-")

nb_param = 0
for i in range(0, len(input_shape_array)):
    nb_param += int(rank_array[i]) * int(input_shape_array[i]) * int(output_shape_array[i]) * int(rank_array[i + 1])
print("nb_param = ", nb_param)

# Apprentissage du réseau avec cross validation k-fold, k = 10
lossArray = []
accArray = []
timeArray = []
print(">> NEW EXPE <<")
for i in range(1, 11):
    print(">>>> CROSS-VALIDATION : " + str(i) + "/10 <<<<")
    res = USHCNmain(input_shape, output_shape, rank, i, num_epochs)
    print(res)
    if res[0] < 10000:
        lossArray.append(res[0])
    else:
        print("loss > 10000 !")
        lossArray.append(10000)

    accArray.append(res[1])
    timeArray.append(res[2])

loss = np.mean(lossArray)
acc = np.mean(accArray)
time = np.mean(timeArray)

res = "| " + str(input_shape) + " | " + str(output_shape) + " | " + str(rank) + " | " + str(loss) +\
      " | " + str(acc) + " | " + str(time) + " | " + str(nb_param) + " |\n"
results.write(res)
print(res)

