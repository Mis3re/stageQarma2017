---
title: "The best set of parameters for CP decomposition"
author: "On CCDS data"
output: pdf_document
---
**Description :** Cette expérience consiste à entrainer un même réseaux de neurones sur les données CCDS avec différents paramètres et à comparer les scores obtenus.

L'architecture du réseau de neurone est assez simple (réalisé avec Lasagne) :  
```{python, eval=FALSE, include=TRUE}
net={}
    #Input layer    
    net['l_in'] = lasagne.layers.InputLayer(shape=(None, 3, 17, 125), input_var=input_var)
    #CPLayer
    net['l_hid1'] = CPLayer(net['l_in'], cp_input_shape, cp_output_shape, rank,
                            nonlinearity=lasagne.nonlinearities.rectify)
    #Fully_connected
    net['l_hid2'] = lasagne.layers.DenseLayer(['l_hid1'], num_units=1024,
                                              nonlinearity=lasagne.nonlinearities.rectify)
    #Fully_connected
    net['l_out'] = lasagne.layers.DenseLayer(net['l_hid2'], num_units= 2125 ,
                                            nonlinearity=lasagne.nonlinearities.identity)
```

Nous avons donc 2 couches cachées suivies d'une fonction d'activation ReLU et la couche de sortie d'une fonction d'activation softMax.

La couche CP va compresser le tenseur qui pondère les connexions aux neurones suivant une certaine valeur de rang.
L'efficacité de cette couche va donc dépendre de la CP décomposition obtenue. Nous la faisons varier d'une part à l'aide de la valeur du rang et d'autre part en effecuant un changement de la forme des tenseurs de pondérations en entrée et en sortie de la couche.

Dans cette expérience, nous avons tiré au hasard 3000 jeux de paramètres parmis tout ceux possibles tel que le rang de la CP décomposition varie entre 4 et 10 et tous les couples de formes possibles sont pris en compte pour les tenseurs d'entrée et de sortie.
Le premier tenseur est constitué des données CCDS qui ont une taille de 3*17*125, ce qui nous donne quel que soit le reshape un total de 6375 valeurs de pondération. Le second possède 1024 valeurs qui correspondent à autant de neurones de sortie que possède la couche.

Chaque exécution retourne 3 valeurs : l'erreur par rapport au corpus de test, la précision (RMSE) et le temps d'exécution.
RMSE signifie Root Mean Square Error, c'est la racine de l'erreur de prédiction au carré.

\newpage

# Précision par rapport au nombre de paramètres

![accuracyToNbParam](/Users/pa/Documents/stageQarma2017/expe/onDataCCDS/theano/cpDecompositionNN/accSortedByNbParam.png)

\newpage

# Précision triées dans l'ordre décroissant

![accuracyToNbParam](/Users/pa/Documents/stageQarma2017/expe/onDataCCDS/theano/cpDecompositionNN/accSortedFromDownTop.png)

\newpage



**Ici l'accuracy est calculée avec RMSE : plus la valeur est petite mieux c'est!**

## 25 Meilleurs loss
| Input shape | Output shape | Rank | Loss | Accuracy | Time | Nb param |
|:-----------:|:------------:|:----:|:----:|:--------:|:----:|:--------:|
| 5-5-3-17-5 | 2-2-8-8-4 | 4 | 0.950007 | 0.971972 | 34.30010 | 800 |
| 5-5-17-3-5 | 8-4-2-8-2 | 4 | 0.952052 | 0.973003 | 39.81089 | 512 |
| 5-3-5-17-5 | 16-4-4-2-2 | 4 | 0.952324 | 0.973133 | 32.85277 | 624 |
| 5-5-17-3-5 | 2-16-2-4-4 | 4 | 0.952422 | 0.973220 | 34.08618 | 624 |
| 5-17-5-3-5 | 8-4-4-4-2 | 4 | 0.952460 | 0.973317 | 37.06249 | 600 |
| 17-5-3-5-5 | 8-8-4-2-2 | 4 | 0.952478 | 0.973212 | 42.94515 | 832 |
| 5-5-5-17-3 | 2-8-4-4-4 | 4 | 0.952590 | 0.973312 | 34.61742 | 600 |
| 5-5-17-5-3 | 2-16-4-2-4 | 4 | 0.952632 | 0.973315 | 32.74526 | 720 |
| 5-3-17-5-5 | 4-8-4-4-2 | 5 | 0.952699 | 0.973334 | 49.08983 | 710 |
| 5-5-17-3-5 | 2-8-8-2-4 | 4 | 0.952831 | 0.973376 | 32.90619 | 848 |
| 5-5-5-3-17 | 4-2-4-8-4 | 4 | 0.952869 | 0.973439 | 33.16507 | 568 |
| 5-5-5-17-3 | 4-4-2-8-4 | 4 | 0.952999 | 0.973559 | 35.66480 | 792 |
| 5-17-3-5-5 | 2-8-8-2-4 | 4 | 0.953044 | 0.973549 | 31.89798 | 800 |
| 5-17-5-5-3 | 2-16-2-4-4 | 4 | 0.953059 | 0.973550 | 35.46786 | 1296 |
| 5-3-17-5-5 | 2-8-16-2-2 | 4 | 0.953062 | 0.973513 | 40.08945 | 1304 |
| 5-5-17-5-3 | 16-4-2-2-4 | 4 | 0.953112 | 0.973588 | 32.98168 | 624 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953168 | 0.973571 | 36.90730 | 680 |
| 5-17-5-3-5 | 16-2-2-8-2 | 4 | 0.953180 | 0.973613 | 42.96329 | 632 |
| 5-5-17-3-5 | 2-4-8-4-4 | 4 | 0.953255 | 0.973665 | 30.43605 | 792 |
| 5-5-3-5-17 | 2-2-16-2-8 | 4 | 0.953267 | 0.973656 | 31.71065 | 856 |
| 3-5-5-5-17 | 16-8-2-2-2 | 4 | 0.953302 | 0.973677 | 37.03641 | 568 |
| 5-5-5-17-3 | 4-8-2-2-8 | 4 | 0.953315 | 0.973728 | 31.04074 | 512 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953343 | 0.973662 | 37.63195 | 680 |
| 5-3-17-5-5 | 2-4-2-2-32 | 4 | 0.953443 | 0.973767 | 25.32193 | 904 |
| 3-17-5-5-5 | 16-4-2-4-2 | 4 | 0.953465 | 0.973758 | 42.82478 | 624 |

## 25 Meilleurs accuracy
| Input shape | Output shape | Rank | Loss | Accuracy | Time | Nb param |
|:-----------:|:------------:|:----:|:----:|:--------:|:----:|:--------:|
| 5-5-3-17-5 | 2-2-8-8-4 | 4 | 0.950007 | 0.971972 | 34.30010 | 800 |
| 5-5-17-3-5 | 8-4-2-8-2 | 4 | 0.952052 | 0.973003 | 39.81089 | 512 |
| 5-3-5-17-5 | 16-4-4-2-2 | 4 | 0.952324 | 0.973133 | 32.85277 | 624 |
| 17-5-3-5-5 | 8-8-4-2-2 | 4 | 0.952478 | 0.973212 | 42.94515 | 832 |
| 5-5-17-3-5 | 2-16-2-4-4 | 4 | 0.952422 | 0.973220 | 34.08618 | 624 |
| 5-5-5-17-3 | 2-8-4-4-4 | 4 | 0.952590 | 0.973312 | 34.61742 | 600 |
| 5-5-17-5-3 | 2-16-4-2-4 | 4 | 0.952632 | 0.973315 | 32.74526 | 720 |
| 5-17-5-3-5 | 8-4-4-4-2 | 4 | 0.952460 | 0.973317 | 37.06249 | 600 |
| 5-3-17-5-5 | 4-8-4-4-2 | 5 | 0.952699 | 0.973334 | 49.08983 | 710 |
| 5-5-17-3-5 | 2-8-8-2-4 | 4 | 0.952831 | 0.973376 | 32.90619 | 848 |
| 5-5-5-3-17 | 4-2-4-8-4 | 4 | 0.952869 | 0.973439 | 33.16507 | 568 |
| 5-3-17-5-5 | 2-8-16-2-2 | 4 | 0.953062 | 0.973513 | 40.08945 | 1304 |
| 5-17-3-5-5 | 2-8-8-2-4 | 4 | 0.953044 | 0.973549 | 31.89798 | 800 |
| 5-17-5-5-3 | 2-16-2-4-4 | 4 | 0.953059 | 0.973550 | 35.46786 | 1296 |
| 5-5-5-17-3 | 4-4-2-8-4 | 4 | 0.952999 | 0.973559 | 35.66480 | 792 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953168 | 0.973571 | 36.90730 | 680 |
| 5-5-17-5-3 | 16-4-2-2-4 | 4 | 0.953112 | 0.973588 | 32.98168 | 624 |
| 5-17-5-3-5 | 16-2-2-8-2 | 4 | 0.953180 | 0.973613 | 42.96329 | 632 |
| 5-5-3-5-17 | 2-2-16-2-8 | 4 | 0.953267 | 0.973656 | 31.71065 | 856 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953343 | 0.973662 | 37.63195 | 680 |
| 5-5-17-3-5 | 2-4-8-4-4 | 4 | 0.953255 | 0.973665 | 30.43605 | 792 |
| 3-5-5-5-17 | 16-8-2-2-2 | 4 | 0.953302 | 0.973677 | 37.03641 | 568 |
| 5-5-5-17-3 | 4-8-2-2-8 | 4 | 0.953315 | 0.973728 | 31.04074 | 512 |
| 3-17-5-5-5 | 16-4-2-4-2 | 4 | 0.953465 | 0.973758 | 42.82478 | 624 |
| 5-3-17-5-5 | 2-4-2-2-32 | 4 | 0.953443 | 0.973767 | 25.32193 | 904 |

## 25 Pires loss
| Input shape | Output shape | Rank | Loss | Accuracy | Time | Nb param |
|:-----------:|:------------:|:----:|:----:|:--------:|:----:|:--------:|
| 5-5-3-17-5 | 2-2-8-8-4 | 4 | 0.950007 | 0.971972 | 34.30010 | 800 |
| 5-5-17-3-5 | 8-4-2-8-2 | 4 | 0.952052 | 0.973003 | 39.81089 | 512 |
| 5-3-5-17-5 | 16-4-4-2-2 | 4 | 0.952324 | 0.973133 | 32.85277 | 624 |
| 5-5-17-3-5 | 2-16-2-4-4 | 4 | 0.952422 | 0.973220 | 34.08618 | 624 |
| 5-17-5-3-5 | 8-4-4-4-2 | 4 | 0.952460 | 0.973317 | 37.06249 | 600 |
| 17-5-3-5-5 | 8-8-4-2-2 | 4 | 0.952478 | 0.973212 | 42.94515 | 832 |
| 5-5-5-17-3 | 2-8-4-4-4 | 4 | 0.952590 | 0.973312 | 34.61742 | 600 |
| 5-5-17-5-3 | 2-16-4-2-4 | 4 | 0.952632 | 0.973315 | 32.74526 | 720 |
| 5-3-17-5-5 | 4-8-4-4-2 | 5 | 0.952699 | 0.973334 | 49.08983 | 710 |
| 5-5-17-3-5 | 2-8-8-2-4 | 4 | 0.952831 | 0.973376 | 32.90619 | 848 |
| 5-5-5-3-17 | 4-2-4-8-4 | 4 | 0.952869 | 0.973439 | 33.16507 | 568 |
| 5-5-5-17-3 | 4-4-2-8-4 | 4 | 0.952999 | 0.973559 | 35.66480 | 792 |
| 5-17-3-5-5 | 2-8-8-2-4 | 4 | 0.953044 | 0.973549 | 31.89798 | 800 |
| 5-17-5-5-3 | 2-16-2-4-4 | 4 | 0.953059 | 0.973550 | 35.46786 | 1296 |
| 5-3-17-5-5 | 2-8-16-2-2 | 4 | 0.953062 | 0.973513 | 40.08945 | 1304 |
| 5-5-17-5-3 | 16-4-2-2-4 | 4 | 0.953112 | 0.973588 | 32.98168 | 624 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953168 | 0.973571 | 36.90730 | 680 |
| 5-17-5-3-5 | 16-2-2-8-2 | 4 | 0.953180 | 0.973613 | 42.96329 | 632 |
| 5-5-17-3-5 | 2-4-8-4-4 | 4 | 0.953255 | 0.973665 | 30.43605 | 792 |
| 5-5-3-5-17 | 2-2-16-2-8 | 4 | 0.953267 | 0.973656 | 31.71065 | 856 |
| 3-5-5-5-17 | 16-8-2-2-2 | 4 | 0.953302 | 0.973677 | 37.03641 | 568 |
| 5-5-5-17-3 | 4-8-2-2-8 | 4 | 0.953315 | 0.973728 | 31.04074 | 512 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953343 | 0.973662 | 37.63195 | 680 |
| 5-3-17-5-5 | 2-4-2-2-32 | 4 | 0.953443 | 0.973767 | 25.32193 | 904 |
| 3-17-5-5-5 | 16-4-2-4-2 | 4 | 0.953465 | 0.973758 | 42.82478 | 624 |

## 25 Pires accuracy
| Input shape | Output shape | Rank | Loss | Accuracy | Time | Nb param |
|:-----------:|:------------:|:----:|:----:|:--------:|:----:|:--------:|
| 5-5-3-17-5 | 2-2-8-8-4 | 4 | 0.950007 | 0.971972 | 34.30010 | 800 |
| 5-5-17-3-5 | 8-4-2-8-2 | 4 | 0.952052 | 0.973003 | 39.81089 | 512 |
| 5-3-5-17-5 | 16-4-4-2-2 | 4 | 0.952324 | 0.973133 | 32.85277 | 624 |
| 17-5-3-5-5 | 8-8-4-2-2 | 4 | 0.952478 | 0.973212 | 42.94515 | 832 |
| 5-5-17-3-5 | 2-16-2-4-4 | 4 | 0.952422 | 0.973220 | 34.08618 | 624 |
| 5-5-5-17-3 | 2-8-4-4-4 | 4 | 0.952590 | 0.973312 | 34.61742 | 600 |
| 5-5-17-5-3 | 2-16-4-2-4 | 4 | 0.952632 | 0.973315 | 32.74526 | 720 |
| 5-17-5-3-5 | 8-4-4-4-2 | 4 | 0.952460 | 0.973317 | 37.06249 | 600 |
| 5-3-17-5-5 | 4-8-4-4-2 | 5 | 0.952699 | 0.973334 | 49.08983 | 710 |
| 5-5-17-3-5 | 2-8-8-2-4 | 4 | 0.952831 | 0.973376 | 32.90619 | 848 |
| 5-5-5-3-17 | 4-2-4-8-4 | 4 | 0.952869 | 0.973439 | 33.16507 | 568 |
| 5-3-17-5-5 | 2-8-16-2-2 | 4 | 0.953062 | 0.973513 | 40.08945 | 1304 |
| 5-17-3-5-5 | 2-8-8-2-4 | 4 | 0.953044 | 0.973549 | 31.89798 | 800 |
| 5-17-5-5-3 | 2-16-2-4-4 | 4 | 0.953059 | 0.973550 | 35.46786 | 1296 |
| 5-5-5-17-3 | 4-4-2-8-4 | 4 | 0.952999 | 0.973559 | 35.66480 | 792 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953168 | 0.973571 | 36.90730 | 680 |
| 5-5-17-5-3 | 16-4-2-2-4 | 4 | 0.953112 | 0.973588 | 32.98168 | 624 |
| 5-17-5-3-5 | 16-2-2-8-2 | 4 | 0.953180 | 0.973613 | 42.96329 | 632 |
| 5-5-3-5-17 | 2-2-16-2-8 | 4 | 0.953267 | 0.973656 | 31.71065 | 856 |
| 3-5-5-5-17 | 32-4-2-2-2 | 4 | 0.953343 | 0.973662 | 37.63195 | 680 |
| 5-5-17-3-5 | 2-4-8-4-4 | 4 | 0.953255 | 0.973665 | 30.43605 | 792 |
| 3-5-5-5-17 | 16-8-2-2-2 | 4 | 0.953302 | 0.973677 | 37.03641 | 568 |
| 5-5-5-17-3 | 4-8-2-2-8 | 4 | 0.953315 | 0.973728 | 31.04074 | 512 |
| 3-17-5-5-5 | 16-4-2-4-2 | 4 | 0.953465 | 0.973758 | 42.82478 | 624 |
| 5-3-17-5-5 | 2-4-2-2-32 | 4 | 0.953443 | 0.973767 | 25.32193 | 904 |
