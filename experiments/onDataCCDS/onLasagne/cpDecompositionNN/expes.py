#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
from USHCNwCPLayer import main as USHCNmain
import numpy as np

def main(numFold):
    folder_path = '/data1/home/pierre-antoine.martrice/stageQarma2017/expe/onDataCCDS/theano/cpDecompositionNN/'
    grid = open(folder_path + "gridOfParameters_CCDS_CPLayer_part" + str(numFold) + ".txt", 'r')
    results = open(folder_path + "results_part" + numFold + ".txt", 'w')
    param = grid.readline().split(" ")

    while not param == [""]:
        print("### param = ", param)
        input_shape = param[0]
        output_shape = param[1]
        rank = int(param[2])
        num_epochs = 5

        # Apprentissage du réseau avec cross validation k-fold, k = 10
        lossArray = []
        accArray = []
        timeArray = []
        valArray = []
        print(">> NEW EXPE <<")
        for i in range(1, 11):
            print(">>>> CROSS-VALIDATION : " + str(i) + "/10 <<<<")
            res = USHCNmain(input_shape, output_shape, rank, i, num_epochs)
            print(res)
            lossArray.append(res[0])
            accArray.append(res[1])
            timeArray.append(res[2])

            inpSh = input_shape.split("-")
            outpSh = output_shape.split("-")
            # Somme des produits termes à termes de inpSh et outpSh, multiplier par le rang
            nb_param = sum([int(x) * int(y) for x, y in zip(inpSh, outpSh)]) * rank
            nb_param_denseLayer = 3 * 17 * 125
            taux_de_compression = float(nb_param) / nb_param_denseLayer
            localVal = res[1] * (1 - taux_de_compression)
            valArray.append(localVal)
            print("nb_param = ", nb_param)
            print("taux_de_compression = ", taux_de_compression)
            print("localVal = ", localVal)

        loss = np.mean(lossArray)
        acc = np.mean(accArray)
        time = np.mean(timeArray)
        val = np.mean(valArray)

        if taux_de_compression < 1:
            res = "| " + str(input_shape) + " | " + str(output_shape) + " | " + str(rank) + " | " + str(loss) +\
                  " | " + str(acc) + " | " + str(time) + " | " + str(nb_param) + " | " + str(val) + " |\n"
            results.write(res)
            print(res)
        param = grid.readline().split(" ")

if __name__ == '__main__':
    kwargs = {}
    if len(sys.argv) > 1:
        kwargs['numFold'] = str(sys.argv[1])
    main(**kwargs)