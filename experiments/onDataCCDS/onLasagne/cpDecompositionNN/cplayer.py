#!/usr/bin/env python
#-*- coding: utf-8 -*-

import numpy as np
import theano
import theano.tensor as T
from keras.engine.topology import Layer
import lasagne
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class CPLayer(Layer):
    def __init__(self,
                 incoming,
                 cp_input_shape,
                 cp_output_shape,
                 rank,
                 **kwargs):
        super(CPLayer, self).__init__(incoming, **kwargs)
        self.nonlinearity = (nonlinearities.identity if nonlinearity is None else nonlinearity)
        num_inputs = int(np.prod(self.input_shape[1:]))
        cp_input_shape = np.array(cp_input_shape)
        cp_output_shape = np.array(cp_output_shape)
        rank = int(rank)
        if np.prod(cp_input_shape) != num_inputs:
            raise ValueError("The size of the input tensor (i.e. product of the elements in cp_input_shape) should "
                             "equal to the number of input neurons %d." %(num_inputs))

        if cp_input_shape.shape[0] != cp_output_shape.shape[0]:
            raise ValueError("The number of input and output dimensions should be the same.")

        if isinstance(rank, int) != True:
            raise ValueError("The rank should be an integer.")

        # print("Hello depuis le constructeur")
        self.cp_input_shape = cp_input_shape
        self.cp_output_shape = cp_output_shape
        self.rank = rank
        self.nonlinearity = nonlinearity
        self.num_dim = cp_input_shape.shape[0]
        local_cores_arr = _generate_cores(cp_input_shape, cp_output_shape, rank)
        self.cores_arr = self.add_param(local_cores_arr, local_cores_arr.shape, name='cores_arr', trainable=True)

    def get_output_for(self, input, **kwargs):
        t = self.cp_input_shape * self.cp_output_shape
        W = T.zeros(t.tolist())
        for r in range(self.rank):
            idx=0
            A = T.ones((1,))
            for i in range(self.num_dim):
                shape = (self.cp_input_shape[i] * self.cp_output_shape[i], self.rank)
                G = self.cores_arr[idx:idx + T.prod(shape)].reshape(shape)
                idx += T.prod(shape)
                A = T.outer(A, G[:, r])
            A = A.reshape((self.cp_input_shape * self.cp_output_shape))
            W += A

        if input.ndim > 2:
            input = input.flatten(2)

        res = T.dot(input,W.reshape((np.prod(self.cp_input_shape), -1)))

        print
        print self.nonlinearity(res)
        print type(self.nonlinearity(res))
        print

        return self.nonlinearity(res)

    def get_output_shape_for(self, input_shape):
        return input_shape[0], np.prod(self.cp_output_shape)


'''
def tix(*args):
    nd = len(args)
    out = []
    for k, new in enumerate(args):
        out.append(new.reshape((1,)*k + (new.size,) + (1,)*(nd-k-1)))
    return out

'''


def _generate_cores(input_shape, output_shape, rank):    # privee
    input_shape = np.array(input_shape)
    output_shape = np.array(output_shape)
    rank = int(rank)
    ranks = np.empty(input_shape.shape[0])
    ranks.fill(rank)
    cores_arr_len = np.sum(input_shape * output_shape * ranks)
    cores_arr = lasagne.random.get_rng().uniform(-1, 1, size=(int(cores_arr_len),))
    #cores_arr = np.zeros((int(cores_arr_len),)) + 0.0001    #lasagne.random.get_rng().normal(0, 1, size=(int(cores_arr_len),))

    return lasagne.utils.floatX(cores_arr)



