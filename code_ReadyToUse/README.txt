Les fichiers CPLayer.py, KDLayer.py et TTLayer.py implémentent des couches denses compréssées à l'aide de la
décomposition éponyme.

L'architecture du réseau de neuones est définit dans neuralNetwork.py.
Cette classe (ie l'architecture) est vouée à être modifiée suivant les besoins.

Le fichier launcher.py