#!/usr/bin/env python
#-*- coding: utf-8 -*-

'''
Classe qui implémente un réseau de neurone construit en fonction des différents paramètres fournis.
Elle renvoie le scores obtenues, le temps d'exécution ainsi que le nombre de paramètres.

Cette classe (ie l'architecture) est vouée à être modifiée suivant les besoins.
Cad modifier l'architecture + les paramètres qui en découles.

Exemple d'appel de la fonction main : main(x_train, y_train, x_test, y_test, keras.losses.categorical_crossentropy, accuracy, 20, 1000, 150)
'''

from __future__ import print_function
import os
import time
import sys
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.callbacks import EarlyStopping
from keras import optimizers
from keras import backend as K
from CPLayer import CPLayer
from KDLayer import KDLayer
from TTLayer import TTLayer


def main(input_shape,
         output_shape_out_layer,
         x_train,
         y_train,
         x_test,
         y_test,
         loss,
         metric,
         optimizer,
         batch_size,
         nb_neurones_dense_layer,
         nb_epochs):

    if not isinstance(batch_size, int):
        raise TypeError('"batch_size" doit être un int!')
    if not batch_size > 0:
        raise ValueError('"batch_size" doit être supérieur à 0!')
    if not isinstance(nb_neurones_dense_layer, int):
        raise TypeError('"nb_neurones_dense_layer" doit être un int!')
    if not nb_neurones_dense_layer > 0:
        raise ValueError('"nb_neurones_dense_layer" doit être supérieur à 0!')
    if not isinstance(nb_epochs, int):
        raise TypeError('"nb_epochs" doit être un int!')
    if not nb_epochs > 0:
        raise ValueError('"nb_epochs" doit être supérieur à 0!')

    # Création de l'architecture du modèle
    model = Sequential()
    model.add(Flatten(input_shape=input_shape))
    model.add(Dense(nb_neurones_dense_layer))
    model.add(Activation('relu'))
    model.add(Dense(output_shape_out_layer, activation='softmax'))
    print(model.summary())

    # EarlyStopping : Fonction qui arrête prématurément l'apprentissage lorsque qu'un minimum local (ou point selle) est atteint.
    es = EarlyStopping(monitor='val_loss', min_delta=0.005, patience=3, verbose=1, mode='auto')
    nb_params = int(sum([np.prod(K.get_value(w).shape) for w in model.trainable_weights]))

    # Fonction de compilation (lie l'architecture aux fonctions de pertes et d'apprentissages choisient)
    model.compile(loss=loss, optimizer = optimizer, metrics=[metric])

    start_time = time.time()
    # Fonction d'exécution des boucles d'apprentissage, de back-propagations etc. sur la base d'apprentissage.
    model.fit(x_train,
              y_train,
              batch_size = batch_size,
              epochs = nb_epochs,
              verbose = 1,
              validation_data = (x_test, y_test),
              callbacks = [es])
    end_time = time.time() - start_time

    # Fonction de calcul des score sur la base de test.
    score = model.evaluate(x_test, y_test, verbose=1)

    score[0] = np.sqrt(score[0])
    score[1] = np.sqrt(score[1])

    return score[0], score[1], end_time, nb_params

if __name__ == '__main__':
    kwargs = {}
    kwargs['input_shape'] = sys.argv[1]
    kwargs['output_shape_out_layer'] = sys.argv[2]
    kwargs['x_train'] = sys.argv[3]
    kwargs['y_train'] = sys.argv[4]
    kwargs['x_test'] = sys.argv[5]
    kwargs['y_test'] = sys.argv[6]
    kwargs['loss'] = sys.argv[7]
    kwargs['metric'] = sys.argv[8]
    kwargs['batch_size'] = sys.argv[9]
    kwargs['nb_neurones_dense_layer'] = sys.argv[10]
    kwargs['nb_epochs'] = sys.argv[11]
    main(**kwargs)