#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Résumé:
------
Couche dense dont la matrice de paramètres est compréssée avec d'une décomposition de CANDECOMP/PARAFAC (CP).


Détails:
-------
Soit une couche dense de n neurones avec m éléments en entré.
La couche apprend donc une matrice de poids W de taille (n * m) telle que Y = X . W

La décomposition CP décompose un tenseur d'ordre p comme une somme de produit de p vecteurs.
Dans notre cas, nous allons reshape la matrice W sous la forme d'un tenseur dont les dimension sont "cp_input_shape",
puis nous allons la décomposer. On obtient donc :

W = ∑_k ( v_1^k * v_2^k * ... * v_p^k)

avec K = rang-CP.
De manière concrète, l'ensemble des vecteurs de la décompositon sont concaténés dans la variable cores_arr
puis récupérés avec les bonnes dimensions dans la fonction call.
Il faut bien comprendre aussi que l'apprentissage se fait directment sur la matrice W compressée, cad qu'on n'initialise pas
une matrice de paramètres W pour la compresser ensuite, mais que l'on génère directement les vecteurs de la décomposition.


Remarque1:
L'ajout d'un biais n'ai pas ici implémenté.

Exemple d'appel de la fonction init : CPLayer((7,4,2,14), (20,4,5,2), 9)
'''

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import theano
import theano.tensor as T
import numpy as np
import theano as th
from theano import tensor
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class CPLayer(Layer):
    def __init__(self, cp_input_shape, cp_output_shape, rank, **kwargs):
        self.cp_input_shape = np.array(cp_input_shape)
        self.cp_output_shape = np.array(cp_output_shape)
        self.rank = int(rank)
        self.num_dim = cp_input_shape.shape[0]
        super(CPLayer, self).__init__(**kwargs)

        if cp_input_shape.shape[0] != cp_output_shape.shape[0]:
            raise ValueError("The number of input and output dimensions should be the same.")
        if isinstance(rank, int) != True:
            raise ValueError("The rank should be an integer.")

    def build(self, input_shape):
        cp_input_shape = np.array(self.cp_input_shape)
        cp_output_shape = np.array(self.cp_output_shape)
        rank = int(self.rank)
        ranks = np.empty(cp_input_shape.shape[0])
        ranks.fill(rank)
        cores_arr_len = int(np.sum(cp_input_shape * cp_output_shape * ranks))
        self.cores_arr = self.add_weight(name="cores_array", shape=(cores_arr_len,), initializer='glorot_uniform', trainable=True)
        super(CPLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, input):
        t = self.cp_input_shape * self.cp_output_shape
        W = T.zeros(t.tolist())
        for r in range(self.rank):
            idx=0
            A = T.ones((1,))
            for i in range(self.num_dim):
                shape = (self.cp_input_shape[i] * self.cp_output_shape[i], self.rank)
                G = self.cores_arr[idx:idx + T.prod(shape)].reshape(shape)
                idx += T.prod(shape)
                A = T.outer(A, G[:, r])
            A = A.reshape((self.cp_input_shape * self.cp_output_shape))
            W += A
        if input.ndim > 2:
            input = input.flatten(2)
        return T.dot(input,W.reshape((np.prod(self.cp_input_shape), -1)))

    def compute_output_shape(self, input_shape):
        return input_shape[0], np.prod(self.cp_output_shape)
