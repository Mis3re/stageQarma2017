#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Résumé:
------
Couche dense dont la matrice de paramètres est compréssée avec d'une décomposition Tensor Train (TT).


Détails:
-------
Soit une couche dense de n neurones avec m éléments en entré.
La couche apprend donc une matrice de poids W de taille (n * m) telle que Y = X . W

La décomposition TT décompose un tenseur T d'ordre p en un ensemble p de tenseurs Gi d'ordre 3 tel que chaque élément
T(j1, ..., jp) de ce tenseur se calcul par :

T(j1, ..., jp) = G1[j1] * G2[j2] * ... * Gp[jp]

ou les Gi[ji] sont donc des matrices de taille (r_(k-1) * r_k) ou le tuple (r_0, r_1, ..., r_(k-1), r_k, ..., r_p) est le rang-TT.
Dans notre cas, nous allons reshape la matrice W sous la forme d'un tenseur W' dont les dimension sont "tt_input_shape",
puis nous allons la décomposer comme exposé ci-desssus.

De manière concrète, l'ensemble des vecteurs de la décompositon sont concaténés dans la variable cores_arr
puis récupérés avec les bonnes dimensions dans la fonction call.
Il faut bien comprendre aussi que l'apprentissage se fait directment sur la matrice W compressée, cad qu'on n'initialise pas
une matrice de paramètres W pour la compresser ensuite, mais que l'on génère directement les vecteurs de la décomposition.


Remarque1:
L'ajout d'un biais n'ai pas ici implémenté.

Remarque2:
tt_input_shape = (x1, x2, ..., xp)
tt_output_shape = (y1, y2, ..., yp)
rank-TT = (1, r1, r2, ..., r(p-1), 1)
Pour calculer le nombre d'élments de l'approximation, on doit faire le calcul suivant :
nb_elmt = (1 * x1 * y1 * r1) + (r1 * x2 * y2 * r2) + ... + (r(p-2) * x(p-1) * y(p-1) * r(p-1)) + (r(p-1) * xp * yp * 1)


Exemple d'appel de la fonction init : TTLayer((7,4,2,14), (20,4,5,2), (1,4,6,4,1))
'''

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import theano
import theano.tensor as T
import numpy as np
import theano as th
from theano import tensor
import theano.typed_list
from sktensor import ktensor, dtensor, cp_als
from theano import shared
from theano.scan_module import reduce


class TTLayer(Layer):
    def __init__(self, tt_input_shape, tt_output_shape, ranks, **kwargs):
        self.tt_input_shape = np.array(tt_input_shape)
        self.tt_output_shape = np.array(tt_output_shape)
        self.ranks = np.array(ranks)
        self.num_dim = tt_input_shape.shape[0]
        super(TTLayer, self).__init__(**kwargs)

        if tt_input_shape.shape[0] != tt_output_shape.shape[0]:
            raise ValueError("The number of input and output dimensions should be the same.")
        for r in ranks:
            if isinstance(r, int) != True:
                raise ValueError("The rank should be an tuple of integer.")

    def build(self, input_shape):
        tt_input_shape = np.array(self.tt_input_shape)
        tt_output_shape = np.array(self.tt_output_shape)
        ranks = np.array(self.ranks)
        cores_arr_len = np.sum(tt_input_shape * tt_output_shape * ranks[1:] * ranks[:-1])
        self.cores_arr = self.add_weight(name="cores_array", shape=(cores_arr_len,), initializer='glorot_normal', trainable=True)
        super(TTLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, input):
        res = input
        core_arr_idx = 0
        for k in range(self.num_dim - 1, -1, -1):
            curr_shape = (self.tt_input_shape[k] * self.ranks[k + 1], self.ranks[k] * self.tt_output_shape[k])
            curr_core = self.cores_arr[core_arr_idx:core_arr_idx+T.prod(curr_shape)].reshape(curr_shape)
            res = T.dot(res.reshape((-1, curr_shape[0])), curr_core)
            res = T.transpose(res.reshape((-1, self.tt_output_shape[k])))
            core_arr_idx += T.prod(curr_shape)
        res = T.transpose(res.reshape((-1, input.shape[0])))
        return res

    def compute_output_shape(self, input_shape):
        return (input_shape[0], np.prod(self.tt_output_shape))
