#!/usr/bin/env python
#-*- coding: utf-8 -*-

'''
Résumé:
------
Couche dense dont la matrice de paramètres est compréssée avec d'une décomposition de Kronecker.


Détails:
-------
Soit une couche dense de n neurones avec m éléments en entré.
La couche apprend donc une matrice de poids W de taille (n * m) telle que Y = X . W

La décomposition de Kronecker suppose une structure potentiellement en bloc de la matrice W.
Elle décompose W comme une somme de K produit (A ⊗ B) avec ⊗ le produit de Kronecker et K le rang-KD.
On a donc :

W = ∑_k ( A^k ⊗ B^k )

puis chaque A^k et B^k sont à leur tour décomposé en un produit (A' ⊗ B'). (ie une somme de produit (A' ⊗ B') avec K=1)
On obtient finalement :

W = ∑_k ( A_input^k ⊗ B_input^k ⊗ A_output^k ⊗ B_output^k )

avec dim(A_input^k ⊗ B_input^k) = n et dim(A_output^k ⊗ B_output^k) = m  où dim() retourne le nombre d'éléments.
Si les shapes correspondants sont bien choisis alors W sera de taille (n * m).
Sinon bien que le nombre de paramètres reste le même, W nécessite un reshape final pour être reformer en (n * m).

Exemple de shape pour images MNIST de dim (28*28)
- A_input_shape = (2, 2) ; B_input_shape = (14, 14) ; A_output_shape = (4, 4) ; B_output_shape = (7, 7) => pas de reshape nécessaire
- A_input_shape = (2, 2) ; B_input_shape = (14, 14) ; A_output_shape = (5, 5) ; B_output_shape = (8, 4) => reshape nécessaire


Remarque1:
Une généralisation de cette décomposition pourrait être une double décomposition de rank (K,L) (actuellement de rang (K,1))
Voir n décompositions de rang Ki récursivement.

Remarque2:
Le backend utilisé par Keras est celui de Theano car TensorFlow n'implémente à ce jour pas encore la fonction kron (ie le produit de kronecker).

Remarque3:
L'ajout d'un biais n'ai pas ici implémenté.

Exemple d'appel de la fonction init : KDLayer(800, (7,4), (2,14), (20,4), (5,2), 9, 6)
'''

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np
import theano as th
from theano import tensor
from theano.tensor.slinalg import kron


class KDLayer(Layer):
    def __init__(self, output_dim, shapeAin, shapeBin, shapeAout, shapeBout, rank, batch_size, **kwargs):
        self.output_dim = output_dim
        self.shapeAin = shapeAin
        self.shapeBin = shapeBin
        self.shapeAout = shapeAout
        self.shapeBout = shapeBout
        self.rank = rank
        self.batch_size = batch_size
        super(KDLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        self.Ain = self.add_weight(name="Ain", shape=(self.rank, self.shapeAin[0], self.shapeAin[1]), initializer='glorot_uniform', trainable=True)
        self.Bin = self.add_weight(name="Bin", shape=(self.rank, self.shapeBin[0], self.shapeBin[1]), initializer='glorot_uniform', trainable=True)
        self.Aout = self.add_weight(name="Aout", shape=(self.rank, self.shapeAout[0], self.shapeAout[1]), initializer='glorot_uniform', trainable=True)
        self.Bout = self.add_weight(name="Bout", shape=(self.rank, self.shapeBout[0], self.shapeBout[1]), initializer='glorot_uniform', trainable=True)
        super(KDLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        A = K.zeros((self.shapeAin[0] * self.shapeBin[0], self.shapeAin[1] * self.shapeBin[1]))
        B = K.zeros((self.shapeAout[0] * self.shapeBout[0],self.shapeAout[1] * self.shapeBout[1]))
        kernel = K.zeros((self.shapeAin[0] * self.shapeBin[0] * self.shapeAout[0] * self.shapeBout[0],
                          self.shapeAin[1] * self.shapeBin[1] * self.shapeAout[1] * self.shapeBout[1]))
        for k in range(self.rank):
            A = A + th.tensor.slinalg.kron(self.Ain[k], self.Bin[k])
            B = B + th.tensor.slinalg.kron(self.Aout[k], self.Bout[k])
            kernel = kernel + th.tensor.slinalg.kron(A, B)
        kernel = K.reshape(kernel, (-1, self.output_dim))
        output = K.dot(x, kernel)
        return output

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)

