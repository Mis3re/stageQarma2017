#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import os
import numpy as np
from neuralNetwork_TTLayers import main as nn_main
from keras.datasets import mnist
from keras import backend as K
import keras


def main():
    # Chargement des données
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    img_rows = x_train.shape[1]
    img_cols = x_train.shape[2]
    if K.image_data_format() == 'channels_first':
        print('channels_first')
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255

    nb_neurones_out_layer = 10
    y_train = keras.utils.to_categorical(y_train, nb_neurones_out_layer)
    y_test = keras.utils.to_categorical(y_test, nb_neurones_out_layer)

    loss = keras.losses.categorical_crossentropy
    metric = 'accuracy'
    optimizer = keras.optimizers.RMSprop()
    batch_size = 500
    nb_neurones_dense_layer = 1000
    nb_epochs = 150

    print("Jeux de donnée : MNIST")
    print("input_shape = " + str(input_shape))
    print("nb_neurones_out_layer = " + str(nb_neurones_out_layer))
    print("x_train.shape = " + str(x_train.shape))
    print("y_train.shape = " + str(y_train.shape))
    print("x_test.shape = " + str(x_test.shape))
    print("y_test.shape = " + str(y_test.shape))
    print("loss = " + str(loss.__name__))
    print("metric = " + str(metric))
    print("optimizer = " + str(type(optimizer)))
    print("batch_size = " + str(batch_size))
    print("nb_epochs = " + str(nb_epochs))

    codeArchi = 3
    rank = "1-5-5-5-1"
    dropout_value = 0.2
    shapeIn = "7-7-4-4"
    shapeOut = "5-5-5-8"

    # Apprentissage du réseau
    res = nn_main(input_shape,
                  nb_neurones_out_layer,
                  x_train,
                  y_train,
                  x_test,
                  y_test,
                  loss,
                  metric,
                  optimizer,
                  batch_size,
                  nb_neurones_dense_layer,
                  nb_epochs,
                  codeArchi,
                  shapeIn,
                  shapeOut,
                  rank,
                  dropout_value)

    loss_value = res[0]
    metric_value = res[1]
    time_value = res[2]
    nb_params_value = res[3]

    folder_path = os.getcwd() + "/"
    results = open(folder_path + "results_TTLayers.txt", 'w')
    t = " | "

    res = "| " + str(loss_value) + t + str(metric_value) + t + str(time_value) + t + str(nb_params_value) + " |\n"
    print("\n\nResults [loss_value/metric_value/time_value/nb_params_value] : " + res)
    results.write(res)

if __name__ == '__main__':
    kwargs = {}
    main(**kwargs)
